.PHONY: all clean target_SPDATDB2_cbl clean_SPDATDB2_cbl target__settings clean__settings 

all: target_SPDATDB2_cbl 

target_SPDATDB2_cbl: ../../../bin/SPDATDB2.so

-include .cobolit.tmp.133.54.150.224/SPDATDB2-28661768ece3aaef6c4cb55b96cb90288998920d.d

../../../bin/SPDATDB2.so: SPDATDB2.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/TECHNIQUE/ASM_BATCH/SPDATDB2.cbl'
	cobc SPDATDB2.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../../bin/SPDATDB2.so" -MT "../../../bin/SPDATDB2.so" -MF .cobolit.tmp.133.54.150.224/SPDATDB2-28661768ece3aaef6c4cb55b96cb90288998920d.d

clean_SPDATDB2_cbl: 
	rm -f "../../../bin/SPDATDB2.so" .cobolit.tmp.133.54.150.224/SPDATDB2-28661768ece3aaef6c4cb55b96cb90288998920d.d



clean: clean_SPDATDB2_cbl 

target__settings: 


clean__settings: 

