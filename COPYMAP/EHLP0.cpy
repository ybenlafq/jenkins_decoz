      *
      * cicsmap ../DECOZ/BMS/EHLP0.bms -- 
      * EHLP0   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EHLP0I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MTIMJOUI              PIC X(5).
           02     MNOMMAPL              PIC S9(4) COMP-5.
           02     MNOMMAPF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MNOMMAPI              PIC X(8).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MLIBERRI              PIC X(79).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MCODTRAI              PIC X(4).
           02     MZONCMDL              PIC S9(4) COMP-5.
           02     MZONCMDF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MZONCMDI              PIC X(15).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(2).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(2).
           02     MSCREENI              PIC X(4).
       01  EHLP0O REDEFINES EHLP0I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNOMMAPA              PIC X.
           02     MNOMMAPC              PIC X.
           02     MNOMMAPH              PIC X.
           02     MNOMMAPO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRO              PIC X(79).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONCMDA              PIC X.
           02     MZONCMDC              PIC X.
           02     MZONCMDH              PIC X.
           02     MZONCMDO              PIC X(15).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSH                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENO              PIC X(4).
