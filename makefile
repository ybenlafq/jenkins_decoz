#SHELL += -x
do_script:
		setenv
BUILD := buildmake.sh
CICSMAP := cicsmap
#COBOLDIR := BATCH CICS TECHNIQUE/ESYT TECHNIQUE/ASM_CICS TECHNIQUE/ASM_BATCH
COBOLDIR := CICS
BMSDIR := BMS
BMSFILES := $(wildcard $(BMSDIR)/*.bms)
MAPDIR := MAP
MAPFILES := $(addprefix $(MAPDIR)/,$(addsuffix .map,$(basename $(notdir $(BMSFILES)))))


all: map cbl

map: message $(MAPFILES)

message :
	-@echo "Compilation des ecrans bms"

$(MAPFILES): $(MAPDIR)/%.map: $(BMSDIR)/%.bms | $(MAPDIR)
	cd $(MAPDIR) ; $(CICSMAP) -m $(notdir $(basename $@)) ../$<

cbl:
	-@echo "Compilation des programmes cobol convertis"
	$(foreach d, $(COBOLDIR), cd $(d) ; $(BUILD) ; $(MAKE); cd -;)
