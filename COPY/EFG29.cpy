      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG20   EFG20                                              00000020
      ***************************************************************** 00000030
       01   EFG29I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUI   PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLLIEUI   PIC X(19).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MPAGEI    PIC XX.                                         00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MPAGEMAXI      PIC XX.                                    00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCOMPTL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MSCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCOMPTF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MSCOMPTI  PIC X(3).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECOMPTL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MECOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MECOMPTF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MECOMPTI  PIC X(3).                                       00000430
           02 MLIGNEI OCCURS   10 TIMES .                               00000440
             03 MSELECTD OCCURS   3 TIMES .                             00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000460
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000470
               04 FILLER     PIC X(4).                                  00000480
               04 MSELECTI   PIC X.                                     00000490
             03 MSECORIGD OCCURS   3 TIMES .                            00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSECORIGL  COMP PIC S9(4).                            00000510
      *--                                                                       
               04 MSECORIGL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MSECORIGF  PIC X.                                     00000520
               04 FILLER     PIC X(4).                                  00000530
               04 MSECORIGI  PIC X(6).                                  00000540
             03 MSECTIOND OCCURS   3 TIMES .                            00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSECTIONL  COMP PIC S9(4).                            00000560
      *--                                                                       
               04 MSECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MSECTIONF  PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 MSECTIONI  PIC X(6).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCOPL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MSOCCOPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCCOPF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MSOCCOPI  PIC X(3).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUCOPL      COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MLIEUCOPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUCOPF      PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLIEUCOPI      PIC X(3).                                  00000670
      * MESSAGE ERREUR                                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MLIBERRI  PIC X(78).                                      00000720
      * CODE TRANSACTION                                                00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCODTRAI  PIC X(4).                                       00000770
      * CICS DE TRAVAIL                                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      * NETNAME                                                         00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MNETNAMI  PIC X(8).                                       00000870
      * CODE TERMINAL                                                   00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MSCREENI  PIC X(5).                                       00000920
      ***************************************************************** 00000930
      * SDF: EFG20   EFG20                                              00000940
      ***************************************************************** 00000950
       01   EFG29O REDEFINES EFG29I.                                    00000960
           02 FILLER    PIC X(12).                                      00000970
      * DATE DU JOUR                                                    00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDATJOUA  PIC X.                                          00001000
           02 MDATJOUC  PIC X.                                          00001010
           02 MDATJOUP  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUV  PIC X.                                          00001040
           02 MDATJOUO  PIC X(10).                                      00001050
      * HEURE                                                           00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNSOCA    PIC X.                                          00001150
           02 MNSOCC    PIC X.                                          00001160
           02 MNSOCP    PIC X.                                          00001170
           02 MNSOCH    PIC X.                                          00001180
           02 MNSOCV    PIC X.                                          00001190
           02 MNSOCO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNLIEUA   PIC X.                                          00001220
           02 MNLIEUC   PIC X.                                          00001230
           02 MNLIEUP   PIC X.                                          00001240
           02 MNLIEUH   PIC X.                                          00001250
           02 MNLIEUV   PIC X.                                          00001260
           02 MNLIEUO   PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLLIEUA   PIC X.                                          00001290
           02 MLLIEUC   PIC X.                                          00001300
           02 MLLIEUP   PIC X.                                          00001310
           02 MLLIEUH   PIC X.                                          00001320
           02 MLLIEUV   PIC X.                                          00001330
           02 MLLIEUO   PIC X(19).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MPAGEA    PIC X.                                          00001360
           02 MPAGEC    PIC X.                                          00001370
           02 MPAGEP    PIC X.                                          00001380
           02 MPAGEH    PIC X.                                          00001390
           02 MPAGEV    PIC X.                                          00001400
           02 MPAGEO    PIC 99.                                         00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGEMAXA      PIC X.                                     00001430
           02 MPAGEMAXC PIC X.                                          00001440
           02 MPAGEMAXP PIC X.                                          00001450
           02 MPAGEMAXH PIC X.                                          00001460
           02 MPAGEMAXV PIC X.                                          00001470
           02 MPAGEMAXO      PIC 99.                                    00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MSCOMPTA  PIC X.                                          00001500
           02 MSCOMPTC  PIC X.                                          00001510
           02 MSCOMPTP  PIC X.                                          00001520
           02 MSCOMPTH  PIC X.                                          00001530
           02 MSCOMPTV  PIC X.                                          00001540
           02 MSCOMPTO  PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MECOMPTA  PIC X.                                          00001570
           02 MECOMPTC  PIC X.                                          00001580
           02 MECOMPTP  PIC X.                                          00001590
           02 MECOMPTH  PIC X.                                          00001600
           02 MECOMPTV  PIC X.                                          00001610
           02 MECOMPTO  PIC X(3).                                       00001620
           02 MLIGNEO OCCURS   10 TIMES .                               00001630
             03 DFHMS1 OCCURS   3 TIMES .                               00001640
               04 FILLER     PIC X(2).                                  00001650
               04 MSELECTA   PIC X.                                     00001660
               04 MSELECTC   PIC X.                                     00001670
               04 MSELECTP   PIC X.                                     00001680
               04 MSELECTH   PIC X.                                     00001690
               04 MSELECTV   PIC X.                                     00001700
               04 MSELECTO   PIC X.                                     00001710
             03 DFHMS2 OCCURS   3 TIMES .                               00001720
               04 FILLER     PIC X(2).                                  00001730
               04 MSECORIGA  PIC X.                                     00001740
               04 MSECORIGC  PIC X.                                     00001750
               04 MSECORIGP  PIC X.                                     00001760
               04 MSECORIGH  PIC X.                                     00001770
               04 MSECORIGV  PIC X.                                     00001780
               04 MSECORIGO  PIC X(6).                                  00001790
             03 DFHMS3 OCCURS   3 TIMES .                               00001800
               04 FILLER     PIC X(2).                                  00001810
               04 MSECTIONA  PIC X.                                     00001820
               04 MSECTIONC  PIC X.                                     00001830
               04 MSECTIONP  PIC X.                                     00001840
               04 MSECTIONH  PIC X.                                     00001850
               04 MSECTIONV  PIC X.                                     00001860
               04 MSECTIONO  PIC X(6).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MSOCCOPA  PIC X.                                          00001890
           02 MSOCCOPC  PIC X.                                          00001900
           02 MSOCCOPP  PIC X.                                          00001910
           02 MSOCCOPH  PIC X.                                          00001920
           02 MSOCCOPV  PIC X.                                          00001930
           02 MSOCCOPO  PIC X(3).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLIEUCOPA      PIC X.                                     00001960
           02 MLIEUCOPC PIC X.                                          00001970
           02 MLIEUCOPP PIC X.                                          00001980
           02 MLIEUCOPH PIC X.                                          00001990
           02 MLIEUCOPV PIC X.                                          00002000
           02 MLIEUCOPO      PIC X(3).                                  00002010
      * MESSAGE ERREUR                                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLIBERRA  PIC X.                                          00002040
           02 MLIBERRC  PIC X.                                          00002050
           02 MLIBERRP  PIC X.                                          00002060
           02 MLIBERRH  PIC X.                                          00002070
           02 MLIBERRV  PIC X.                                          00002080
           02 MLIBERRO  PIC X(78).                                      00002090
      * CODE TRANSACTION                                                00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCODTRAA  PIC X.                                          00002120
           02 MCODTRAC  PIC X.                                          00002130
           02 MCODTRAP  PIC X.                                          00002140
           02 MCODTRAH  PIC X.                                          00002150
           02 MCODTRAV  PIC X.                                          00002160
           02 MCODTRAO  PIC X(4).                                       00002170
      * CICS DE TRAVAIL                                                 00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCICSA    PIC X.                                          00002200
           02 MCICSC    PIC X.                                          00002210
           02 MCICSP    PIC X.                                          00002220
           02 MCICSH    PIC X.                                          00002230
           02 MCICSV    PIC X.                                          00002240
           02 MCICSO    PIC X(5).                                       00002250
      * NETNAME                                                         00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MNETNAMA  PIC X.                                          00002280
           02 MNETNAMC  PIC X.                                          00002290
           02 MNETNAMP  PIC X.                                          00002300
           02 MNETNAMH  PIC X.                                          00002310
           02 MNETNAMV  PIC X.                                          00002320
           02 MNETNAMO  PIC X(8).                                       00002330
      * CODE TERMINAL                                                   00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSCREENA  PIC X.                                          00002360
           02 MSCREENC  PIC X.                                          00002370
           02 MSCREENP  PIC X.                                          00002380
           02 MSCREENH  PIC X.                                          00002390
           02 MSCREENV  PIC X.                                          00002400
           02 MSCREENO  PIC X(5).                                       00002410
                                                                                
