           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGETB ETABLISSEMENTS COMPTA GROUPE     *        
      *----------------------------------------------------------------*        
       01  RVGA01ML.                                                            
           05  FGETB-CTABLEG2    PIC X(15).                                     
           05  FGETB-CTABLEG2-REDEF REDEFINES FGETB-CTABLEG2.                   
               10  FGETB-NSOC            PIC X(03).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGETB-NSOC-N         REDEFINES FGETB-NSOC                    
                                         PIC 9(03).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGETB-CETAB           PIC X(03).                             
           05  FGETB-WTABLEG     PIC X(80).                                     
           05  FGETB-WTABLEG-REDEF  REDEFINES FGETB-WTABLEG.                    
               10  FGETB-WACTIF          PIC X(01).                             
               10  FGETB-LIBELLE         PIC X(20).                             
               10  FGETB-WFACT           PIC X(01).                             
               10  FGETB-WETBPRIN        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ML-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGETB-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGETB-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGETB-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGETB-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
