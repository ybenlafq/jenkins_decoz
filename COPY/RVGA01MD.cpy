           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGECS LES ECS DE LA FACT INTER SOC     *        
      *----------------------------------------------------------------*        
       01  RVGA01MD.                                                            
           05  FGECS-CTABLEG2    PIC X(15).                                     
           05  FGECS-CTABLEG2-REDEF REDEFINES FGECS-CTABLEG2.                   
               10  FGECS-NOECS           PIC X(05).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGECS-NOECS-N        REDEFINES FGECS-NOECS                   
                                         PIC 9(05).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  FGECS-WTABLEG     PIC X(80).                                     
           05  FGECS-WTABLEG-REDEF  REDEFINES FGECS-WTABLEG.                    
               10  FGECS-WACTIF          PIC X(01).                             
               10  FGECS-MOT1            PIC X(08).                             
               10  FGECS-MOT2            PIC X(08).                             
               10  FGECS-MOT3            PIC X(08).                             
               10  FGECS-TYPMT           PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01MD-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGECS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGECS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGECS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGECS-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
