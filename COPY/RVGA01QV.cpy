           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGGCT JRN NETTING A INTEGRER DS GCT    *        
      *----------------------------------------------------------------*        
       01  RVGA01QV.                                                            
           05  FGGCT-CTABLEG2    PIC X(15).                                     
           05  FGGCT-CTABLEG2-REDEF REDEFINES FGGCT-CTABLEG2.                   
               10  FGGCT-NJRN            PIC X(10).                             
               10  FGGCT-NSOCC           PIC X(03).                             
               10  FGGCT-NSEQ            PIC X(02).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGGCT-NSEQ-N         REDEFINES FGGCT-NSEQ                    
                                         PIC 9(02).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  FGGCT-WTABLEG     PIC X(80).                                     
           05  FGGCT-WTABLEG-REDEF  REDEFINES FGGCT-WTABLEG.                    
               10  FGGCT-WACTIF          PIC X(01).                             
               10  FGGCT-NETAB           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01QV-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGGCT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGGCT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGGCT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGGCT-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
