      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DES ECRITURES EN ENTREE                                   
      *        DANS LA BASE COMPTA GROUPE                                       
      *                                                                         
      *                                                                         
       01       FFGCTA-DSECT.                                                   
           05   FFGCTA-IDENT.                                                   
001          10 FFGCTA-NSOCCOMPT         PIC X(03).                             
004          10 FFGCTA-NETAB             PIC X(03).                             
007          10 FFGCTA-NEXERCICE         PIC X(04).                             
011          10 FFGCTA-NPERIODE          PIC X(03).                             
014          10 FFGCTA-NJRN              PIC X(10).                             
024          10 FFGCTA-NPIECE            PIC X(10).                             
034          10 FFGCTA-NLIGNE            PIC X(03).                             
           05   FFGCTA-SUITE.                                                   
037          10 FFGCTA-DCOMPTA           PIC X(08).                             
045          10 FFGCTA-NUMFACT           PIC X(07).                             
052          10 FFGCTA-NSOCORIG          PIC X(03).                             
055          10 FFGCTA-NLIEUORIG         PIC X(03).                             
058          10 FFGCTA-NSOCDEST          PIC X(03).                             
061          10 FFGCTA-NLIEUDEST         PIC X(03).                             
064          10 FFGCTA-NATFACT           PIC X(05).                             
069          10 FFGCTA-CVENT             PIC X(05).                             
074          10 FFGCTA-NOECS             PIC X(05).                             
079          10 FFGCTA-DPIECE            PIC X(08).                             
087          10 FFGCTA-MONTANT           PIC S9(11)V99 COMP-3.                  
094          10 FFGCTA-SENS              PIC X(01).                             
095          10 FFGCTA-CTAUXTVA          PIC X(05).                             
100          10 FFGCTA-COMPTE            PIC X(06).                             
106          10 FFGCTA-SECTION           PIC X(06).                             
112          10 FFGCTA-RUBRIQUE          PIC X(06).                             
118          10 FFGCTA-LIBELLE           PIC X(20).                             
138          10 FFGCTA-CTYPPIECE         PIC X(02).                             
140          10 FFGCTA-CTYPLIGNE         PIC X(03).                             
143          10 FFGCTA-CNOMPGRM          PIC X(06).                             
149          10 FFGCTA-DECHEANC          PIC X(08).                             
157          10 FFGCTA-NSOCCOR           PIC X(03).                             
160          10 FFGCTA-NTIERS            PIC X(06).                             
166          10 FFGCTA-SGL               PIC X(01).                             
167          10 FFGCTA-FILLER            PIC X(14).                             
->180 *                                                                         
                                                                                
