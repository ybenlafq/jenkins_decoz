      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG25   EFG25                                              00000020
      ***************************************************************** 00000030
       01   EFG25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCPTAL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLIBCPTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBCPTAF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBCPTAI      PIC X(22).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCPTAL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSOCCPTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCCPTAF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCCPTAI      PIC X(30).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPAGEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPAGEEI   PIC XX.                                         00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPAGEMI   PIC XX.                                         00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MTIERSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTIERSF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTIERSI   PIC X(30).                                      00000370
           02 MTABECSI OCCURS   14 TIMES .                              00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCOMPTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTEF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCOMPTEI     PIC X(6).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPCOMPL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MTYPCOMPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MTYPCOMPF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MTYPCOMPI    PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNATUREL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNATUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNATUREF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNATUREI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRITEREL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCRITEREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCRITEREF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCRITEREI    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEFL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCOMPTEFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMPTEFF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCOMPTEFI    PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCAUXGL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MSOCAUXGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCAUXGF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSOCAUXGI    PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCEFFTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSOCEFFTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCEFFTF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSOCEFFTI    PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(2).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(73).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EFG25   EFG25                                              00000920
      ***************************************************************** 00000930
       01   EFG25O REDEFINES EFG25I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MFONCA    PIC X.                                          00001110
           02 MFONCC    PIC X.                                          00001120
           02 MFONCP    PIC X.                                          00001130
           02 MFONCH    PIC X.                                          00001140
           02 MFONCV    PIC X.                                          00001150
           02 MFONCO    PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MLIBCPTAA      PIC X.                                     00001180
           02 MLIBCPTAC PIC X.                                          00001190
           02 MLIBCPTAP PIC X.                                          00001200
           02 MLIBCPTAH PIC X.                                          00001210
           02 MLIBCPTAV PIC X.                                          00001220
           02 MLIBCPTAO      PIC X(22).                                 00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MSOCCPTAA      PIC X.                                     00001250
           02 MSOCCPTAC PIC X.                                          00001260
           02 MSOCCPTAP PIC X.                                          00001270
           02 MSOCCPTAH PIC X.                                          00001280
           02 MSOCCPTAV PIC X.                                          00001290
           02 MSOCCPTAO      PIC X(30).                                 00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MPAGEEA   PIC X.                                          00001320
           02 MPAGEEC   PIC X.                                          00001330
           02 MPAGEEP   PIC X.                                          00001340
           02 MPAGEEH   PIC X.                                          00001350
           02 MPAGEEV   PIC X.                                          00001360
           02 MPAGEEO   PIC 99.                                         00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGEMA   PIC X.                                          00001390
           02 MPAGEMC   PIC X.                                          00001400
           02 MPAGEMP   PIC X.                                          00001410
           02 MPAGEMH   PIC X.                                          00001420
           02 MPAGEMV   PIC X.                                          00001430
           02 MPAGEMO   PIC 99.                                         00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MTIERSA   PIC X.                                          00001460
           02 MTIERSC   PIC X.                                          00001470
           02 MTIERSP   PIC X.                                          00001480
           02 MTIERSH   PIC X.                                          00001490
           02 MTIERSV   PIC X.                                          00001500
           02 MTIERSO   PIC X(30).                                      00001510
           02 MTABECSO OCCURS   14 TIMES .                              00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MCOMPTEA     PIC X.                                     00001540
             03 MCOMPTEC     PIC X.                                     00001550
             03 MCOMPTEP     PIC X.                                     00001560
             03 MCOMPTEH     PIC X.                                     00001570
             03 MCOMPTEV     PIC X.                                     00001580
             03 MCOMPTEO     PIC 9(6).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MTYPCOMPA    PIC X.                                     00001610
             03 MTYPCOMPC    PIC X.                                     00001620
             03 MTYPCOMPP    PIC X.                                     00001630
             03 MTYPCOMPH    PIC X.                                     00001640
             03 MTYPCOMPV    PIC X.                                     00001650
             03 MTYPCOMPO    PIC X.                                     00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MNATUREA     PIC X.                                     00001680
             03 MNATUREC     PIC X.                                     00001690
             03 MNATUREP     PIC X.                                     00001700
             03 MNATUREH     PIC X.                                     00001710
             03 MNATUREV     PIC X.                                     00001720
             03 MNATUREO     PIC X(5).                                  00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCRITEREA    PIC X.                                     00001750
             03 MCRITEREC    PIC X.                                     00001760
             03 MCRITEREP    PIC X.                                     00001770
             03 MCRITEREH    PIC X.                                     00001780
             03 MCRITEREV    PIC X.                                     00001790
             03 MCRITEREO    PIC X(5).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MCOMPTEFA    PIC X.                                     00001820
             03 MCOMPTEFC    PIC X.                                     00001830
             03 MCOMPTEFP    PIC X.                                     00001840
             03 MCOMPTEFH    PIC X.                                     00001850
             03 MCOMPTEFV    PIC X.                                     00001860
             03 MCOMPTEFO    PIC 9(6).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MSOCAUXGA    PIC X.                                     00001890
             03 MSOCAUXGC    PIC X.                                     00001900
             03 MSOCAUXGP    PIC X.                                     00001910
             03 MSOCAUXGH    PIC X.                                     00001920
             03 MSOCAUXGV    PIC X.                                     00001930
             03 MSOCAUXGO    PIC X.                                     00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MSOCEFFTA    PIC X.                                     00001960
             03 MSOCEFFTC    PIC X.                                     00001970
             03 MSOCEFFTP    PIC X.                                     00001980
             03 MSOCEFFTH    PIC X.                                     00001990
             03 MSOCEFFTV    PIC X.                                     00002000
             03 MSOCEFFTO    PIC X(10).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(2).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(73).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
