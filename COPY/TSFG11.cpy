      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      * TS DE FG11                                                              
      ****************************************************************          
      * DESCRIPTION DE LA TS 'TS11' UTILISEE DANS TFG11 ET TFG14                
      ****************************************************************          
      *                                                                         
          02 TS11-NOM.                                                          
             03 FILLER             PIC X(4) VALUE 'FG11'.                       
             03 TS11-TERM          PIC X(4) VALUE SPACES.                       
          02 TS11-DATA.                                                         
             03 TS11-LIGNE-DATA    OCCURS 8.                                    
                05 TS11-BLOQUE         PIC X(01).                               
                05 TS11-ANCIEN         PIC X(01).                               
                05 TS11-FLAG-ANNUL     PIC X(01).                               
                05 TS11-MODIF-FG01     PIC X(01).                               
                05 TS11-MODIF-FG07     PIC X(01).                               
                05 TS11-CRITERE        PIC X(05).                               
                05 TS11-NLIGNE         PIC X(03).                               
                05 TS11-NLIGNE-9       REDEFINES TS11-NLIGNE PIC 9(03).         
                05 TS11-QTE            PIC S9(4)V9(3) COMP-3.                   
                05 TS11-PUHT           PIC S9(11)V9(2) COMP-3.                  
                05 TS11-MONTHT         PIC S9(11)V9(2) COMP-3.                  
                05 TS11-TTVA           PIC X(05).                               
                05 TS11-MONTTVA        PIC S9(11)V9(2) COMP-3.                  
                05 TS11-MONTTTC        PIC S9(11)V9(2) COMP-3.                  
                05 TS11-MONTESC        PIC S9(11)V9(2) COMP-3.                  
                05 TS11-DESIGN         PIC X(40).                               
                05 TS11-COMMENT        PIC X(01).                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 TS11-ITEM-FG14      PIC S9(4) COMP.                          
      *--                                                                       
                05 TS11-ITEM-FG14      PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
100*8 *   02 TS11-LONG           PIC S9(4) COMP VALUE +800.                     
      *                                                                         
      *--                                                                       
          02 TS11-LONG           PIC S9(4) COMP-5 VALUE +800.                   
                                                                                
      *}                                                                        
