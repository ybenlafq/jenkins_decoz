           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGANN DEF� TYPES DE FRAIS ANNEXES      *        
      *----------------------------------------------------------------*        
       01  RVGA01LR.                                                            
           05  FGANN-CTABLEG2    PIC X(15).                                     
           05  FGANN-CTABLEG2-REDEF REDEFINES FGANN-CTABLEG2.                   
               10  FGANN-CFRAIS          PIC X(06).                             
               10  FGANN-NATURE          PIC X(05).                             
           05  FGANN-WTABLEG     PIC X(80).                                     
           05  FGANN-WTABLEG-REDEF  REDEFINES FGANN-WTABLEG.                    
               10  FGANN-WACTIF          PIC X(01).                             
               10  FGANN-BASEMT          PIC X(01).                             
               10  FGANN-CTVA            PIC X(01).                             
               10  FGANN-TYPDOC          PIC X(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01LR-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGANN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGANN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGANN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGANN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
 
      *}                                                                        
