      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TFG20 (MENU)             TR: FG20  *    00020000
      *                                                            *    00030000
      *           POUR L'ADMINISTATION DES DONNEES                 *    00040000
      **************************************************************    00050000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00060000
      **************************************************************    00070000
      *                                                                 00080000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00090000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00100000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00110000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00120000
      *                                                                 00130000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00140000
      * COMPRENANT :                                                    00150000
      * 1 - LES ZONES RESERVEES A AIDA                                  00160000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00170000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00180000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00190000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00200000
      *                                                                 00210000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00220000
      * PAR AIDA                                                        00230000
      *                                                                 00240000
      *-------------------------------------------------------------    00250000
      *                                                                 00260000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-FG20-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00270000
      *--                                                                       
       01  COM-FG20-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00280000
       01  Z-COMMAREA.                                                  00290000
      *                                                                 00300000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00310000
          02 FILLER-COM-AIDA      PIC X(100).                           00320000
      *                                                                 00330000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00340000
          02 COMM-CICS-APPLID     PIC X(8).                             00350000
          02 COMM-CICS-NETNAM     PIC X(8).                             00360000
          02 COMM-CICS-TRANSA     PIC X(4).                             00370000
      *                                                                 00380000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00390000
          02 COMM-DATE-SIECLE     PIC XX.                               00400000
          02 COMM-DATE-ANNEE      PIC XX.                               00410000
          02 COMM-DATE-MOIS       PIC XX.                               00420000
          02 COMM-DATE-JOUR       PIC XX.                               00430000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00440000
          02 COMM-DATE-QNTA       PIC 999.                              00450000
          02 COMM-DATE-QNT0       PIC 99999.                            00460000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00470000
          02 COMM-DATE-BISX       PIC 9.                                00480000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00490000
          02 COMM-DATE-JSM        PIC 9.                                00500000
      *   LIBELLES DU JOUR COURT - LONG                                 00510000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00520000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00530000
      *   LIBELLES DU MOIS COURT - LONG                                 00540000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00550000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00560000
      *   DIFFERENTES FORMES DE DATE                                    00570000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00580000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00590000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00600000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00610000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00620000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00630000
      *   DIFFERENTES FORMES DE DATE                                    00640000
          02 COMM-DATE-SEMSS      PIC X(02).                            00650000
          02 COMM-DATE-SEMAA      PIC X(02).                            00660000
          02 COMM-DATE-SEMNU      PIC X(02).                            00670000
          02 COMM-DATE-FILLER     PIC X(08).                            00680000
      *                                                                 00690000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00700000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00710000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00720000
      *                                                                 00730000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00740000
      *                                                                 00750000
      *            TRANSACTION FG20 : ADMINISTRATION DES DONNEES      * 00760000
      *                                                                 00770005
          02 COMM-FG20-APPLI.                                           00780000
      *                                                                 00790000
             03 COMM-FG20-REFSOC.                                       00800007
                05 COMM-FG20-NSOC         PIC X(03).                    00801007
                05 COMM-FG20-NLIEU        PIC X(03).                    00810007
                05 COMM-FG20-LLIEU        PIC X(20).                    00820007
                05 COMM-FG20-NSOCCOMPT    PIC X(03).                    00830007
                05 COMM-FG20-NLIEUCOMPT   PIC X(03).                    00840007
             03 COMM-FG20-NATSOC       PIC X(01).                       00850005
             03 COMM-FG20-LIBERR       PIC X(78).                       00850006
             03 COMM-FG20-CIMP         PIC X(04).                       00860005
             03 COMM-FG20-SELEC        PIC X.                           00860006
             03 COMM-FG20-NFRAIS       PIC X(06).                       00870005
             03 COMM-FG20-FILLER1      PIC X(07).                       00880005
             03 COMM-FG20-NSOC4        PIC X(03).                       00890005
             03 COMM-FG20-LSOC4        PIC X(25).                       00890006
             03 COMM-FG20-NEXERC       PIC X(04).                       00900005
             03 COMM-FG20-NPER         PIC X(03).                       00910005
             03 COMM-FG20-FONCTION     PIC X(03).                       00920005
             03 COMM-FG20-CTYPE        PIC X(03).                       00930005
             03 COMM-FG20-TTES         PIC X(01).                       00930006
AM           03 COMM-FG20-DVALEUR      PIC X(08).                       00950008
AP           03 COMM-FG20-SECTION      PIC X(06).                       00950008
             03 COMM-FG20-FILLER       PIC X(39).                       00950008
      *                                                                 00960005
      *------------------------------ ZONE COMMUNE AUX SOUS-PROGRAMMES  00970000
             03 COMM-FG20-SSPRGS         PIC X(3500).                   00980008
      *                                                                 00990005
                                                                        01020005
      *------------------------------ ZONE SPECIFIQUE TFG22             01040006
             03 COMM-FG22                REDEFINES COMM-FG20-SSPRGS.    01040007
                                                                        01040008
                05 COMM-FG22-TABFRAIS.                                  01040009
                   07 COMM-FG22-NBPOST         PIC 9(2).                01040010
                   07 COMM-FG22-NFRAIS         PIC X(6).                01050005
                   07 COMM-FG22-NATURE-FRAIS   OCCURS 14.               01060005
                      10 COMM-FG22-NATURE         PIC X(5).             01070005
                      10 COMM-FG22-LIBNAT         PIC X(80).            01080005
                      10 COMM-FG22-TAUX           PIC 9(3)V99.          01090005
                      10 COMM-FG22-TAUX-MAJ       PIC 9(3)V99.          01100005
                      10 COMM-FG22-TYPTAUX        PIC X(1).             01110005
                      10 COMM-FG22-EXIST          PIC X(1).             01120005
                05 COMM-FG22-CFRAIS-S          PIC X(6).                01130005
                05 COMM-FG22-NATURE-S          PIC X(5).                01140005
                05 COMM-FG22-TYPTAUX-S         PIC X(1).                01150005
                05 COMM-FG22-NUITEM-LU         PIC S9(4) COMP-3.        01160005
                05 COMM-FG22-NUITEM-ECR        PIC S9(4) COMP-3.        01170005
                05 COMM-FG22-CURS              PIC X(1).                01171009
                05 COMM-FG22-FILLER      PIC X(2115).                   01180009
                                                                        01190005
      *------------------------------ ZONE SPECIFIQUE TFG23             01200000
             03 COMM-FG23                REDEFINES COMM-FG20-SSPRGS.    01210010
                 05 COMM-FG23-TAB-NJRNG.                                01220011
                    07 COMM-FG23-NBPOST PIC 9(2).                       01230011
                    07 COMM-FG23-TAB       OCCURS 15.                   01240012
                       10 COMM-FG23-NJRNG PIC X(10).                    01240013
                       10 COMM-FG23-LJRNG PIC X(20).                    01240014
                       10 COMM-FG23-NJRN PIC X(10).                     01240015
                       10 COMM-FG23-NJRN-MAJ PIC X(10).                 01240016
                       10 COMM-FG23-EXIST-FG12 PIC X(1).                01240017
                05 COMM-FG23-NUITEM-LU         PIC S9(2) COMP-3.        01249213
                05 COMM-FG23-NUITEM-ECR        PIC S9(2) COMP-3.        01249313
                05 COMM-FG23-FILLER      PIC X(2729).                   01260015
                                                                        01270005
      *------------------------------ ZONE SPECIFIQUE TFG24             01270006
             03 COMM-FG24                REDEFINES COMM-FG20-SSPRGS.    01270007
                 05 COMM-FG24-TAB-EXERC.                                01270008
                    07 COMM-FG24-NBPOST PIC 9(2).                       01270009
                    07 COMM-FG24-TAB       OCCURS 14.                   01270010
                       10 COMM-FG24-EXERCICE PIC X(4).                  01270020
                       10 COMM-FG24-PERIODE PIC X(3).                   01270030
                       10 COMM-FG24-DDEBUT PIC X(10).                   01270040
                       10 COMM-FG24-DFIN PIC X(10).                     01270050
                       10 COMM-FG24-LPERIODE PIC X(20).                 01270060
                       10 COMM-FG24-EXERSOC PIC X(4).                   01270070
                       10 COMM-FG24-EXERSOC-MAJ PIC X(4).               01270080
                       10 COMM-FG24-PERSOC PIC X(3).                    01270090
                       10 COMM-FG24-PERSOC-MAJ PIC X(3).                01270100
                       10 COMM-FG24-EXIST-FG11 PIC X(1).                01270200
                05 COMM-FG24-NUITEM-LU         PIC S9(3) COMP-3.        01270300
                05 COMM-FG24-NUITEM-ECR        PIC S9(3) COMP-3.        01270400
                05 COMM-FG24-FILLER      PIC X(2626).                   01270600
                                                                        01270700
      *------------------------------ ZONE SPECIFIQUE TFG25             01270800
             03 COMM-FG25                REDEFINES COMM-FG20-SSPRGS.    01270900
                 05 COMM-FG25-SOC-DEST  PIC XXX.                        01271000
                 05 COMM-FG25-LIB-DEST  PIC X(20).                      01271001
                 05 COMM-FG25-TAB-COMPT.                                01271010
                    07 COMM-FG25-NBPOST PIC 9(2).                       01271100
                    07 COMM-FG25-TAB       OCCURS 14.                   01271200
                       10 COMM-FG25-COMPTECG PIC X(6).                  01271210
                       10 COMM-FG25-TYPCOMPT PIC X(1).                  01271220
      *                10 COMM-FG25-DATEFF   PIC X(8).                  01271230
                       10 COMM-FG25-NATURE   PIC X(5).                  01271240
                       10 COMM-FG25-CVENT    PIC X(5).                  01271241
      *                10 COMM-FG25-CRITERE3 PIC X(5).                  01271242
                       10 COMM-FG25-SOCIETE PIC X(3).                   01271260
                       10 COMM-FG25-SOCIETE-MAJ PIC X(3).               01271270
                       10 COMM-FG25-COMPTE-F PIC X(6).                  01271280
                       10 COMM-FG25-COMPTE-F-MAJ PIC X(6).              01271290
                       10 COMM-FG25-AUXILIAIR PIC X(1).                 01271291
                       10 COMM-FG25-AUXILIAIR-MAJ PIC X(1).             01271292
                       10 COMM-FG25-DATEFF-F PIC X(8).                  01271293
                       10 COMM-FG25-DATEFF-F-MAJ PIC X(8).              01271294
                       10 COMM-FG25-EXIST-FG13 PIC X(1).                01271299
      *                                                                 01271300
                 05 COMM-FG25-NUITEM-LU         PIC S9(3) COMP-3.       01272300
                 05 COMM-FG25-NUITEM-ECR        PIC S9(3) COMP-3.       01272400
                                                                        01272600
                 05 COMM-FG25-SOCGEST     PIC X(3).                             
                 05 COMM-FG25-LIEUGEST    PIC X(3).                             
                 05 COMM-FG25-LIBGEST     PIC X(20).                            
                                                                        01272600
                 05 COMM-FG25-FILLER      PIC X(2689).                  01272500
                                                                        01272600
                                                                        01270700
      *------------------------------ ZONE SPECIFIQUE TFG26             01270800
             03 COMM-FG26                REDEFINES COMM-FG20-SSPRGS.    01270900
                05  COMM-FG26-ACCES       PIC X(1).                             
                    88 COMM-FG26-CRE          VALUE '0'.                        
                    88 COMM-FG26-MAJ          VALUE '1'.                        
                05  COMM-FG26-NSOC        PIC X(3).                             
                05  COMM-FG26-NETAB       PIC X(3).                             
                05  COMM-FG26-TYPREMSOC   PIC X(2).                             
                05  COMM-FG26-TYPREMTR    PIC X(2).                             
                05  COMM-FG26-TYPERGL     PIC X(2).                             
                05  COMM-FG26-NATREMUN    PIC X(5).                             
                05  COMM-FG26-CTAUXTVA    PIC X(5).                             
                05  COMM-FG26-NBJOURS     PIC S9(3) COMP-3.                     
                05  COMM-FG26-NJRNCTR     PIC X(10).                            
                05  COMM-FG26-NJRNPTR     PIC X(10).                            
                05  COMM-FG26-NJRNFTR     PIC X(10).                            
                05  COMM-FG26-NJRNC       PIC X(10).                            
                05  COMM-FG26-NJRNP       PIC X(10).                            
LG=85           05  COMM-FG26-NJRNF       PIC X(10).                            
      *                                                                         
                05  COMM-FG26-FILLER      PIC X(3415).                  01271000
                                                                        01272600
      *------------------------------ ZONE SPECIFIQUE TFG27             01270800
             03 COMM-FG27                REDEFINES COMM-FG20-SSPRGS.    01270900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FG27-PAGE       PIC S9(4) COMP.                         
      *--                                                                       
                05 COMM-FG27-PAGE       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FG27-TOTPAGE    PIC S9(4) COMP.                         
      *--                                                                       
                05 COMM-FG27-TOTPAGE    PIC S9(4) COMP-5.                       
      *}                                                                        
                05 COMM-FG27-NSOCTR     PIC X(03).                              
LG=10           05 COMM-FG27-NETABTR    PIC X(03).                              
      *                                                                         
                05 COMM-FG27-FILLER     PIC X(3490).                    01271000
                                                                        01330005
      *------------------------------ ZONE SPECIFIQUE TFG28             01270800
                05 COMM-FG28          REDEFINES COMM-FG27-FILLER.       01270900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            07 COMM-FG28-PAGE       PIC S9(4) COMP.                      
      *--                                                                       
                   07 COMM-FG28-PAGE       PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            07 COMM-FG28-TOTPAGE    PIC S9(4) COMP.                      
      *--                                                                       
                   07 COMM-FG28-TOTPAGE    PIC S9(4) COMP-5.                    
      *}                                                                        
                   07 COMM-FG28-NSOC       PIC X(3).                            
                   07 COMM-FG28-LIBELLE    PIC X(20).                           
                   07 COMM-FG28-SENS       PIC X.                               
                   07 COMM-FG28-MTVALEUR   PIC 9(11)V99 COMP-3.                 
                   07 COMM-FG28-DVALEUR    PIC X(8).                            
                   07 COMM-FG28-FLAG       PIC X.                               
LG=52              07 COMM-FG28-DINIT      PIC X(8).                            
      *                                                                         
                   07 COMM-FG28-FILLER     PIC X(3438).                 01271000
      *                                                                         
      *------------------------------ ZONE SPECIFIQUE TFG29             01040006
             03 COMM-FG29                REDEFINES COMM-FG20-SSPRGS.    01040007
                                                                        01040008
                05 COMM-FG29-PAGE        PIC 99.                                
                05 COMM-FG29-PAGEMAX     PIC 99.                                
                05 COMM-FG29-SOCCOP      PIC X(03).                             
                05 COMM-FG29-LIEUCOP     PIC X(03).                             
                05 COMM-FG29-LLIEUCOP    PIC X(03).                             
                05 COMM-FG29-NSOCCOMPT2  PIC X(03).                             
                05 COMM-FG29-NLIEUCOMPT2 PIC X(03).                             
                05 COMM-FG29-FILLER      PIC X(3481).                   01180009
                                                                                
                                                                        01190005
