      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      ****************************************************************          
      *                                                              *          
      *TOP SECRET SECURITY CICS PARAMETER LIST.       V5L1           *          
      *                                                              *          
      *NOTE FIELD TSSHEAD MUST HAVE THE VALUE 'TCPLV5L1'             *          
      *04/29/94 - UPDATED BY QA, SEE CONTACT 2402388 AND 2435039.    *          
      *11/12/98 - UPDATED BY QA, SEE CONTACT 6570558                 *          
      ****************************************************************          
      *    THE COPY STATEMENT FOR THIS COPYBOOK MUST SUPPLY A LENGTH *          
      *    FOR THE FIELD TSSRTN AS FOLLOWS:                          *          
      *       COPY TSSCPLC REPLACING ==:RTLN:== BY ==NNNNN==         *          
      *    THE VALUE EMPLOYED FOR THIS LENGTH MUST BE SUFFICIENT TO  *          
      *    HOLD ANY REQUEST WITH RCLASS=RESLIST FACLIST FLDXTR FLDUPD*          
      *    THE VALUE IN TSSLRTN MUST NOT EXCEED THE LENGTH SUPPLIED. *          
      *                                                              *          
      *    FOR PROGRAMS WHICH USE SUCH RCLASS REQUESTS, THE VALUE    *          
      *    RTLN MUST LIE BETWEEN 1024 AND 32654 BYTES.               *          
      *                                                              *          
      *    FOR PROGRAMS WHICH DO NOT USE SUCH REQUESTS, RTLN MAY     *          
      *    BE SET TO 256.                                            *          
      ****************************************************************          
       01  TSSCPL.                                                              
           02  TSSHEAD        PICTURE X(8) VALUE 'TCPLV5L1'.                    
           02  TSSCLASS       PICTURE X(8).                                     
           02  TSSRNAME       PICTURE X(44).                                    
           02  TSSPPGM        PICTURE X(8).                                     
           02  TSSACC         PICTURE X(8).                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSRC          PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
           02  TSSRC          PICTURE S9(4) COMP-5.                             
               88  TSSROK     VALUE +00.                                        
               88  TSSRND     VALUE +04.                                        
               88  TSSRNA     VALUE +08.                                        
               88  TSSRIPL    VALUE +12.                                        
               88  TSSRENV    VALUE +16.                                        
               88  TSSRINAC   VALUE +20.                                        
               88  TSSRXSAC   VALUE +24.                                        
               88  TSSRSEGT   VALUE +28.                                        
               88  TSSRFDTE   VALUE +32.                                        
               88  TSSRUSRF   VALUE +36.                                        
               88  TSSRGF     VALUE +40.                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSSTAT        PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
           02  TSSSTAT        PICTURE S9(4) COMP-5.                             
               88  TSSSDEF    VALUE +00.                                        
               88  TSSSUND    VALUE +04.                                        
               88  TSSSNSO    VALUE +08.                                        
               88  TSSSIDT    VALUE +12.                                        
           02  TSSCRC         PICTURE X(2).                                     
           02  TSSCSTAT       PICTURE X(2).                                     
           02  TSSCACEE       PICTURE X(4).                                     
           02  TSSVOL         PICTURE X(6).                                     
           02  TSSLOG         PICTURE X.                                        
           02  FILLER         PICTURE X(2).                                     
           02  TSSDRC         PICTURE X.                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSLRTN        PICTURE 9(9) COMP VALUE 1024.                     
      *--                                                                       
           02  TSSLRTN        PICTURE 9(9) COMP-5 VALUE 1024.                   
           02  FILLER         PICTURE X(12).                                    
           02  TSSRTN         PIC X(1024).                                      
      *============================================================             
      *   TSSRTN REDEFINITION FOR TSSRCLASS OTHER THAN                          
      *          FACLIST,RESLIST,FLDXTR,FLDUPD                                  
      *============================================================             
           02  TSSOTHR REDEFINES TSSRTN.                                        
               05  TSSACIDA       PICTURE X(8).                                 
               05  TSSFAC         PICTURE X(8).                                 
               05  TSSMODE        PICTURE X(8).                                 
               05  TSSTYPE        PICTURE X(8).                                 
               05  TSSTERM        PICTURE X(8).                                 
               05  TSSSYS         PICTURE X(8).                                 
               05  TSSACIDF       PICTURE X(32).                                
               05  TSSDEPTA       PICTURE X(8).                                 
               05  TSSDEPTF       PICTURE X(32).                                
               05  TSSDIVA        PICTURE X(8).                                 
               05  TSSDIVF        PICTURE X(32).                                
               05  TSSZONEA       PICTURE X(8).                                 
               05  TSSZONEF       PICTURE X(32).                                
      *============================================================             
      *   TSSRTN REDEFINITION FOR TSSRCLASS RESLIST                             
      *          BECAUSE THE RESOURCE ENTRY DATA IS VARIABLE                    
      *          AND DEPENDS ON A COMPUTED LENGTH, UNSTRING                     
      *          MUST BE USED TO DEBLOCK TSSRTN                                 
      *============================================================             
           02  TSSRES-DATA REDEFINES TSSRTN.                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  TSSRESFLDS PICTURE S9(9) COMPUTATIONAL.                      
      *--                                                                       
               05  TSSRESFLDS PICTURE S9(9) COMP-5.                             
      *============================================================             
      *       FIRST 2 LENGTHS BELOW REPRESENT THE TRADITIONAL LENGTH            
      *       OF TSSCPL WHEN TSSRTN HAS LENGTH 256 AND 1024 RESP.               
      *       TO COMPUTE DYNAMIC LENGTH USING TSSLRTN                           
      *       FOR FACLIST, RESLIST, FLDXTR OR FLDUPD ONLY                       
      *            COMPUTE TSSPLENX                                             
      *               = 114 + TSSLRTN                                           
      *============================================================             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSPLEN            PICTURE S9(4) COMPUTATIONAL VALUE +370.           
      *--                                                                       
       01  TSSPLEN            PICTURE S9(4) COMP-5 VALUE +370.                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSPLEN2           PICTURE S9(4) COMPUTATIONAL VALUE +1138.          
      *--                                                                       
       01  TSSPLEN2           PICTURE S9(4) COMP-5 VALUE +1138.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSPLENX           PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
       01  TSSPLENX           PICTURE S9(4) COMP-5.                             
      *============================================================             
      *       FACLIST ENTRIES ARE 8 CHARACTERS IN LENGTH                        
      *       UNSTRING SUCCESSIVE FACILITIES IN TSSRTN                          
      *           UNSTRING TSSRTN                                               
      *              INTO TSSFACL-ENTRY                                         
      *              WITH POINTER TSSRES-PTR.                                   
      *       AS UNSTRING DEBLOCKS EACH ENTRY, POINTER IS INCREMENTED           
      *============================================================             
       01  TSSFACL-ENTRY      PICTURE X(8).                                     
      *============================================================             
      *       RESLIST ENTRIES ARE VARIABLE IN LENGTH                            
      *       UNSTRING SUCCESSIVE RESOURCE-ENTRIES IN TSSRTN                    
      *       UNSTRING POINTER SHOULD START AT POSITION 5 OF TSSRTN             
      *                (BYPASSING TSSRESFLDS COUNTER)                           
      *       AS UNSTRING DEBLOCKS EACH ENTRY, POINTER IS INCREMENTED           
      ***------------------------------------------------                       
      ***   UNLOAD LENGTH FIELD INTO RSRC ARRAY TO SETUP                        
      ***------------------------------------------------                       
      *     UNSTRING TSSRTN                                                     
      *              INTO TSSRES-LNTH-X                                         
      *              WITH POINTER TSSRES-PTR.                                   
      ***------------------------------------------------                       
      ***   UNLOAD GENERIC/NONGENERIC FLAG AND RSRC NAME                        
      ***------------------------------------------------                       
      *     UNSTRING TSSRTN                                                     
      *              INTO TSSRES-FLAG TSSRES-RSRC                               
      *              WITH POINTER TSSRES-PTR.                                   
      ***------------------------------------------------                       
      *============================================================             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSRES-INDEX       PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
       01  TSSRES-INDEX       PICTURE S9(4) COMP-5.                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSRES-PTR         PICTURE S9(4) COMPUTATIONAL VALUE 5.              
      *--                                                                       
       01  TSSRES-PTR         PICTURE S9(4) COMP-5 VALUE 5.                     
       01  TSSRES-ENTRY.                                                        
           02  TSSRES-LNTH-X   PICTURE X(2).                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSRES-LNTH     REDEFINES TSSRES-LNTH-X                          
      *                        PICTURE S9(4) COMPUTATIONAL.                     
      *--                                                                       
           02  TSSRES-LNTH     REDEFINES TSSRES-LNTH-X                          
                               PICTURE S9(4) COMP-5.                            
           02  TSSRES-FLAG     PICTURE X.                                       
               88  TSSRES-NGNRC             VALUE LOW-VALUE.                    
           02  TSSRES-RSRC.                                                     
               05  FILLER     PICTURE X(1)                                      
                              OCCURS 1 TO 255 TIMES                             
                              DEPENDING ON TSSRES-LNTH.                         
      *============================================================             
      *    TSSRTN TARGET STRUCTURE FOR FLDXTR/FLDUPD                            
      ***------------------------------------------------                       
      ***   FOR FLDXTR                                                          
      ***------------------------------------------------                       
      ***   UNLOAD LENGTH FIELD INTO FLDXTR STRUCTURE                           
      ***------------------------------------------------                       
      *     UNSTRING TSSRTN                                                     
      *              INTO TSSFLDLNTH-X                                          
      *              WITH POINTER TSSRES-PTR.                                   
      ***------------------------------------------------                       
      ***   UNLOAD GENERIC/NONGENERIC FLAG AND RSRC NAME                        
      ***------------------------------------------------                       
      *     UNSTRING TSSRTN                                                     
      *              INTO TSSFLDDATA                                            
      *              WITH POINTER TSSRES-PTR.                                   
      ***------------------------------------------------                       
      ***------------------------------------------------                       
      ***   FOR FLDUPD                                                          
      ***------------------------------------------------                       
      *     MOVE FIELD-LENGTH TO TSSFLDLNTH                                     
      *     MOVE FIELD-DATA TO TSSFLDDATA                                       
      *     MOVE TSSFLD-ENTRY TO TSSRTN                                         
      *============================================================             
       01  TSSFLD-ENTRY.                                                        
           02  TSSFLDLNTH-X   PICTURE X(4).                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSFLDLNTH REDEFINES TSSFLDLNTH-X                                
      *                       PICTURE S9(9) COMPUTATIONAL.                      
      *--                                                                       
           02  TSSFLDLNTH REDEFINES TSSFLDLNTH-X                                
                              PICTURE S9(9) COMP-5.                             
           02  TSSFLDDATA.                                                      
               05  FILLER     PICTURE X(1)                                      
                   OCCURS 1 TO 1024 TIMES                                       
                   DEPENDING ON TSSFLDLNTH.                                     
      *                                                                         
      * DESCRIPTION USER-AREA-FOR-INSTALLATION-DATA   - DARTY NCP               
      *                                                                         
       01  TSS-AREA-INST.                                                       
           02  TSS-PRT1-COD       PIC X(004).                                   
           02  TSS-PRT1-TYP       PIC X(001).                                   
           02  TSS-AREA-FIL       PIC X(251).                                   
      *------------------------------------------------------*                  
      * ===> ATTENTION <===                                  *                  
      * CETTE DESCRIPTION COBOL-TSSCPLC A UNE CORRESPONDANCE *                  
      * PL1-TSSCPLP                                          *                  
      * TOUTES LES MODIFICATIONS DOIVENT ETRE RECIPROQUES.   *                  
      *------------------------------------------------------*                  
                                                                                
