           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG0600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0600.                                                            
           02  FG06-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FG06-CCHRONO                                                     
               PIC X(0002).                                                     
           02  FG06-NUMFACT                                                     
               PIC X(0007).                                                     
           02  FG06-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FG06-NSEQ                                                        
               PIC X(0002).                                                     
           02  FG06-DANNUL                                                      
               PIC X(0008).                                                     
           02  FG06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FG06-LCOMMENT                                                    
               PIC X(0078).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG0600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-CCHRONO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-CCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-NUMFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-NUMFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG06-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG06-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
