      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
       01  TS-FG27-LONG             PIC S9(5) COMP-3 VALUE +812.                
       01  TS-FG27-RECORD.                                                      
           05 TS-FG27-LIGNE         OCCURS 14.                                  
              10 TS-FG27-NSOC       PIC X(3).                                   
              10 TS-FG27-LIBELLE    PIC X(20).                                  
              10 TS-FG27-WALIM      PIC X.                                      
              10 TS-FG27-DSOLDE     PIC X(8).                                   
              10 TS-FG27-DT-CR      PIC X.                                      
              10 TS-FG27-MTSOLDE    PIC S9(11)V99 COMP-3.                       
              10 TS-FG27-INIT-SENS  PIC X.                                      
              10 TS-FG27-INIT-SOLDE PIC S9(11)V99 COMP-3.                       
              10 TS-FG27-WAJOUT     PIC X.                                      
              10 TS-FG27-FLAG       PIC X.                                      
              10 TS-FG27-DATE-INIT  PIC X(8).                                   
                                                                                
