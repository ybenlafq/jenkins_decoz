      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *---------------------------------------------------------                
      *   ENREGISTREMENT VERS LA COMPTA GROUPE                                  
      *---------------------------------------------------------                
      *                                                                         
       01  IDFAC3.                                                              
           02  IDFAC3-CCHRONO PIC X(0002).                                      
      *        CODE CHRONO DACEM 'MD',                                          
           02  IDFAC3-NUMFACT PIC 9(0007).                                      
      *        NUMERO DE FACTURE                                                
           02  IDFAC3-NSOCORIG  PIC X(0003).                                    
      *        CODE SOCIETE ORIGINE                                             
           02  IDFAC3-NLIEUORIG  PIC X(0003).                                   
      *        CODE LIEU ORIGINE                                                
           02  IDFAC3-CCLIENT.                                                  
      *        CODE CLIENT  DACEM                                               
               10  IDFAC3-NSOCDEST  PIC X(0003).                                
      *            CODE SOCIETE DEST                                            
               10  IDFAC3-NLIEUDEST  PIC X(0003).                               
      *            CODE LIEU DEST                                               
           02  IDFAC3-CTYPFACT PIC X(0002).                                     
      *        TYPE DE FACTURE CONVERTIR PAR FGTYP                              
           02  IDFAC3-NATFACT PIC X(0005).                                      
      *        NATURE EX MUTDA, FRGES, TRANS                                    
           02  IDFAC3-CVENT   PIC X(0005).                                      
      *        CRITERES DE VENTILATION EX ACCBL ACCBR PDEBL PDEBR               
           02  IDFAC3-NUMORIG PIC 9(0007).                                      
      *        NUMERO DE COMMANDE CLIENT                                        
           02  IDFAC3-DMOUV   PIC X(0008).                                      
      *        DATE DU MOUVEMENT                                                
           02  IDFAC3-MONTHT PIC S9(11)V9(0002) COMP-3.                         
      *        MONTANT HORS TAXES                                               
           02  IDFAC3-CTAUXTVA PIC X(0005).                                     
      *        CODE TVA                                                         
           02  IDFAC3-TXESCPT PIC S9(3)V9(0002) COMP-3.                         
      *        TAUX ESCOMPTE                                                    
           02  IDFAC3-DCREAT PIC X(0008).                                       
      *        DATE DE CREATION DE LA LIGNE                                     
           02  IDFAC3-QNBPIECES PIC S9(0005) COMP-3.                            
      *        NOMBRE DE PIECES                                                 
           02  IDFAC3-SAUVE  PIC X(0001).                                       
      *        'O' ON N'ECRASE PAS                                              
                                                                                
