           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGCTL SECTION OBLIG EN SAISIE TP O/N   *        
      *----------------------------------------------------------------*        
       01  RVGA01T0.                                                            
           05  FGCTL-CTABLEG2    PIC X(15).                                     
           05  FGCTL-CTABLEG2-REDEF REDEFINES FGCTL-CTABLEG2.                   
               10  FGCTL-NSOC            PIC X(03).                             
               10  FGCTL-NLIEU           PIC X(03).                             
           05  FGCTL-WTABLEG     PIC X(80).                                     
           05  FGCTL-WTABLEG-REDEF  REDEFINES FGCTL-WTABLEG.                    
               10  FGCTL-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01T0-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGCTL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGCTL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGCTL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGCTL-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
