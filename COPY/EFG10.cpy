      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG10   EFG10                                              00000020
      ***************************************************************** 00000030
       01   EFG10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * OPTION                                                          00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MZONCMDI  PIC X.                                          00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MSOCCI    PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCL   COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MLSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCCF   PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLSOCCI   PIC X(20).                                      00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSERVL      COMP PIC S9(4).                            00000290
      *--                                                                       
           02 MLIBSERVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBSERVF      PIC X.                                     00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MLIBSERVI      PIC X(9).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVL    COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MSERVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSERVF    PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MSERVI    PIC X(5).                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSERVL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLSERVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSERVF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLSERVI   PIC X(20).                                      00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MNSOCI    PIC X(3).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MNLIEUI   PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MLLIEUI   PIC X(20).                                      00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCTYPEI   PIC X(2).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATUREL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNATUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNATUREF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNATUREI  PIC X(5).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPIECEL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MDPIECEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPIECEF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MDPIECEI  PIC X(10).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMFACTL      COMP PIC S9(4).                            00000650
      *--                                                                       
           02 MNUMFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMFACTF      PIC X.                                     00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNUMFACTI      PIC X(7).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCORL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MSOCCORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCCORF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MSOCCORI  PIC X(3).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MDDEBUTI  PIC X(10).                                      00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCIMPI    PIC X(4).                                       00000800
      * MESSAGE ERREUR                                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(78).                                      00000850
      * CODE TRANSACTION                                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      * CICS DE TRAVAIL                                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCICSI    PIC X(5).                                       00000950
      * NETNAME                                                         00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNETNAMI  PIC X(8).                                       00001000
      * CODE TERMINAL                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(5).                                       00001050
      ***************************************************************** 00001060
      * SDF: EFG10   EFG10                                              00001070
      ***************************************************************** 00001080
       01   EFG10O REDEFINES EFG10I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
      * DATE DU JOUR                                                    00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
      * HEURE                                                           00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
      * OPTION                                                          00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MZONCMDA  PIC X.                                          00001290
           02 MZONCMDC  PIC X.                                          00001300
           02 MZONCMDP  PIC X.                                          00001310
           02 MZONCMDH  PIC X.                                          00001320
           02 MZONCMDV  PIC X.                                          00001330
           02 MZONCMDO  PIC X.                                          00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MSOCCA    PIC X.                                          00001360
           02 MSOCCC    PIC X.                                          00001370
           02 MSOCCP    PIC X.                                          00001380
           02 MSOCCH    PIC X.                                          00001390
           02 MSOCCV    PIC X.                                          00001400
           02 MSOCCO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLSOCCA   PIC X.                                          00001430
           02 MLSOCCC   PIC X.                                          00001440
           02 MLSOCCP   PIC X.                                          00001450
           02 MLSOCCH   PIC X.                                          00001460
           02 MLSOCCV   PIC X.                                          00001470
           02 MLSOCCO   PIC X(20).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLIBSERVA      PIC X.                                     00001500
           02 MLIBSERVC PIC X.                                          00001510
           02 MLIBSERVP PIC X.                                          00001520
           02 MLIBSERVH PIC X.                                          00001530
           02 MLIBSERVV PIC X.                                          00001540
           02 MLIBSERVO      PIC X(9).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MSERVA    PIC X.                                          00001570
           02 MSERVC    PIC X.                                          00001580
           02 MSERVP    PIC X.                                          00001590
           02 MSERVH    PIC X.                                          00001600
           02 MSERVV    PIC X.                                          00001610
           02 MSERVO    PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLSERVA   PIC X.                                          00001640
           02 MLSERVC   PIC X.                                          00001650
           02 MLSERVP   PIC X.                                          00001660
           02 MLSERVH   PIC X.                                          00001670
           02 MLSERVV   PIC X.                                          00001680
           02 MLSERVO   PIC X(20).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNSOCA    PIC X.                                          00001710
           02 MNSOCC    PIC X.                                          00001720
           02 MNSOCP    PIC X.                                          00001730
           02 MNSOCH    PIC X.                                          00001740
           02 MNSOCV    PIC X.                                          00001750
           02 MNSOCO    PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNLIEUA   PIC X.                                          00001780
           02 MNLIEUC   PIC X.                                          00001790
           02 MNLIEUP   PIC X.                                          00001800
           02 MNLIEUH   PIC X.                                          00001810
           02 MNLIEUV   PIC X.                                          00001820
           02 MNLIEUO   PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLLIEUA   PIC X.                                          00001850
           02 MLLIEUC   PIC X.                                          00001860
           02 MLLIEUP   PIC X.                                          00001870
           02 MLLIEUH   PIC X.                                          00001880
           02 MLLIEUV   PIC X.                                          00001890
           02 MLLIEUO   PIC X(20).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCTYPEA   PIC X.                                          00001920
           02 MCTYPEC   PIC X.                                          00001930
           02 MCTYPEP   PIC X.                                          00001940
           02 MCTYPEH   PIC X.                                          00001950
           02 MCTYPEV   PIC X.                                          00001960
           02 MCTYPEO   PIC X(2).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNATUREA  PIC X.                                          00001990
           02 MNATUREC  PIC X.                                          00002000
           02 MNATUREP  PIC X.                                          00002010
           02 MNATUREH  PIC X.                                          00002020
           02 MNATUREV  PIC X.                                          00002030
           02 MNATUREO  PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDPIECEA  PIC X.                                          00002060
           02 MDPIECEC  PIC X.                                          00002070
           02 MDPIECEP  PIC X.                                          00002080
           02 MDPIECEH  PIC X.                                          00002090
           02 MDPIECEV  PIC X.                                          00002100
           02 MDPIECEO  PIC X(10).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNUMFACTA      PIC X.                                     00002130
           02 MNUMFACTC PIC X.                                          00002140
           02 MNUMFACTP PIC X.                                          00002150
           02 MNUMFACTH PIC X.                                          00002160
           02 MNUMFACTV PIC X.                                          00002170
           02 MNUMFACTO      PIC X(7).                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MSOCCORA  PIC X.                                          00002200
           02 MSOCCORC  PIC X.                                          00002210
           02 MSOCCORP  PIC X.                                          00002220
           02 MSOCCORH  PIC X.                                          00002230
           02 MSOCCORV  PIC X.                                          00002240
           02 MSOCCORO  PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDDEBUTA  PIC X.                                          00002270
           02 MDDEBUTC  PIC X.                                          00002280
           02 MDDEBUTP  PIC X.                                          00002290
           02 MDDEBUTH  PIC X.                                          00002300
           02 MDDEBUTV  PIC X.                                          00002310
           02 MDDEBUTO  PIC X(10).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCIMPA    PIC X.                                          00002340
           02 MCIMPC    PIC X.                                          00002350
           02 MCIMPP    PIC X.                                          00002360
           02 MCIMPH    PIC X.                                          00002370
           02 MCIMPV    PIC X.                                          00002380
           02 MCIMPO    PIC X(4).                                       00002390
      * MESSAGE ERREUR                                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLIBERRA  PIC X.                                          00002420
           02 MLIBERRC  PIC X.                                          00002430
           02 MLIBERRP  PIC X.                                          00002440
           02 MLIBERRH  PIC X.                                          00002450
           02 MLIBERRV  PIC X.                                          00002460
           02 MLIBERRO  PIC X(78).                                      00002470
      * CODE TRANSACTION                                                00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
      * CICS DE TRAVAIL                                                 00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCICSA    PIC X.                                          00002580
           02 MCICSC    PIC X.                                          00002590
           02 MCICSP    PIC X.                                          00002600
           02 MCICSH    PIC X.                                          00002610
           02 MCICSV    PIC X.                                          00002620
           02 MCICSO    PIC X(5).                                       00002630
      * NETNAME                                                         00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNETNAMA  PIC X.                                          00002660
           02 MNETNAMC  PIC X.                                          00002670
           02 MNETNAMP  PIC X.                                          00002680
           02 MNETNAMH  PIC X.                                          00002690
           02 MNETNAMV  PIC X.                                          00002700
           02 MNETNAMO  PIC X(8).                                       00002710
      * CODE TERMINAL                                                   00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MSCREENA  PIC X.                                          00002740
           02 MSCREENC  PIC X.                                          00002750
           02 MSCREENP  PIC X.                                          00002760
           02 MSCREENH  PIC X.                                          00002770
           02 MSCREENV  PIC X.                                          00002780
           02 MSCREENO  PIC X(5).                                       00002790
                                                                                
