      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
           EJECT                                                                
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      **********************************************************                
      *   COPY DE LA TABLE RVAN0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAN0000                         
      **********************************************************                
       01  RVAN0000.                                                            
           02  AN00-CNOMPGRM                                                    
               PIC X(0006).                                                     
           02  AN00-NSEQERR                                                     
               PIC X(0004).                                                     
           02  AN00-LIBERR                                                      
               PIC X(0100).                                                     
           02  AN00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVAN0000                                  
      **********************************************************                
       01  RVAN0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN00-CNOMPGRM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN00-CNOMPGRM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN00-NSEQERR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN00-NSEQERR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN00-LIBERR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN00-LIBERR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  AN00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
