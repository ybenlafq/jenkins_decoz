      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ******************************************************************        
      *    COMMAREA POUR LES PERIODES GCT                                       
      *    IDEM COMMNPER SAUF NSOC AU LIEU DE NENTITE                           
      *    CODE RETOUR: 0=TOUT VA BIEN                                          
      *                 1=TOUT VA MAL (OU MOINS = CODE SQL)                     
      *    ON DOIT RENSEIGNER LA SOCIETE                                        
      *    DANS LE TABLEAU: POSTE 1 = PERIODE PRECEDENTE                        
      *                     POSTE 2 = PERIODE EN COURS                          
      *                     POSTE 3 = PERIODE FUTURE                            
      *    SI L'EXERCICE/PERIODE DU POSTE 4 EST DIFFERENT DE BLANC,             
      *    RECHERCHE DU LIBELLE, ETC.                                           
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-NPER-LONG-COMMAREA PIC S9(4)  COMP VALUE +200.                  
      *--                                                                       
       01  COMM-NPER-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
      *}                                                                        
       01  Z-COMMAREA-NPER.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NPER-CODE-RETOUR    PIC S9(4) BINARY.                            
      *--                                                                       
           05  NPER-CODE-RETOUR    PIC S9(4) COMP-5.                            
      *}                                                                        
           05  NPER-NSOC           PIC X(5).                                    
           05  NPER-TABLEAU  OCCURS 4.                                          
               10  NPER-EXERCICE       PIC XXXX.                                
               10  NPER-PERIODE        PIC XX.                                  
               10  NPER-DDEBUT         PIC X(8).                                
               10  NPER-DFIN           PIC X(8).                                
               10  NPER-LIBELLE        PIC X(20).                               
               10  NPER-TYPE-JOURNAL   PIC XX.                                  
                                                                                
