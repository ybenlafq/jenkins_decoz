           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG0202                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG0202                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0202.                                                            
           02  FG02-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FG02-NJRN                                                        
               PIC X(0010).                                                     
           02  FG02-NPIECE                                                      
               PIC X(0010).                                                     
           02  FG02-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FG02-NEXERCICE                                                   
               PIC X(0004).                                                     
           02  FG02-NPERIODE                                                    
               PIC X(0003).                                                     
           02  FG02-NOECS                                                       
               PIC X(0005).                                                     
           02  FG02-DCREAT                                                      
               PIC X(0008).                                                     
           02  FG02-COMPTE                                                      
               PIC X(0006).                                                     
           02  FG02-SSCOMPTE                                                    
               PIC X(0006).                                                     
           02  FG02-SECTION                                                     
               PIC X(0006).                                                     
           02  FG02-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FG02-TCOMPTE                                                     
               PIC X(0001).                                                     
           02  FG02-LIBELLE                                                     
               PIC X(0020).                                                     
           02  FG02-MONTANT                                                     
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG02-SENS                                                        
               PIC X(0001).                                                     
           02  FG02-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FG02-NETAB                                                       
               PIC X(0003).                                                     
           02  FG02-NATFACT                                                     
               PIC X(0005).                                                     
           02  FG02-CVENT                                                       
               PIC X(0005).                                                     
           02  FG02-NUMFACT                                                     
               PIC X(0007).                                                     
           02  FG02-CTYPPIECE                                                   
               PIC X(0002).                                                     
           02  FG02-CTYPLIGNE                                                   
               PIC X(0003).                                                     
           02  FG02-NLIGNEFACT                                                  
               PIC X(0003).                                                     
           02  FG02-TSOCFACT                                                    
               PIC X(0001).                                                     
           02  FG02-NSOC                                                        
               PIC X(0003).                                                     
           02  FG02-NLIEU                                                       
               PIC X(0003).                                                     
           02  FG02-NSOCCOR                                                     
               PIC X(0003).                                                     
           02  FG02-DATEFIC                                                     
               PIC X(0008).                                                     
           02  FG02-WRECUP                                                      
               PIC X(0001).                                                     
           02  FG02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FG02-NTIERS                                                      
               PIC X(0006).                                                     
           02  FG02-DVALEUR                                                     
               PIC X(0008).                                                     
           02  FG02-CCHRONO                                                     
               PIC X(0002).                                                     
           02  FG02-DRECUPCC                                                    
               PIC X(0008).                                                     
           02  FG02-TYPECPTE                                                    
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG0202                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0202-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NEXERCICE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NEXERCICE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NPERIODE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NPERIODE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NOECS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NOECS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-SSCOMPTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-SSCOMPTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-TCOMPTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-TCOMPTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-SENS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-SENS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NATFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NATFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-CVENT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-CVENT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NUMFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NUMFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-CTYPPIECE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-CTYPPIECE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-CTYPLIGNE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-CTYPLIGNE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NLIGNEFACT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NLIGNEFACT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-TSOCFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-TSOCFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NSOCCOR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NSOCCOR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-DATEFIC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-DATEFIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-WRECUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-WRECUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-NTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-NTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-DVALEUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-DVALEUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-CCHRONO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-CCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-DRECUPCC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-DRECUPCC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG02-TYPECPTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG02-TYPECPTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
