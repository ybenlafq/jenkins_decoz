           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG0700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG0700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0700.                                                            
           02  FG07-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FG07-CCHRONO                                                     
               PIC X(0002).                                                     
           02  FG07-NUMFACT                                                     
               PIC X(0007).                                                     
           02  FG07-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FG07-QTEFACT                                                     
               PIC S9(4)V9(0003) COMP-3.                                        
           02  FG07-PUFACT                                                      
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG07-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FG07-LCOMMENT                                                    
               PIC X(0040).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG0700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-CCHRONO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-CCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-NUMFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-NUMFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-QTEFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-QTEFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-PUFACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-PUFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG07-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG07-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
