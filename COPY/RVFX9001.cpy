           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFX9001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX9001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX9001.                                                            
           02  FX90-NSOC                                                        
               PIC X(0003).                                                     
           02  FX90-NOECS                                                       
               PIC X(0005).                                                     
           02  FX90-CRITERE1                                                    
               PIC X(0005).                                                     
           02  FX90-CRITERE2                                                    
               PIC X(0005).                                                     
           02  FX90-CRITERE3                                                    
               PIC X(0005).                                                     
           02  FX90-COMPTECG                                                    
               PIC X(0006).                                                     
           02  FX90-SECTION                                                     
               PIC X(0006).                                                     
           02  FX90-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FX90-DATEFF                                                      
               PIC X(0008).                                                     
           02  FX90-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  FX90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FX90-WFX00                                                       
               PIC X(0006).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFX9001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX9001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-NOECS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-NOECS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-CRITERE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-CRITERE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-CRITERE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-CRITERE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-CRITERE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-CRITERE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-COMPTECG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-COMPTECG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-DATEFF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-DATEFF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX90-WFX00-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX90-WFX00-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
