           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGVEN VENTILATIONS DE FACTURATION      *        
      *----------------------------------------------------------------*        
       01  RVGA01LW.                                                            
           05  FGVEN-CTABLEG2    PIC X(15).                                     
           05  FGVEN-CTABLEG2-REDEF REDEFINES FGVEN-CTABLEG2.                   
               10  FGVEN-NATURE          PIC X(05).                             
               10  FGVEN-CVENT           PIC X(05).                             
           05  FGVEN-WTABLEG     PIC X(80).                                     
           05  FGVEN-WTABLEG-REDEF  REDEFINES FGVEN-WTABLEG.                    
               10  FGVEN-WACTIF          PIC X(01).                             
               10  FGVEN-LVENT           PIC X(30).                             
               10  FGVEN-TYPEDC          PIC X(01).                             
               10  FGVEN-PRIMAIRE        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01LW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGVEN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGVEN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGVEN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGVEN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
 
      *}                                                                        
