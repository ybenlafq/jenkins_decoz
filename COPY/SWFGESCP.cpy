      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DU FICHIER POUR EDITION DU                                
      *       RECAPITULATIF DES ESCOMPTES ACCORDES (SRP)                        
      *                                                                         
      * LG = 70                                                                 
      *                                                                         
      *                                                                         
       01         FFGESCPT-DSECT.                                               
           03    FFGESCPT-IDENTIFIANT.                                          
001           05 FFGESCPT-NSOCC-ORIG   PIC X(03).                               
004           05 FFGESCPT-NSOCC-DEST   PIC X(03).                               
007           05 FFGESCPT-NSOCORIG     PIC X(03).                               
010           05 FFGESCPT-NLIEUORIG    PIC X(03).                               
013           05 FFGESCPT-NSOCDEST     PIC X(03).                               
016           05 FFGESCPT-NLIEUDEST    PIC X(03).                               
019           05 FFGESCPT-DMUTATION    PIC X(08).                               
027           05 FFGESCPT-NMUTATION    PIC X(07).                               
034           05 FFGESCPT-CTYPFACT     PIC X(02).                               
036           05 FFGESCPT-NUMFACT      PIC X(07).                               
043           05 FFGESCPT-CVENT        PIC X(05).                               
048           05 FILLER                PIC X(06).                               
           03    FFGESCPT-MONTANTS.                                             
054           05 FFGESCPT-TXESCPT      PIC S9(03)V9(4) COMP-3.                  
058           05 FFGESCPT-MONTHT       PIC S9(11)V99   COMP-3.                  
065        05 FILLER                   PIC X(06).                               
-=>070*                                                                         
                                                                                
