           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGSCT SOCIETES DEMARREES DANS GCT      *        
      *----------------------------------------------------------------*        
       01  RVGA01Q6.                                                            
           05  FGSCT-CTABLEG2    PIC X(15).                                     
           05  FGSCT-CTABLEG2-REDEF REDEFINES FGSCT-CTABLEG2.                   
               10  FGSCT-NSOCC           PIC X(03).                             
               10  FGSCT-NETAB           PIC X(03).                             
           05  FGSCT-WTABLEG     PIC X(80).                                     
           05  FGSCT-WTABLEG-REDEF  REDEFINES FGSCT-WTABLEG.                    
               10  FGSCT-WACTIF          PIC X(01).                             
               10  FGSCT-NSOCNCG         PIC X(03).                             
               10  FGSCT-WFX00           PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Q6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGSCT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGSCT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGSCT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGSCT-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
