      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/04/2016 1        
                                                                                
               EJECT                                                    00010005
      *==> DARTY ****************************************************   00020007
      *          * ZONES ADRESSABLES EXTERNES A LA TACHE            *   00030007
      **************************************************** SYKLINKA *   00040007
      *                      CWA                                        00070000
       01  LINK-CWA          PIC X(512).                                00080000
      *                      TWA                                        00090000
       01  LINK-TWA          PIC X(500).                                00100000
      *                      TCTUA                                      00110000
       01  LINK-TCTUA        PIC X(255).                                00120000
      *                      USER                                       00130000
      *01  LINK-USER         PIC X(4000).                               00140001
      *                            ZONE AIDA                            00150000
       01   LINK-TS.                                                    00160000
            02  LINK-TS-MAP        PIC X(2400).                         00170007
            02  LINK-TS-COMMAREA   PIC X(4096).                         00180000
            02  LINK-TS-SWAP-HELP  REDEFINES LINK-TS-COMMAREA.          00190003
                05 LINK-TS-CODERR        PIC X(04).                     00200003
                05 LINK-TS-PGMPRC        PIC X(08).                     00210003
                05 LINK-TS-NOM-MAP       PIC X(08).                     00220003
                05 LINK-TS-NOM-MAPSET    PIC X(08).                     00230003
                05 LINK-TS-NOM-TACHE     PIC X(04).                     00240003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 LINK-TS-COM-LONG      PIC S9(4) COMP.                00250003
      *--                                                                       
                05 LINK-TS-COM-LONG      PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 LINK-TS-MAP-LONG      PIC S9(4) COMP.                00260003
      *--                                                                       
                05 LINK-TS-MAP-LONG      PIC S9(4) COMP-5.                      
      *}                                                                        
                05 LINK-TS-SELECT.                                      00270003
                   08 LINK-TS-SELECTION  PIC X(04) OCCURS 3.            00280003
                05 LINK-TS-TACHE-JUMP    PIC X(04).                     00290003
                05 FILLER                PIC X(48).                     00300003
                05 LINK-TS-CICS          PIC X(20).                     00301006
                05 LINK-TS-DATE          PIC X(100).                    00302006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 LINK-TS-POS-CURS      PIC S9(4) COMP.                00303006
      *--                                                                       
                05 LINK-TS-POS-CURS      PIC S9(4) COMP-5.                      
      *}                                                                        
                05 LINK-FILLER           PIC X(3874).                   00303110
      *                                                                 00304009
      *                            ZONE AIDA                            00310003
      *                      LISTE DES POINTEURS PCB                    00320000
      *                                                                 00321009
       01  PCB-ADR-LIST.                                                00330000
           02  PCB-ADR       USAGE IS POINTER OCCURS 10 TIMES.          00340000
      *                      PCB COURANT                                00350000
       01  PCB-N             PIC X(300).                                00360000
               EJECT                                                    00380008
                                                                                
                                                                                
