      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                         
      ************************************************** EXEC DLI ******        
      * PREPARATION DE L'ACCES AU SEGMENT DIGSAC PAR LE PATH DIGFA0             
      ******************************************************************        
      *                                                                         
       CLEF-DIGFA0             SECTION.                                         
      *                                                                         
      ************************************* PREPARATION CLEF-1                  
      *                                                                         
           MOVE 'DIGSA'  TO SEG-NAME-1.                                         
           MOVE 'DIGFA0'  TO KEY-NAME-1.                                        
           MOVE '128'  TO SEG-LONG-1.                                           
           MOVE '36'  TO KEY-LONG-1.                                            
           MOVE DIGSA-DIGFA0                                                    
                         TO KEY-VAL-1.                                          
      *                                                                         
           MOVE 'DIGFA0'  TO PATH-NAME.                                         
      *                                                                         
           IF PSB-NOT-OK THEN                                                   
                         PERFORM SCHEDULING-EXEC                                
           END-IF.                                                              
      *                                                                         
       FIN-CLEF-DIGFA0.    EXIT.                                                
                EJECT                                                           
                                                                                
EMOD                                                                            
