      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                         
      * SOUS-PROGRAMMES COMMUNS POUR EDITION DES FACTURES EN                    
      * BATCH (BFG220) ET EN TP (MFG13)                                         
      *================================================================*        
      *                                                                *        
      * MODIF AP DSA005 - LE 14/05/97                                  *        
      * --> ADAPTATION POUR DEMARRAGE GCT MACON - RECUPERER LE LIBELLE *        
      *     DE L'ETABLISSEMENT DANS FGETL                              *        
      *     --> MODIF REFERENCEE PAR LE CODE 'DRA'  EN COL 1           *        
      *                                                                *        
      *================================================================*        
      *                                                                *        
      * MODIF PHRI - DSA026 - LE 19/11/09                              *        
      * --> RECUPERER LE CODE TVA INTRACOMMUNAUTAIRE DANS FGLIB        *        
      * --> GESTION DU CODE GEOGRAPHIQUE POUR AFFICHER LE CODE TVA     *        
      *     INTRACOMMUNAUTAIRE SI SOCIETE DANS PAYS CEE HORS FRANCE    *        
      * --> MODIF REFERENCEE PHRI EN COL 1                             *        
      *================================================================*        
      *                                                                         
      *                                                                         
      *    CHARGEMENT INITIAL DES SOUS-TABLES                                   
      *                                                                         
       CHARGE-GA01 SECTION.                                                     
           MOVE HIGH-VALUE TO WGA01-TAB-ALL                                     
                              WGA01-BMAX-ALL                                    
           MOVE  LOW-VALUE TO WGA01-BMIN-ALL                                    
      *    CHARGEMENT TAUX DE TVA                                               
           INITIALIZE WTVA-TABLE                                                
           MOVE 'TXTVA' TO WGA01-TAB1                                           
      *    CHARGEMENT TYPES DE DOCUMENT                                         
           INITIALIZE WTYP-TABLE                                                
           MOVE 'FGTYP' TO WGA01-TAB2                                           
           IF    EDITION-TP                                                     
                 MOVE FG01-CTYPFACT TO WGA01-BMIN2                              
                                       WGA01-BMAX2                              
           END-IF                                                               
      *    CHARGEMENT NATURES DE FACTURATION                                    
           INITIALIZE WNAT-TABLE                                                
           MOVE 'FGNAT' TO WGA01-TAB3                                           
           IF    EDITION-TP                                                     
                 MOVE FG01-NATFACT  TO WGA01-BMIN3                              
                                       WGA01-BMAX3                              
           END-IF                                                               
      *    CHARGEMENT VENTILATION PAR NATURE                                    
           INITIALIZE WVEN-TABLE                                                
           MOVE 'FGVEN' TO WGA01-TAB4                                           
           IF    EDITION-TP                                                     
                 MOVE FG01-NATFACT  TO WGA01-BMIN4(1:5)                         
                                       WGA01-BMAX4(1:5)                         
           END-IF                                                               
AM    *    CHARGEMENT LIBELLE SPECIF. PAR TYPE DE DOCUMENT                      
AM         INITIALIZE WCOM-TABLE                                                
AM         MOVE 0 TO W-COMMENT                                                  
AM         INITIALIZE  W-TYPE  W-NATCOM  W-NPARAG  W-NLIGNE                     
AM         MOVE 'FGCOM' TO WGA01-TAB5                                           
AM         IF    EDITION-TP                                                     
AM               MOVE FG01-CTYPFACT  TO WGA01-BMIN5(1:2)                        
AM                                      WGA01-BMAX5(1:2)                        
AM         END-IF                                                               
      *                                                                         
           PERFORM DECLARE-GA01                                                 
           PERFORM   FETCH-GA01                                                 
           PERFORM UNTIL NON-TROUVE                                             
              EVALUATE GA01-CTABLEG1                                            
                 WHEN 'TXTVA'                                                   
                    ADD 1 TO WTVA-NBP                                           
                    IF WTVA-NBP > WTVA-MAX                                      
                       MOVE 'WTVA' TO W-TABLE                                   
                       PERFORM DEPASSEMENT-TABLE                                
                    END-IF                                                      
                    MOVE GA01-CTABLEG2       TO WTVA-CTAUX  (WTVA-NBP)          
                    MOVE GA01-WTABLEG(21:03) TO WTVA-TAUXN-X(WTVA-NBP)          
                    MOVE GA01-WTABLEG(32:03) TO WTVA-TAUXA-X(WTVA-NBP)          
                    MOVE GA01-WTABLEG(24:08) TO WTVA-DEFFET (WTVA-NBP)          
                 WHEN 'FGTYP'                                                   
                    ADD 1 TO WTYP-NBP                                           
                    IF WTYP-NBP > WTYP-MAX                                      
                       MOVE 'WTYP' TO W-TABLE                                   
                       PERFORM DEPASSEMENT-TABLE                                
                    END-IF                                                      
                    MOVE GA01-CTABLEG2        TO WTYP-TYPF(WTYP-NBP)            
                    MOVE GA01-WTABLEG (02:25) TO WTYP-LTYP(WTYP-NBP)            
                    MOVE GA01-WTABLEG (31:01) TO WTYP-TVA (WTYP-NBP)            
                    MOVE GA01-WTABLEG (34:01) TO WTYP-REG (WTYP-NBP)            
                 WHEN 'FGNAT'                                                   
                    ADD 1 TO WNAT-NBP                                           
                    IF WNAT-NBP > WNAT-MAX                                      
                       MOVE 'WNAT' TO W-TABLE                                   
                       PERFORM DEPASSEMENT-TABLE                                
                    END-IF                                                      
                    MOVE GA01-CTABLEG2        TO WNAT-NATF(WNAT-NBP)            
                    MOVE GA01-WTABLEG (02:25) TO WNAT-LNAT(WNAT-NBP)            
                 WHEN 'FGVEN'                                                   
                    ADD 1 TO WVEN-NBP                                           
                    IF WVEN-NBP > WVEN-MAX                                      
                       MOVE 'WVEN' TO W-TABLE                                   
                       PERFORM DEPASSEMENT-TABLE                                
                    END-IF                                                      
                    MOVE GA01-CTABLEG2(01:05) TO WVEN-NATF(WVEN-NBP)            
                    MOVE GA01-CTABLEG2(06:05) TO WVEN-CVEN(WVEN-NBP)            
                    MOVE GA01-WTABLEG (02:25) TO WVEN-LVEN(WVEN-NBP)            
AM               WHEN 'FGCOM'                                                   
AM    *---- N� DES LIGNES PAR TYPE, NATURE, TOUS PARAG CONFONDUS                
AM                  MOVE GA01-CTABLEG2  TO W-FGCOM-CTABLEG2                     
AM                  MOVE GA01-WTABLEG   TO W-FGCOM-WTABLEG                      
AM                  IF W-FGCOM-ACTIF = 'O'                                      
AM                     IF W-FGCOM-TYPE NOT = W-TYPE  OR                         
1295                      W-FGCOM-NATCOM NOT = W-NATCOM                         
AM                        MOVE 1              TO W-NPARAG                       
AM                        MOVE 0              TO W-NLIGNE                       
AM                        MOVE W-FGCOM-TYPE   TO W-TYPE                         
1295                      MOVE W-FGCOM-NATCOM TO W-NATCOM                       
AM                        SET DEBUT-COM       TO TRUE                           
AM                     ELSE                                                     
AM                        IF W-FGCOM-NPARAG NOT = W-NPARAG                      
AM                           ADD  1         TO W-NPARAG                         
AM                           SET DEBUT-COM  TO TRUE                             
AM                        END-IF                                                
AM                     END-IF                                                   
AM    *                                                                         
AM                     IF DEBUT-COM                                             
AM                        ADD 1 TO W-NLIGNE   WCOM-NBP                          
AM                        IF WCOM-NBP > WCOM-MAX                                
AM                           MOVE 'WCOM' TO W-TABLE                             
AM                           PERFORM DEPASSEMENT-TABLE                          
AM                        END-IF                                                
AM                                                                              
AM                        MOVE W-TYPE    TO WCOM-TYPF(WCOM-NBP)                 
1295                      MOVE W-NATCOM  TO WCOM-NATCOM(WCOM-NBP)               
AM                        MOVE W-NPARAG  TO WCOM-NPARAG(WCOM-NBP)               
AM                        MOVE W-NLIGNE  TO WCOM-NLIGNE(WCOM-NBP)               
AM                        MOVE W-FGCOM-COMMENT                                  
AM                                       TO WCOM-COMMENT(WCOM-NBP)(1:38)        
AM                        SET FIN-COM  TO TRUE                                  
AM                     ELSE                                                     
AM                        MOVE W-FGCOM-COMMENT                                  
AM                                     TO WCOM-COMMENT(WCOM-NBP)(39:38)         
AM                        SET DEBUT-COM  TO TRUE                                
AM                     END-IF                                                   
AM                  END-IF                                                      
              END-EVALUATE                                                      
              PERFORM FETCH-GA01                                                
           END-PERFORM                                                          
           PERFORM CLOSE-GA01.                                                  
           DISPLAY 'COMPTEURS SOUS-TABLES '                                     
           DISPLAY 'FGNAT : ' WNAT-MAX ' / ' WNAT-NBP                           
           DISPLAY 'FGTYP : ' WTYP-MAX ' / ' WTYP-NBP                           
           DISPLAY 'FGCOM : ' WCOM-MAX ' / ' WCOM-NBP                           
           DISPLAY 'FGVEN : ' WVEN-MAX ' / ' WVEN-NBP                           
PHRI       DISPLAY 'FGLIB : ' WLIB2-MAX ' / ' WLIB2-NBP                         
           DISPLAY 'STE   : ' WSTE-MAX ' / ' WSTE-NBP                           
           DISPLAY 'SERV  : ' WSERV-MAX ' / ' WSERV-NBP                         
           DISPLAY 'LIB   : ' ' / ' WLIB-NBP                                    
           DISPLAY 'TXTVA : ' ' / ' WTVA-NBP                                    
           DISPLAY 'GA10  : ' WGA10-MAX ' / ' .                                 
       FIN-CHARGE-GA01.                                                         
         EXIT.                                                                  
      * ============================================================= *         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      * ============================================================= *         
       TRAITEMENT-FACTURE SECTION.                                              
      *                                                                         
           MOVE   FG01-NSOCCOMPT TO W-NSOCCOMPT                                 
           MOVE   FG01-NSOCORIG  TO W-NSOC-E                                    
           MOVE   FG01-NLIEUORIG TO W-NLIEU-E                                   
           MOVE   FG01-NSOCDEST  TO W-NSOC-R                                    
           MOVE   FG01-NLIEUDEST TO W-NLIEU-R                                   
           MOVE   FG01-CCHRONO   TO W-CCHRONO                                   
           MOVE   FG01-CTYPFACT  TO W-TYPF                                      
           MOVE   FG01-NATFACT   TO W-NATF                                      
           MOVE   FG01-NUMFACT   TO W-NUMF                                      
           MOVE   FG01-DCOMPTA   TO W-DCTA                                      
AP00       IF FG01-CACID > SPACES                                               
AM            MOVE FG01-CACID    TO H00-CACID                                   
AP00       ELSE                                                                 
AP00          MOVE SPACES        TO H00-CACID                                   
AP00       END-IF                                                               
           MOVE   SPACE          TO W-NSOCF-E                                   
                                    W-NSOCF-R                                   
      *                                                                         
           MOVE   FG01-SERVORIG  TO W-SERVORIG                                  
           MOVE   FG01-SERVDEST  TO W-SERVDEST                                  
      *                                                                         
AM         IF FG01-DVALEUR > SPACES                                             
AM            MOVE FG01-DVALEUR  TO  W-DVALEUR                                  
AM            STRING FG01-DVALEUR(7:2) '/' FG01-DVALEUR(5:2)                    
AM                                     '/' FG01-DVALEUR(1:4)                    
AM                     DELIMITED SIZE INTO B07-DVALEUR                          
AM         ELSE                                                                 
AM            MOVE SPACES        TO  W-DVALEUR                                  
AM         END-IF                                                               
      *                                                                         
AM         IF FG01-NUMORIG > SPACES AND FG01-DMOUV > SPACES AND                 
AM            (FG01-NUMORIG NOT = FG01-NUMFACT  OR                              
AM             FG01-DMOUV   NOT = FG01-DCREAT)                                  
AM             MOVE FG01-NUMORIG   TO  W-NORIG  H01-NORIG                       
AM             STRING FG01-DMOUV (7:2) '/' FG01-DMOUV (5:2)                     
AM                                     '/' FG01-DMOUV (1:4)                     
AM                    DELIMITED SIZE INTO H01-DORIG                             
AM          ELSE                                                                
AM             MOVE SPACES         TO  W-NORIG                                  
AM          END-IF                                                              
      *                                                                         
           STRING FG01-DCOMPTA (7:2) '/' FG01-DCOMPTA (5:2)                     
                                     '/' FG01-DCOMPTA (1:4)                     
                  DELIMITED SIZE INTO W-DATF                                    
           STRING FG01-DECHEANC(7:2) '/' FG01-DECHEANC(5:2)                     
                                     '/' FG01-DECHEANC(1:4)                     
                  DELIMITED SIZE INTO B06-DECH                                  
      *                                                                         
      *                                                                         
      *                                                                         
      *    RECHERCHE DE LA NATURE DE FACTURATION                                
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > WNAT-NBP                     
                   OR WNAT-NATF(I) = W-NATF                                     
           END-PERFORM                                                          
           IF I > WNAT-NBP                                                      
              STRING W-NATF ' NON RETROUVE DANS FGNAT'                          
                     DELIMITED SIZE INTO WFG-MESS                               
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           MOVE WNAT-LNAT(I) TO W-LNATF                                         
      *                                                                         
      *    RECHERCHE DU TYPE DE DOCUMENT                                        
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > WTYP-NBP                     
                   OR WTYP-TYPF(I) = W-TYPF                                     
           END-PERFORM                                                          
           IF I > WTYP-NBP                                                      
              STRING W-TYPF ' NON RETROUVE DANS FGTYP'                          
                     DELIMITED SIZE INTO WFG-MESS                               
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           MOVE WTYP-LTYP(I) TO W-LTYP                                          
           IF   WTYP-TVA (I) = 'N'                                              
                SET TVA-NON TO TRUE                                             
           ELSE                                                                 
                SET TVA-OUI TO TRUE                                             
           END-IF                                                               
           IF   WTYP-REG (I) = 'N'                                              
                SET REG-NON TO TRUE                                             
           ELSE                                                                 
                SET REG-OUI TO TRUE                                             
           END-IF                                                               
      *                                                                         
      *                                                                         
      *                                                                         
      *    RECHERCHE SOCIETES / ETABLISSEMENT DE COMPTABILISATION               
           MOVE W-NSOC-E   TO W-NSOC                                            
           MOVE W-NLIEU-E  TO W-NLIEU                                           
           PERFORM  RECH-NSOC                                                   
DRA        MOVE W-NETAB    TO W-NETAB-E                                         
           MOVE W-NSOCF    TO W-NSOCF-E                                         
           MOVE I TO IGE                                                        
           MOVE W-NSOC-R   TO W-NSOC                                            
           MOVE W-NLIEU-R  TO W-NLIEU                                           
           PERFORM  RECH-NSOC                                                   
           MOVE W-NSOCC    TO W-NSOCC-R                                         
DRA        MOVE W-NETAB    TO W-NETAB-R                                         
           MOVE W-NSOCF    TO W-NSOCF-R                                         
           MOVE I TO IGR                                                        
PHRI  *--- RECHERCHE CODE GEOGRAPHIQUE DE LA SOCIETE RECEPTRICE                 
PHRI       PERFORM  RECH-CODEGEO                                                
PHRI  *    CHARGEMENT CODE TVA INTRACOMMUNAUTAIRE                               
"          IF W-CODEGEO = '2'                                                   
"             INITIALIZE WLIB2-TABLE                                            
"             INITIALIZE WLIB2-TVA-INTRA                                        
"                                                                               
"             MOVE 'FGLIB' TO GA01-CTABLEG1                                     
"             STRING W-NSOC-R '04'                                              
"                    DELIMITED SIZE INTO GA01-CTABLEG2                          
"             PERFORM SELECT-GA01                                               
"             IF SQLCODE = 0                                                    
"                MOVE GA01-WTABLEG (02:25) TO WLIB2-TVA-INTRA                   
"             ELSE                                                              
"                MOVE SPACES               TO WLIB2-TVA-INTRA                   
"             END-IF                                                            
PHRI       END-IF                                                               
      *                                                                         
      ***  IF W-NSOCF-E = W-NSOCF-R                                             
      ***     SET TVA-NON TO TRUE                                               
      ***  END-IF                                                               
      *    RECHERCHE LIBELLE RCS STE EMETTRICE                                  
           IF W-NSOCF-E NOT = W-NSOC-LIB                                        
              MOVE HIGH-VALUE TO WGA01-TAB-ALL                                  
                                 WGA01-BMAX-ALL                                 
              MOVE  LOW-VALUE TO WGA01-BMIN-ALL                                 
              MOVE W-NSOCF-E TO W-NSOC-LIB                                      
              MOVE 'FGLIB'   TO  WGA01-TAB1                                     
              MOVE W-NSOCF-E TO  WGA01-BMIN1(1:3)                               
                                 WGA01-BMAX1(1:3)                               
              PERFORM DECLARE-GA01                                              
              PERFORM   FETCH-GA01                                              
              INITIALIZE  WLIB-TABLE                                            
              PERFORM UNTIL NON-TROUVE                                          
                 IF GA01-WTABLEG(1:1) = 'O'                                     
                    ADD  1 TO WLIB-NBP                                          
                    MOVE GA01-WTABLEG(2:60) TO WLIB-LIB(WLIB-NBP)               
                 END-IF                                                         
                 IF WLIB-NBP = WLIB-MAX                                         
                    SET NON-TROUVE TO TRUE                                      
                 ELSE                                                           
                    PERFORM FETCH-GA01                                          
                 END-IF                                                         
              END-PERFORM                                                       
              PERFORM CLOSE-GA01                                                
           END-IF                                                               
      *                                                                         
      *    RECHERCHES DETAIL DES LIGNES                                         
           INITIALIZE WFG01-TABLE                                               
                      WTVA-TABLE2                                               
                      W-MONTANTS-FACT                                           
           MOVE FG01-TXESCPT TO W-TXESCPT                                       
           IF   FG01-TXESCPT > 0                                                
                SET ESC-OUI TO TRUE                                             
           ELSE                                                                 
                SET ESC-NON TO TRUE                                             
           END-IF                                                               
           PERFORM UNTIL FIN-FG01                                               
              OR         FG01-NSOCCOMPT NOT = W-NSOCCOMPT                       
              OR         FG01-CCHRONO   NOT = W-CCHRONO                         
              OR         FG01-NUMFACT   NOT = W-NUMF                            
      *                                                                         
              ADD 1 TO WFG01-NBP                                                
              IF  WFG01-NBP > WFG01-MAX                                         
                  MOVE 'WFG01' TO W-TABLE                                       
                  PERFORM DEPASSEMENT-TABLE                                     
              END-IF                                                            
              MOVE WFG01-NBP     TO             I                               
              MOVE FG01-NLIGNE   TO WFG01-NLIG (I)                              
              MOVE FG01-CVENT    TO WFG01-CVENT(I)                              
              MOVE FG01-TXESCPT  TO WFG01-ESCPT(I)                              
              MOVE FG01-MONTHT   TO WFG01-PUF  (I)                              
              MOVE 1             TO WFG01-QTF  (I)                              
              MOVE SPACE         TO WFG01-LIBL (I)                              
              IF FG01-CVENT > SPACE                                             
                 PERFORM VARYING   J  FROM 1 BY 1 UNTIL J > WVEN-NBP            
AM                    OR (WVEN-CVEN(J) = FG01-CVENT   AND                       
AM                        WVEN-NATF(J) = W-NATF)                                
                 END-PERFORM                                                    
                 IF J > WVEN-NBP                                                
                    MOVE ALL '?'      TO WFG01-LIBC(I)                          
                 ELSE                                                           
                    MOVE WVEN-LVEN(J) TO WFG01-LIBC(I)                          
                 END-IF                                                         
              ELSE                                                              
                 MOVE W-LNATF         TO WFG01-LIBC(I)                          
              END-IF                                                            
      *                                                                         
              IF   FG01-TXESCPT NOT = W-TXESCPT                                 
                   SET ESC-MULT TO TRUE                                         
              END-IF                                                            
      *                                                                         
      *                                                                         
              MOVE    FG01-MONTHT   TO WFG01-MTHT (I)                           
              COMPUTE W-MT        = FG01-MONTHT * FG01-TXESCPT                  
              COMPUTE W-MT        = W-MT / 100                                  
              ADD     W-MT          TO W-TOTESC                                 
              COMPUTE W-MT        = FG01-MONTHT - W-MT                          
              ADD     FG01-MONTHT   TO W-TOTHT                                  
              ADD     W-MT          TO W-NETHT                                  
              IF TVA-OUI AND FG01-CTAUXTVA NOT = SPACE                          
                 PERFORM VARYING       J FROM 1 BY 1 UNTIL J > WTVA-NBP         
                         OR WTVA-CTAUX(J) = FG01-CTAUXTVA                       
                 END-PERFORM                                                    
                 IF J > WTVA-NBP                                                
                    STRING FG01-CTAUXTVA ' TVA NON TROUVE DANS TXTVA'           
                           DELIMITED SIZE INTO WFG-MESS                         
                           PERFORM FIN-ANORMALE                                 
                 END-IF                                                         
                 IF FG01-DCOMPTA < WTVA-DEFFET(J)                               
                    MOVE WTVA-TAUXA(J) TO W-TAUX                                
                 ELSE                                                           
                    MOVE WTVA-TAUXN(J) TO W-TAUX                                
                 END-IF                                                         
                 COMPUTE W-MTTVA = (W-MT * W-TAUX) / 100                        
                 MOVE W-TAUX  TO WTVA-TX      (J)                               
                 ADD  W-MT    TO WTVA-MTHT    (J)                               
                 ADD  W-MTTVA TO WTVA-MTTVA   (J)                               
                                 W-TOTTVA                                       
              END-IF                                                            
              PERFORM FETCH-FG01                                                
           END-PERFORM                                                          
*******    DISPLAY 'FACTURE : ' W-NUMF ' ' W-TYPF ' 'W-NATF                     
      *    RECHERCHES COMMENTAIRES FACTURES DANS FG06                           
           INITIALIZE WFG06-TABLE                                               
           MOVE SPACES TO W-NLIG-MAX-FG06                                       
                          W-NLIG-TRT                                            
                          W-NLIG-MAX-LFACT                                      
           MOVE 0      TO W-NBP-MAX-FG06                                        
                          W-NBP-TRT                                             
                          W-NBP-MAX-LFACT                                       
           PERFORM UNTIL FIN-FG06                                               
              OR   FG06-NSOCCOMPT NOT = W-NSOCCOMPT                             
              OR   FG06-CCHRONO   NOT = W-CCHRONO                               
              OR   FG06-NUMFACT   NOT = W-NUMF                                  
              ADD 1 TO WFG06-NBP                                                
              IF  WFG06-NBP > WFG06-MAX                                         
                  MOVE 'WFG06' TO W-TABLE                                       
                  PERFORM DEPASSEMENT-TABLE                                     
              END-IF                                                            
              MOVE FG06-NLIGNE   TO WFG06-NLIG (WFG06-NBP)                      
              MOVE FG06-LCOMMENT TO WFG06-LCOMM(WFG06-NBP)                      
              IF   FG06-NLIGNE = '000'                                          
                   ADD 1 TO WFG06-NBE                                           
AM            ELSE                                                              
AM               IF FG06-NLIGNE NOT = W-NLIG-TRT                                
AM                  IF W-NBP-TRT > W-NBP-MAX-FG06                               
AM                     MOVE W-NLIG-TRT TO W-NLIG-MAX-FG06                       
AM                     MOVE W-NBP-TRT  TO W-NBP-MAX-FG06                        
AM                  END-IF                                                      
AM                  MOVE FG06-NLIGNE   TO W-NLIG-TRT                            
AM                  MOVE 0             TO W-NBP-TRT                             
AM               END-IF                                                         
AM               ADD 1 TO W-NBP-TRT                                             
              END-IF                                                            
              PERFORM FETCH-FG06                                                
           END-PERFORM                                                          
AM    *---- TRAITEMENT DE LA DERNIERE LIGNE DE FG01, PEUT ETRE CELLE            
AM    *-----QUI A LE PLUS DE LIGNE DE COMM SPECIF                               
AM         IF W-NBP-TRT > W-NBP-MAX-FG06                                        
AM            MOVE W-NLIG-TRT TO W-NLIG-MAX-FG06                                
AM            MOVE W-NBP-TRT  TO W-NBP-MAX-FG06                                 
AM         END-IF                                                               
AM         MOVE W-NLIG-MAX-FG06  TO   W-NLIG-MAX-LFACT                          
AM         MOVE W-NBP-MAX-FG06   TO   W-NBP-MAX-LFACT                           
*******    DISPLAY 'WFG06-NBE = ' WFG06-NBE                                     
*******    DISPLAY 'W-NLIG-MAX-LFACT = ' W-NLIG-MAX-LFACT                       
*******    DISPLAY 'W-NBP-MAX-LFACT  = ' W-NBP-MAX-LFACT                        
      *                                                                         
AM    *--- RECHERCHES COMMENTAIRE SPECIFIQUE AU TYPE DE DOCUMENT                
AM3105     INITIALIZE WFGCOM-TABLE                                              
AM    *--- RECHERCHE DU PREMIER ENREGISTREMENT DU TYPE TRAITE                   
AM         PERFORM VARYING I FROM 1 BY 1 UNTIL I > WCOM-NBP OR                  
AM                 WCOM-TYPF(I) = W-TYPF                                        
AM         END-PERFORM                                                          
1295  *----RECHERCHE SI SPECIFIQUE POUR LA NATURE                               
1295       MOVE  0  TO I-CTRL I-NATURE  I-STD                                   
1295       IF I NOT > WCOM-NBP                                                  
1295          PERFORM VARYING I-CTRL FROM I BY 1 UNTIL                          
1295             I-CTRL > WCOM-NBP   OR                                         
1295             WCOM-TYPF(I-CTRL) NOT = W-TYPF  OR                             
1295             I-NATURE > 0                                                   
1295             IF WCOM-NATCOM(I-CTRL) = '*****'  AND                          
1295                I-STD = 0                                                   
1295                MOVE I-CTRL  TO  I-STD                                      
1295             ELSE                                                           
1295                IF WCOM-NATCOM(I-CTRL) = W-NATF  AND                        
1295                   I-NATURE = 0                                             
1295                   MOVE I-CTRL  TO  I-NATURE                                
1295                END-IF                                                      
1295             END-IF                                                         
1295          END-PERFORM                                                       
1295       END-IF                                                               
      *                                                                         
1295       IF I-NATURE > 0                                                      
1295          MOVE I-NATURE   TO  I                                             
1295       ELSE                                                                 
1295          IF I-STD > 0                                                      
1295             MOVE I-STD   TO  I                                             
              ELSE                                                              
                 COMPUTE I = WCOM-NBP + 1                                       
1295          END-IF                                                            
1295       END-IF                                                               
AM    *--- SI TYPE TROUVE --> LIB SPECIFIQUE : CHARG. DE TTES LES LIGNES        
AM         IF I NOT > WCOM-NBP                                                  
      *                                                                         
              MOVE WCOM-NATCOM(I)  TO W-NATCOM                                  
AM            MOVE WCOM-NPARAG(I)  TO W-NPARAG                                  
AM            PERFORM VARYING I FROM I BY 1 UNTIL I > WCOM-NBP OR               
AM                     WCOM-TYPF(I) NOT = W-TYPF  OR                            
1295                   WCOM-NATCOM(I) NOT = W-NATCOM                            
AM               IF WCOM-NPARAG(I) NOT = W-NPARAG                               
AM                  ADD 1 TO WFGCOM-NBP                                         
AM                  MOVE WCOM-NPARAG(I) TO W-NPARAG                             
AM               END-IF                                                         
AM               ADD 1 TO WFGCOM-NBP                                            
AM               IF  WFGCOM-NBP > WFGCOM-MAX                                    
AM                   MOVE 'WFGCO' TO W-TABLE                                    
AM                   PERFORM DEPASSEMENT-TABLE                                  
AM               END-IF                                                         
AM               MOVE WCOM-COMMENT(I)  TO WFGCOM-LIBL(WFGCOM-NBP)               
AM            END-PERFORM                                                       
AM         END-IF                                                               
*******    DISPLAY 'NB POSTES WFGCOM = ' WFGCOM-NBP                             
      *                                                                         
      *    RECHERCHES DETAIL DES LIGNES                                         
           PERFORM UNTIL FIN-FG07                                               
              OR         FG07-NSOCCOMPT NOT = W-NSOCCOMPT                       
              OR         FG07-CCHRONO   NOT = W-CCHRONO                         
              OR         FG07-NUMFACT   NOT = W-NUMF                            
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > WFG01-NBP                 
                      OR WFG01-NLIG(I) = FG07-NLIGNE                            
              END-PERFORM                                                       
              IF NOT I > WFG01-NBP                                              
                 MOVE  FG07-QTEFACT  TO WFG01-QTF  (I)                          
                 MOVE  FG07-PUFACT   TO WFG01-PUF  (I)                          
      ***********IF    FG07-LCOMMENT(26:15) = SPACE                             
                 IF    NOT FG07-LCOMMENT =  WFG01-LIBC (I)                      
                    MOVE   FG07-LCOMMENT TO WFG01-LIBL (I)                      
AM                  IF (FG07-NLIGNE = W-NLIG-MAX-LFACT) OR                      
                       (W-NLIG-MAX-LFACT = '   ')                               
AM                      IF W-NLIG-MAX-LFACT = '   '                             
AM                         MOVE FG07-NLIGNE TO W-NLIG-MAX-LFACT                 
AM                      END-IF                                                  
AM                     COMPUTE W-NBP-MAX-LFACT = W-NBP-MAX-LFACT + 1            
AM                  END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
              PERFORM FETCH-FG07                                                
           END-PERFORM                                                          
*******    DISPLAY 'W-NBP-MAX-LFACT APRES FG07 = ' W-NBP-MAX-LFACT              
      *                                                                         
      *----AJOUT DE 2 LIGNES A W-NBP-MAX-LFACT POUR LIBELLE STD CRITERE         
      *---- ET SAUT DE LIGNE                                                    
           COMPUTE W-NBP-MAX-LFACT = W-NBP-MAX-LFACT + 2                        
*******    DISPLAY 'W-NBP-MAX-LFACT APRES + 2  = ' W-NBP-MAX-LFACT              
      *                                                                         
      *    PREPARATION FACTURE ; CALCUL NOMBRE DE PAGES                         
      *                                                                         
      *    HAUT DE PAGE TERMINE SUR LIGNE 24                                    
      *    PIED DE PAGE COMMENCE EN 48 �ME LIGNE                                
      *    RETIRER LES LIGNES DE COMMENTAIRES D'ENTETE                          
AM    *--- ET LES LIGNES DE COMMENT. SPECIFIQUES AU TYPE DU DOCUMENT LE         
AM    *--- CAS ECHEANT                                                          
      *    NOMBRE DE LIGNES DU CORPS =                                          
      *--- RETIRER LES LIGNES DE COMMENTAIRE D'ENTETE                           
           IF WFG06-NBE > 0                                                     
              COMPUTE W-NDISP = 23 - WFG06-NBE - 1                              
           ELSE                                                                 
              COMPUTE W-NDISP = 23                                              
           END-IF                                                               
*******    DISPLAY 'W-NDISP = ' W-NDISP                                         
      *--- RETIRER LES LIGNES DE COMMENT. SPECIFIQUE TYPE DE DOC                
AM         IF WFGCOM-NBP > 0                                                    
AM            COMPUTE W-NDISP2 = W-NDISP - WFGCOM-NBP - 1                       
*******       DISPLAY 'W-NDISP2 = ' W-NDISP2                                    
AM            IF W-NDISP2 < 3                                                   
AM    *----------LIMITER LE NOMBRE DE LIGNES DU LIBELLE SPECIF                  
AM               COMPUTE WFGCOM-NBP = W-NDISP - 4                               
AM               MOVE 3 TO W-NDISP                                              
AM            ELSE                                                              
AM               MOVE W-NDISP2 TO W-NDISP                                       
AM            END-IF                                                            
AM         END-IF                                                               
*******    DISPLAY 'W-NDISP FINAL = ' W-NDISP                                   
AM    *----SI LE MAX DE LIGNES DE LIBELLE POUR UNE LIGNE DE FACT EST <          
AM    *----AU NB DE LIGNES DISPO., REDUIRE LE NB DE LIGNES DU LIBELLE           
AM    *----SPECIF, ET RECALCULER LE NB DE LIGNES DISPO (AUGMENTE)               
*******    DISPLAY ' W-NBP-MAX-LFACT = ' W-NBP-MAX-LFACT                        
AM         IF W-NDISP < W-NBP-MAX-LFACT                                         
AM            COMPUTE W-DIFF = W-NBP-MAX-LFACT - W-NDISP                        
*******       DISPLAY 'W-DIFF = ' W-DIFF                                        
AM            MOVE W-NBP-MAX-LFACT TO W-NDISP                                   
AM            COMPUTE WFGCOM-NBP = WFGCOM-NBP - W-DIFF                          
AM            IF WFGCOM-NBP < 1                                                 
                 MOVE 0 TO WFGCOM-NBP                                           
                 ADD 1 TO W-NDISP                                               
              END-IF                                                            
*******       DISPLAY 'FINAL : W-NDISP = ' W-NDISP                              
*******       DISPLAY '        WFGCOM-NBP = ' WFGCOM-NBP                        
AM         END-IF                                                               
      *                                                                         
      *    PREDETERMINATION DU NOMBRE DE PAGES                                  
           MOVE 0 TO J                                                          
           MOVE 1 TO W-NP2 IFG06                                                
           COMPUTE IFG06 = WFG06-NBE + 1                                        
*******    DISPLAY 'IFG06 INIT = ' IFG06                                        
           PERFORM VARYING IFG01 FROM 1 BY 1 UNTIL IFG01 > WFG01-NBP            
              IF WFG01-LIBL(IFG01) > SPACE                                      
                 MOVE 3 TO K                                                    
              ELSE                                                              
                 MOVE 2 TO K                                                    
              END-IF                                                            
*******       DISPLAY 'IFG01/K = ' IFG01 ' ' K                                  
              PERFORM VARYING IFG06 FROM IFG06 BY 1                             
                        UNTIL IFG06 > WFG06-NBP                                 
                           OR WFG06-NLIG(IFG06) > WFG01-NLIG(IFG01)             
                 ADD 1 TO K                                                     
              END-PERFORM                                                       
              IF (J + K) > W-NDISP                                              
                 ADD 1 TO W-NP2                                                 
                 MOVE K TO J                                                    
              ELSE                                                              
                 ADD  K TO J                                                    
              END-IF                                                            
*******       DISPLAY 'W-NP2/J = ' W-NP2 ' ' J                                  
              MOVE W-NP2 TO WFG01-NP(IFG01)                                     
           END-PERFORM.                                                         
       FIN-TRAITEMENT-FACTURE.                                                  
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *                                                                         
      * EDITION DU DOCUMENT                                                     
       EDITION-FACTURE SECTION.                                                 
           MOVE 1       TO W-NP1                                                
           MOVE 0       TO B00-TOTHT                                            
                           B01-MTESC                                            
                           B03-NETHT                                            
                           B04-MTTVA                                            
                           B05-MTTTC                                            
AM----*                    ZTVA-MTHT                                            
AM----*                    ZTVA-TX                                              
AM----*                    ZTVA-MTTVA                                           
AM    *---- DEPLACEMENT DE LA REMISE A BLANC                                    
AM         MOVE SPACES   TO B03-LTVA                                            
AM                          B04-LTVA                                            
AM                          B05-LTVA                                            
      *                                                                         
           PERFORM EDITION-HAUT                                                 
      *                                                                         
      *                                                                         
      *                                                                         
*******    DISPLAY 'APRES EDITION HAUT - J = ' J                                
           PERFORM VARYING IFG01 FROM 1 BY 1 UNTIL IFG01 > WFG01-NBP            
      *       SAUT DE PAGE                                                      
              IF WFG01-NP(IFG01) > W-NP1                                        
                     MOVE '../..' TO B00-REP                                    
                     PERFORM EDITION-BAS                                        
                     ADD  1  TO W-NP1                                           
                     PERFORM EDITION-HAUT                                       
              END-IF                                                            
      *                                                                         
      * DETAIL LIGNE PAR LIGNE                                                  
              MOVE WFG01-CVENT(IFG01) TO L02-CVENT                              
              MOVE WFG01-LIBC (IFG01) TO L02-LIBC                               
              MOVE WFG01-QTF  (IFG01) TO L02-QTF                                
              MOVE WFG01-PUF  (IFG01) TO L02-PUF                                
              MOVE WFG01-ESCPT(IFG01) TO L02-ESCPT                              
              MOVE WFG01-MTHT (IFG01) TO L02-MTHT                               
              ADD  2     TO J                                                   
              MOVE L02               TO W-LIG                                   
              PERFORM ECR-LIG                                                   
              MOVE ' '               TO W-ASA                                   
              IF WFG01-LIBL(IFG01) > ' '                                        
                 MOVE WFG01-LIBL (IFG01) TO L03-LIBL                            
                 MOVE L03                TO W-LIG                               
                 ADD  1     TO J                                                
                 PERFORM ECR-LIG                                                
              END-IF                                                            
              PERFORM  VARYING IFG06 FROM IFG06 BY 1                            
                 UNTIL IFG06 > WFG06-NBP                                        
                 OR    WFG06-NLIG(IFG06) > WFG01-NLIG(IFG01)                    
      *                                                                         
                 IF WFG06-NLIG(IFG06) = WFG01-NLIG(IFG01)                       
                    MOVE WFG06-LCOMM(IFG06) TO L03-LIBL                         
                    MOVE L03                TO W-LIG                            
                    ADD  1     TO J                                             
                    PERFORM ECR-LIG                                             
                 END-IF                                                         
      *                                                                         
              END-PERFORM                                                       
              MOVE '0' TO W-ASA                                                 
      *                                                                         
           END-PERFORM                                                          
*******    DISPLAY 'FIN DETAIL LIGNES - J = ' J                                 
           MOVE W-TOTHT  TO B00-TOTHT                                           
           MOVE W-TOTESC TO B01-MTESC                                           
           MOVE W-NETHT  TO B03-NETHT                                           
           MOVE W-TOTTVA TO B04-MTTVA                                           
           COMPUTE          B05-MTTTC = W-NETHT + W-TOTTVA                      
           IF TVA-OUI                                                           
              MOVE 0 TO K                                                       
AM            MOVE 0 TO TOP-TVA                                                 
              PERFORM VARYING ITVA FROM 1 BY 1 UNTIL ITVA > WTVA-NBP            
                 IF WTVA-MTTVA     (ITVA) > 0                                   
AM                  SET TVA-OK TO TRUE                                          
                    MOVE WTVA-MTHT (ITVA) TO ZTVA-MTHT                          
                    MOVE WTVA-TX   (ITVA) TO ZTVA-TX                            
                    MOVE WTVA-MTTVA(ITVA) TO ZTVA-MTTVA                         
                    ADD 1 TO K                                                  
                    EVALUATE K                                                  
                       WHEN 1           MOVE ZTVA-LIB TO B03-LTVA               
                       WHEN 2           MOVE ZTVA-LIB TO B04-LTVA               
                       WHEN 3           MOVE ZTVA-LIB TO B05-LTVA               
                       WHEN OTHER                                               
                            MOVE 'MODIFIER DESSIN FACTURE + 3 TVA'              
                                  TO WFG-MESS                                   
                            PERFORM FIN-ANORMALE                                
                    END-EVALUATE                                                
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF                                                               
           PERFORM EDITION-BAS.                                                 
       FIN-EDITION-FACTURE.                                                     
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *    EDITION DES LIGNES D'ENTETE DE LA FACTURE                            
       EDITION-HAUT SECTION.                                                    
           MOVE W-NSOCCOMPT  TO W-NSOCC                                         
           MOVE W-SERVORIG   TO W-SERV                                          
           MOVE IGE          TO IG                                              
           PERFORM STOCKE-ADR                                                   
      *                                                                         
           MOVE '1'     TO W-ASA                                                
AP00  *****IF FG01-CACID > SPACES                                               
AP00          MOVE H00     TO W-LIG                                             
AP00  *****ELSE                                                                 
AP00  *****   MOVE SPACES  TO W-LIG                                             
AP00  *****END-IF                                                               
           PERFORM ECR-LIG                                                      
           MOVE ' '     TO W-ASA                                                
AM         MOVE SPACES  TO W-LIG                                                
           PERFORM ECR-LIG                                                      
      *                                                                         
           MOVE WADR-ADR(1)      TO W-LIG(02:38)                                
           MOVE ALL '*'          TO W-LIG(40:34)                                
           IF W-LIBPRO NOT = SPACE                                              
                MOVE W-LIBPRO   TO  W-LIG(52:11)                                
           END-IF                                                               
           PERFORM ECR-LIG                                                      
      *                                                                         
           MOVE  30     TO WCTR-L                                               
           MOVE  W-LTYP TO WCTR-X                                               
           PERFORM CENTRAGE                                                     
           MOVE ' '     TO W-ASA                                                
           MOVE SPACE   TO W-LIG                                                
           MOVE WADR-ADR(2)       TO W-LIG(02:38)                               
           MOVE '*'               TO W-LIG(40:01)                               
                                     W-LIG(73:01)                               
           MOVE  WCTR-X TO W-LIG(42:30)                                         
           PERFORM ECR-LIG                                                      
           MOVE SPACE                 TO W-LIG                                  
           MOVE WADR-ADR(3)           TO W-LIG(02:38)                           
           STRING '   N� ' W-NUMF ' DU ' W-DATF '   '                           
                   DELIMITED SIZE INTO   W-LIB                                  
           MOVE    W-LIB              TO W-LIG(42:30)                           
           MOVE '*'                   TO W-LIG(40:01)                           
                                         W-LIG(73:01)                           
           PERFORM ECR-LIG                                                      
           MOVE SPACE                 TO W-LIG                                  
           MOVE WADR-ADR(4)           TO W-LIG(02:38)                           
           MOVE ALL '*'               TO W-LIG(40:34)                           
           PERFORM ECR-LIG                                                      
           MOVE SPACE                 TO W-LIG                                  
           MOVE WADR-ADR(5)           TO W-LIG(02:38)                           
           PERFORM ECR-LIG                                                      
           MOVE SPACE                 TO W-LIG                                  
           MOVE WADR-ADR(6)           TO W-LIG(02:38)                           
           PERFORM ECR-LIG                                                      
           MOVE ' '      TO W-LIG  WCTR-X                                       
           MOVE  1       TO L                                                   
AM         STRING W-NATF ' - ' W-LNATF   DELIMITED '  ' INTO WCTR-X             
AM                                       WITH POINTER L                         
           STRING  ' DE ' W-NSOC-E ' ' W-NLIEU-E                                
                    ' A ' W-NSOC-R ' ' W-NLIEU-R                                
                          DELIMITED SIZE INTO WCTR-X WITH POINTER L             
AM         MOVE  58     TO WCTR-L                                               
           PERFORM CENTRAGE                                                     
AM         MOVE  WCTR-X TO W-LIG(23:58)                                         
           PERFORM ECR-LIG                                                      
           MOVE W-NSOCC-R    TO W-NSOCC                                         
           MOVE W-SERVDEST   TO W-SERV                                          
           MOVE IGR          TO IG                                              
           PERFORM STOCKE-ADR                                                   
           PERFORM STOCKE-CTA                                                   
      *                                                                         
           MOVE '0'     TO W-ASA                                                
           MOVE ' '     TO W-LIG                                                
           MOVE WCTA-LIG (1)      TO W-LIG(02:22)                               
           MOVE WADR-ADR (1)      TO W-LIG(50:30)                               
           PERFORM ECR-LIG                                                      
           MOVE ' '     TO W-ASA                                                
           MOVE WCTA-LIG (2)      TO W-LIG(02:22)                               
           MOVE WADR-ADR (2)  TO W-LIG(50:30)                                   
           PERFORM ECR-LIG                                                      
           MOVE WCTA-LIG (3)  TO W-LIG(02:22)                                   
           MOVE WADR-ADR (3)  TO W-LIG(50:30)                                   
           PERFORM ECR-LIG                                                      
           MOVE WCTA-LIG (4)  TO W-LIG(02:22)                                   
           MOVE WADR-ADR (4)  TO W-LIG(50:30)                                   
           PERFORM ECR-LIG                                                      
           MOVE WCTA-LIG (5)  TO W-LIG(02:22)                                   
           MOVE WADR-ADR (5)  TO W-LIG(50:30)                                   
           PERFORM ECR-LIG                                                      
           MOVE ' '           TO W-LIG                                          
           MOVE WADR-ADR (6)  TO W-LIG(50:30)                                   
           PERFORM ECR-LIG                                                      
AM    *----REMONTER LE N� DE PAGE SUR LA LIGNE DU N� ORIGINE                    
PHRI       MOVE SPACES             TO H01-TVA-INTRA                             
PHRI       MOVE SPACES             TO W-LIG                                     
AM         IF W-NORIG > SPACES                                                  
AM            MOVE H01        TO W-LIG                                          
AM         END-IF                                                               
PHRI  *--- ECRITURE DU LIBELLE TVA POUR SOCIETE RECEPTRICE INTRACOMM.           
PHRI       IF W-CODEGEO = '2'                                                   
"             MOVE 'TVA N� : '     TO H01-LIB-INTRA                             
"             MOVE WLIB2-TVA-INTRA TO H01-TX-INTRA                              
"             MOVE H01-TVA-INTRA   TO W-LIG(36:34)                              
"          ELSE                                                                 
"             MOVE SPACES          TO W-LIG(36:34)                              
PHRI       END-IF                                                               
AM         MOVE W-NP1              TO Z-NP1                                     
AM         MOVE W-NP2              TO Z-NP2                                     
AM         MOVE WPAG-ZONE          TO W-LIG(71:10)                              
AM         PERFORM ECR-LIG                                                      
AM         MOVE ' '                TO W-ASA                                     
AM         MOVE SPACES             TO W-LIG                                     
AM         PERFORM ECR-LIG                                                      
AM         MOVE ' '                TO W-ASA                                     
AM    *--- EDITION DU COMMENTAIRE SPECIFIQUE                                    
AM         IF WFGCOM-NBP > 0                                                    
AM            PERFORM VARYING J FROM 1 BY 1 UNTIL J > WFGCOM-NBP                
AM               MOVE WFGCOM-LIBL(J)  TO W-LIG                                  
AM               PERFORM ECR-LIG                                                
AM            END-PERFORM                                                       
AM    *                                                                         
AM    *------ ECRITURE D'UNE LIGNE BLANCHE AVANT L'ENTETE                       
AM            MOVE ' '         TO W-ASA                                         
AM            MOVE SPACES      TO W-LIG                                         
AM            PERFORM ECR-LIG                                                   
AM         END-IF                                                               
      *--- EDITION DE L'ENTETE DE FACTURE                                       
           IF WFG06-NBE > 0                                                     
              MOVE ALL '-' TO W-LIG                                             
              PERFORM ECR-LIG                                                   
              MOVE ' ' TO W-LIG                                                 
              PERFORM VARYING IFG06 FROM 1 BY 1 UNTIL IFG06 > WFG06-NBE         
                 MOVE WFG06-LCOMM(IFG06) TO W-LIG(5:70)                         
                 PERFORM ECR-LIG                                                
              END-PERFORM                                                       
           END-IF                                                               
           MOVE ALL '-' TO W-LIG                                                
           PERFORM ECR-LIG                                                      
           MOVE '0'     TO W-ASA                                                
           MOVE SPACE   TO W-LIG                                                
           PERFORM ECR-LIG                                                      
           MOVE '0'     TO W-ASA                                                
           MOVE L00     TO W-LIG                                                
           PERFORM ECR-LIG                                                      
           MOVE ' '     TO W-ASA                                                
           MOVE L01     TO W-LIG                                                
           PERFORM ECR-LIG                                                      
           MOVE ' '     TO W-LIG.                                               
           MOVE  0      TO J.                                                   
       FIN-EDITION-HAUT.                                                        
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *     EDITION DU PIED DE FACTURE                                          
       EDITION-BAS SECTION.                                                     
           MOVE SPACE TO W-LIG                                                  
           PERFORM UNTIL J NOT < W-NDISP                                        
*******       DISPLAY 'AVT CALCUL K= ' K ' W-DISP= ' W-NDISP ' J= ' J           
              COMPUTE  K = W-NDISP - J                                          
              EVALUATE K                                                        
                 WHEN  1       MOVE ' ' TO W-ASA                                
                 WHEN  2       MOVE '0' TO W-ASA                                
                 WHEN  OTHER   MOVE '-' TO W-ASA                                
                               MOVE  3  TO K                                    
              END-EVALUATE                                                      
*******       DISPLAY 'K REEL = ' K                                             
              PERFORM ECR-LIG                                                   
              ADD K TO J                                                        
           END-PERFORM                                                          
           MOVE ALL '-' TO W-LIG                                                
           MOVE     '0' TO W-ASA                                                
           PERFORM      ECR-LIG                                                 
      *                                                                         
AM         IF TVA-OUI AND TVA-OK                                                
              MOVE '0'  TO W-ASA                                                
              MOVE B00  TO W-LIG         PERFORM ECR-LIG                        
              MOVE ' '  TO W-ASA                                                
              MOVE B01  TO W-LIG         PERFORM ECR-LIG                        
              MOVE B02  TO W-LIG         PERFORM ECR-LIG                        
              MOVE B03  TO W-LIG         PERFORM ECR-LIG                        
              MOVE B04  TO W-LIG         PERFORM ECR-LIG                        
              MOVE B05  TO W-LIG         PERFORM ECR-LIG                        
           ELSE                                                                 
              MOVE '0'  TO W-ASA                                                
              MOVE B00  TO W-LIG         PERFORM ECR-LIG                        
              MOVE ' '  TO W-ASA                                                
              MOVE B01  TO W-LIG         PERFORM ECR-LIG                        
              MOVE '0'  TO W-ASA                                                
              MOVE B03  TO W-LIG         PERFORM ECR-LIG                        
              MOVE ' '  TO W-LIG         PERFORM ECR-LIG                        
           END-IF                                                               
           IF W-NP1 = W-NP2                                                     
              MOVE SPACE TO W-LIG                                               
              MOVE 1     TO P                                                   
AM            IF TVA-OUI AND TVA-OK                                             
                 STRING W-LIBTVA     DELIMITED '  ' INTO W-LIG                  
                        WITH POINTER P                                          
                 ADD 1 TO            P                                          
              END-IF                                                            
              IF ESC-NON                                                        
                 STRING W-LIBESC-NON DELIMITED '  ' INTO W-LIG                  
                        WITH POINTER P                                          
                 ADD 1 TO            P                                          
              END-IF                                                            
              IF ESC-OUI                                                        
                 STRING W-LIBESC-OUI DELIMITED '  ' INTO W-LIG                  
                        WITH POINTER P                                          
                 IF ESC-UNIQ                                                    
                    MOVE   W-TXESCPT TO Z-TXESCPT                               
                    STRING Z-TXESCPT '%' DELIMITED SIZE INTO W-LIG              
                           WITH POINTER P                                       
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE ' ' TO W-LIG                                                 
           END-IF                                                               
           MOVE '0' TO W-ASA                                                    
           PERFORM ECR-LIG                                                      
AM         IF W-NP1 = W-NP2                                                     
AM            IF W-DVALEUR > SPACES                                             
AM               MOVE B07   TO  W-LIG                                           
AM            ELSE                                                              
                 IF REG-OUI                                                     
                    MOVE B06  TO W-LIG                                          
                 ELSE                                                           
                    MOVE ' '  TO W-LIG                                          
                 END-IF                                                         
AM            END-IF                                                            
AM         END-IF                                                               
           MOVE ' '  TO W-ASA                                                   
           PERFORM ECR-LIG                                                      
           MOVE '-' TO W-ASA                                                    
           MOVE ' ' TO W-LIG                                                    
           PERFORM VARYING L FROM 1 BY 1 UNTIL L > WLIB-NBP                     
              MOVE WLIB-LIB(L) TO W-LIG(10:70)                                  
              PERFORM ECR-LIG                                                   
              MOVE ' ' TO W-ASA                                                 
           END-PERFORM.                                                         
       FIN-BAS-PAGE.                                                            
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *                                                                         
      * ELIMINATION DES LOW-VALUE ET ECRITURE                                   
       ECR-LIG SECTION.                                                         
           INSPECT W-LIG REPLACING ALL '' BY ' '                                
      ****                              -   : LOW-VALUE                         
           PERFORM ECR-FACT.                                                    
       FIN-ECR-LIG.                                                             
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *    PREPARATION DE L'ADRESSE                                             
       STOCKE-ADR SECTION.                                                      
           INITIALIZE WADR-ZONE                                                 
           SET IADR TO 1                                                        
      *                                                                         
      *    MOVE WSTE-LLIEU(IG) TO WADR-ADR(IADR)                                
      *    SET IADR UP BY 1                                                     
      *                                                                         
           IF WSTE-LADR1(IG) > SPACE                                            
              MOVE WSTE-LADR1(IG) TO WADR-ADR(IADR)                             
              SET IADR UP BY 1                                                  
           END-IF                                                               
      *                                                                         
           IF WSTE-LADR2(IG) > SPACE                                            
              MOVE WSTE-LADR2(IG) TO WADR-ADR(IADR)                             
              SET IADR UP BY 1                                                  
           END-IF                                                               
      *                                                                         
           IF WSTE-LADR3(IG) > SPACE                                            
              MOVE WSTE-LADR3(IG) TO WADR-ADR(IADR)                             
              SET IADR UP BY 1                                                  
           END-IF                                                               
      *                                                                         
           MOVE WSTE-CPOSTAL (IG) TO WADR-ADR(IADR)(1:6)                        
           MOVE WSTE-LCOMMUNE(IG) TO WADR-ADR(IADR)(7:30)                       
           SET IADR UP BY 1                                                     
      *                                                                         
           IF W-SERV > SPACE                                                    
              PERFORM VARYING ISERV FROM 1 BY 1 UNTIL ISERV > WSERV-NBP         
                 OR (WSERV-NSOCC(ISERV) = W-NSOCC AND                           
                     WSERV-SERV (ISERV) = W-SERV      )                         
              END-PERFORM                                                       
              IF ISERV > WSERV-NBP                                              
                 MOVE 'FGSER' TO GA01-CTABLEG1                                  
                 MOVE W-NSOCC TO GA01-CTABLEG2                                  
                 MOVE W-SERV  TO GA01-CTABLEG2(4:5)                             
                 PERFORM SELECT-GA01                                            
                 IF TROUVE                                                      
                    MOVE GA01-WTABLEG(2:20) TO W-LSERV                          
                 ELSE                                                           
                    MOVE W-SERV             TO W-LSERV                          
                 END-IF                                                         
                 IF ISERV NOT > WSERV-MAX                                       
                    SET WSERV-NBP TO ISERV                                      
                    MOVE W-NSOCC TO WSERV-NSOCC(ISERV)                          
                    MOVE W-SERV  TO WSERV-SERV (ISERV)                          
                    MOVE W-LSERV TO WSERV-LSERV(ISERV)                          
                 END-IF                                                         
              ELSE                                                              
                 MOVE WSERV-LSERV(ISERV) TO W-LSERV                             
              END-IF                                                            
              MOVE W-LSERV  TO WADR-ADR(6)                                      
           END-IF.                                                              
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      * REFERENCES COMPTABLES                                                   
       STOCKE-CTA SECTION.                                                      
           INITIALIZE WCTA-ZONE                                                 
           IF W-DCTA > SPACE                                                    
1197          MOVE W-DEST-FG02  TO FG02-NSOCCOMPT                               
              MOVE W-TYPF       TO FG02-CTYPPIECE                               
              MOVE W-NUMF       TO FG02-NUMFACT                                 
              MOVE W-NATF       TO FG02-NATFACT                                 
              MOVE W-DCTA       TO FG02-DCOMPTA                                 
              MOVE '001'        TO FG02-NLIGNE                                  
1295          MOVE W-TSOCFACT   TO FG02-TSOCFACT                                
              PERFORM SELECT-FG02                                               
              IF TROUVE                                                         
                 MOVE '**********************' TO WCTA-LIG(1)                   
                 MOVE '* JOURNAL            *' TO WCTA-LIG(2)                   
                 MOVE '* PIECE              *' TO WCTA-LIG(3)                   
                 MOVE '*    DU     /  /     *' TO WCTA-LIG(4)                   
                 MOVE '**********************' TO WCTA-LIG(5)                   
      *                                                                         
                 MOVE FG02-NJRN         TO WCTA-LIG(2)(11:10)                   
                 MOVE FG02-NPIECE       TO WCTA-LIG(3)(11:10)                   
                 MOVE    W-DCTA(7:2)    TO WCTA-LIG(4)(11:02)                   
                 MOVE    W-DCTA(5:2)    TO WCTA-LIG(4)(14:02)                   
                 MOVE    W-DCTA(1:4)    TO WCTA-LIG(4)(17:04)                   
              END-IF                                                            
           END-IF.                                                              
       FIN-STOCKE-CTA.                                                          
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *                                                                         
      *    CENTRAGE D'UNE ZONE                                                  
       CENTRAGE SECTION.                                                        
           MOVE 1      TO WCTR-I1                                               
           MOVE WCTR-L TO WCTR-I2                                               
           PERFORM UNTIL WCTR-I1 > WCTR-L                                       
                   OR    WCTR-X(WCTR-I1:WCTR-I2) = SPACE                        
              ADD      1 TO   WCTR-I1                                           
              SUBTRACT 1 FROM WCTR-I2                                           
           END-PERFORM                                                          
           IF WCTR-I1 < WCTR-L                                                  
              COMPUTE WCTR-I2 = ((WCTR-L - WCTR-I1) / 2) + 2                    
              MOVE WCTR-X  TO WCTR-XR                                           
              MOVE SPACE   TO WCTR-X                                            
              MOVE WCTR-XR TO WCTR-X(WCTR-I2:WCTR-I1)                           
           END-IF.                                                              
       FIN-CENTRAGE.                                                            
         EXIT.                                                                  
      *                                                                         
      *                                                                         
      *                                                                         
      *    RECHERCHE GA10 ET STOCKAGE                                           
      *--------------------*                                                    
       RECH-NSOC SECTION.                                                       
      *--------------------*                                                    
           PERFORM RECH-WGA10                                                   
           MOVE    WGA10-NSOCC(I) TO W-NSOCC                                    
DRA        MOVE    WGA10-NETAB(I) TO W-NETAB                                    
PHRI       MOVE    WGA10-CODEBDF(I) TO W-CODEBDF                                
           PERFORM RECH-STE                                                     
           MOVE    WSTE-NSOCF (I) TO W-NSOCF.                                   
      *                                                                         
      *    RECHERCHE STE DE COMPTABILISATION D'UN LIEU GA10                     
      *--------------------*                                                    
       RECH-WGA10 SECTION.                                                      
      *--------------------*                                                    
           PERFORM VARYING     I  FROM 1 BY 1 UNTIL I > WGA10-NBP               
               OR (WGA10-NSOC (I) = W-NSOC AND                                  
                   WGA10-NLIEU(I) = W-NLIEU   )                                 
           END-PERFORM                                                          
           IF I > WGA10-NBP                                                     
              IF I > WGA10-MAX                                                  
                 MOVE 'WGA10'    TO W-TABLE                                     
                 PERFORM DEPASSEMENT-TABLE                                      
              END-IF                                                            
              MOVE W-NSOC  TO GA10-NSOCIETE                                     
              MOVE W-NLIEU TO GA10-NLIEU                                        
              PERFORM  SELECT-GA10                                              
              IF NON-TROUVE                                                     
                 STRING GA10-NSOCIETE ' ' GA10-NLIEU ' NON TROUVE GA10'         
                        DELIMITED SIZE INTO WFG-MESS                            
              END-IF                                                            
              MOVE I TO WGA10-NBP                                               
              MOVE GA10-NSOCIETE   TO WGA10-NSOC (I)                            
              MOVE GA10-NLIEU      TO WGA10-NLIEU(I)                            
              MOVE GA10-NSOCCOMPT  TO WGA10-NSOCC(I)                            
DRA           MOVE GA10-NLIEUCOMPT TO WGA10-NETAB(I)                            
PHRI          MOVE GA10-CODEBDF    TO WGA10-CODEBDF(I)                          
           END-IF.                                                              
       FIN-RECH-GA10.                                                           
         EXIT.                                                                  
      *                                                                         
      *       RECHERCHE ADRESSE STE DE COMPTABILISATION                         
      *--------------------*                                                    
       RECH-STE SECTION.                                                        
      *--------------------*                                                    
DRA        IF W-NSOCC = '945' AND                                               
DRA          (W-NETAB = '837' OR '939')                                         
DRA           MOVE W-NETAB  TO  W-NSOC-RECH                                     
DRA        ELSE                                                                 
DRA           MOVE W-NSOCC  TO  W-NSOC-RECH                                     
DRA        END-IF.                                                              
      *                                                                         
           PERFORM VARYING    I  FROM 1 BY 1 UNTIL I > WSTE-NBP                 
DRA            OR  WSTE-NSOC (I) = W-NSOC-RECH                                  
           END-PERFORM                                                          
           IF I > WSTE-NBP                                                      
              IF I > WSTE-MAX                                                   
                 MOVE 'WSTE'    TO W-TABLE                                      
                 PERFORM DEPASSEMENT-TABLE                                      
              END-IF                                                            
DRA   ********MOVE W-NSOCC TO GA10-NSOCIETE                                     
DRA           MOVE W-NSOC-RECH TO GA10-NSOCIETE                                 
              MOVE '000'       TO GA10-NLIEU                                    
              PERFORM  SELECT-GA10                                              
              IF NON-TROUVE                                                     
                 STRING GA10-NSOCIETE ' 000 NON TROUVE GA10'                    
                        DELIMITED SIZE INTO WFG-MESS                            
                 PERFORM FIN-ANORMALE                                           
              END-IF                                                            
              MOVE I              TO WSTE-NBP                                   
              INITIALIZE             WSTE-POSTE      (I)                        
              MOVE GA10-NSOCIETE  TO WSTE-NSOC       (I)                        
              MOVE GA10-LLIEU     TO WSTE-LLIEU      (I)                        
              MOVE GA10-LADRLIEU1 TO WSTE-LADR1      (I)                        
              MOVE GA10-LADRLIEU2 TO WSTE-LADR2      (I)                        
              MOVE GA10-LADRLIEU3 TO WSTE-LADR3      (I)                        
              MOVE GA10-CPOSTAL   TO WSTE-CPOSTAL    (I)                        
              MOVE GA10-LCOMMUNE  TO WSTE-LCOMMUNE   (I)                        
              MOVE 'FGSOC'        TO GA01-CTABLEG1                              
              MOVE W-NSOCC        TO GA01-CTABLEG2                              
              PERFORM SELECT-GA01                                               
              IF  TROUVE                                                        
              AND GA01-WTABLEG(1:1) = 'O'                                       
                  MOVE GA01-WTABLEG(2:3) TO WSTE-NSOCF(I)                       
              ELSE                                                              
                  MOVE W-NSOCC           TO WSTE-NSOCF(I)                       
              END-IF                                                            
           END-IF.                                                              
       FIN-RECH-STE.                                                            
         EXIT.                                                                  
      *                                                                         
PHRI  *    RECHERCHE CODE GEOGRAPHIQUE                                          
"     *--------------------*                                                    
"      RECH-CODEGEO SECTION.                                                    
"     *--------------------*                                                    
"          MOVE W-CODEBDF          TO FM55-CODEBDF                              
"          PERFORM  SELECT-FM55                                                 
"          IF NON-TROUVE                                                        
"             STRING 'CODE PAYS ' FM55-CODEBDF ' NON TROUVE FM55'               
"             DELIMITED SIZE INTO WFG-MESS                                      
"          END-IF                                                               
"          MOVE FM55-CODEGEO       TO W-CODEGEO.                                
       FIN-RECH-CODEGEO.                                                        
         EXIT.                                                                  
      *                                                                         
       DEPASSEMENT-TABLE SECTION.                                               
           EVALUATE W-TABLE                                                     
             WHEN 'WNAT'   MOVE WNAT-MAX                 TO W-MAX               
                           MOVE 'NATURES DE FACTUR.'     TO W-LIB               
             WHEN 'WTYP'   MOVE WTYP-MAX                 TO W-MAX               
                           MOVE 'TYPES DE DOCUMENTS'     TO W-LIB               
             WHEN 'WVEN  ' MOVE WVEN-MAX                 TO W-MAX               
                           MOVE 'CRITERES PAR NATURE'    TO W-LIB               
             WHEN 'WTVA'   MOVE WTVA-MAX                 TO W-MAX               
                           MOVE 'TAUX DE TVA '           TO W-LIB               
             WHEN 'WGA10'  MOVE WGA10-MAX                TO W-MAX               
                           MOVE 'LIEUX DE GA10    '      TO W-LIB               
             WHEN 'WSTE'   MOVE WSTE-MAX                 TO W-MAX               
                           MOVE 'SOCIETES DE COMPTA'     TO W-LIB               
             WHEN 'WLIB'   MOVE WLIB-MAX                 TO W-MAX               
                           MOVE 'LIBELLES RCS ..'        TO W-LIB               
             WHEN 'WFG01'  MOVE WFG01-MAX                TO W-MAX               
                           MOVE 'LIGNES PAR FACTURE'     TO W-LIB               
             WHEN 'WFG06'  MOVE WFG06-MAX                TO W-MAX               
                           MOVE 'OMMENT. PAR FACTURE'    TO W-LIB               
             WHEN 'WCOM'   MOVE WCOM-MAX                 TO W-MAX               
                           MOVE 'TOUS COMM. SPECIF. '    TO W-LIB               
             WHEN 'WFGCO'  MOVE WFGCOM-MAX               TO W-MAX               
                           MOVE 'COMM. SPECIF. FACT.'    TO W-LIB               
PHRI         WHEN 'WLIB2'  MOVE WLIB2-MAX                TO W-MAX               
PHRI                       MOVE 'TAUX TVA INTRACOMM.'    TO W-LIB               
           END-EVALUATE                                                         
           STRING  '*** AUGMENTER ' W-TABLE '-MAX ET OCCURS.'                   
                   DELIMITED SIZE INTO WFG-MESS                                 
           PERFORM FIN-ANORMALE.                                                
       FIN-DEPASSEMENT-TABLE.                                                   
         EXIT.                                                                  
      *                                                                         
                                                                                
