      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *    STOCKAGE LIGNE DES FACTURES                                          
       01  WFG01-ZONE.                                                          
           5    WFG01-TABLE.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WFG01-NBP      PIC S999 COMP.                                   
      *--                                                                       
            7   WFG01-NBP      PIC S999 COMP-5.                                 
      *}                                                                        
1097        7   WFG01-POSTE                  OCCURS 400.                        
             9  WFG01-NLIG     PIC X(3).                                        
             9  WFG01-CVENT    PIC X(5).                                        
             9  WFG01-TVA      PIC X(5).                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      9  WFG01-ESCPT    PIC S9(03)V99  COMP.                             
      *--                                                                       
             9  WFG01-ESCPT    PIC S9(03)V99 COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      9  WFG01-PUF      PIC S9(11)V99  COMP.                             
      *--                                                                       
             9  WFG01-PUF      PIC S9(11)V99 COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      9  WFG01-QTF      PIC S9(04)V999 COMP.                             
      *--                                                                       
             9  WFG01-QTF      PIC S9(04)V999 COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      9  WFG01-MTHT     PIC S9(11)V99  COMP.                             
      *--                                                                       
             9  WFG01-MTHT     PIC S9(11)V99 COMP-5.                            
      *}                                                                        
             9  WFG01-LIBC     PIC X(25).                                       
             9  WFG01-LIBL     PIC X(40).                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      9  WFG01-NP       PIC S9(3)      COMP.                             
      *--                                                                       
             9  WFG01-NP       PIC S9(3) COMP-5.                                
      *}                                                                        
      *    STOCKAGE COMMENTAIRES SUR FACTURE                                    
       01  WFG06-ZONE.                                                          
           5    WFG06-TABLE.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WFG06-NBP      PIC S999 COMP.                                   
      *--                                                                       
            7   WFG06-NBP      PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WFG06-NBE      PIC S999 COMP.                                   
      *--                                                                       
            7   WFG06-NBE      PIC S999 COMP-5.                                 
      *}                                                                        
            7   WFG06-POSTE                  OCCURS 100.                        
             9  WFG06-NLIG     PIC X(3).                                        
             9  WFG06-LCOMM    PIC X(70).                                       
AM    *    STOCKAGE LIBELLE SPECIFIQUE POUR LE TYPE DE DOCUMENT                 
AM     01  WFGCOM-ZONE.                                                         
AM         5    WFGCOM-TABLE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *     7   WFGCOM-NBP     PIC S999 COMP.                                   
      *--                                                                       
            7   WFGCOM-NBP     PIC S999 COMP-5.                                 
      *}                                                                        
AM          7   WFGCOM-POSTE                 OCCURS 15.                         
AM           9  WFGCOM-LIBL    PIC X(76).                                       
                                                                                
