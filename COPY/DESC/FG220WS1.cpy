      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
       01  VARIABLES-NUM.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 I                PIC S999        COMP.                            
      *--                                                                       
           05 I                PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 J                PIC S9999       COMP.                            
      *--                                                                       
           05 J                PIC S9999 COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 K                PIC S999        COMP.                            
      *--                                                                       
           05 K                PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 L                PIC S999        COMP.                            
      *--                                                                       
           05 L                PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 P                PIC S999        COMP.                            
      *--                                                                       
           05 P                PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IG               PIC S999        COMP.                            
      *--                                                                       
           05 IG               PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IGE              PIC S999        COMP.                            
      *--                                                                       
           05 IGE              PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IGR              PIC S999        COMP.                            
      *--                                                                       
           05 IGR              PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 W-MAX            PIC S9(5)       COMP.                            
      *--                                                                       
           05 W-MAX            PIC S9(5) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
1295  *    05 I-STD            PIC S999        COMP.                            
      *--                                                                       
           05 I-STD            PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
1295  *    05 I-NATURE         PIC S999        COMP.                            
      *--                                                                       
           05 I-NATURE         PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
1295  *    05 I-CTRL           PIC S999        COMP.                            
      *--                                                                       
           05 I-CTRL           PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IFG01            PIC S999        COMP.                            
      *--                                                                       
           05 IFG01            PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IFG06            PIC S999        COMP.                            
      *--                                                                       
           05 IFG06            PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 ITVA             PIC S999        COMP.                            
      *--                                                                       
           05 ITVA             PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 W-NSEQ           PIC S9(7)       COMP.                            
      *--                                                                       
           05 W-NSEQ           PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 W-NDISP          PIC S999        COMP.                            
      *--                                                                       
           05 W-NDISP          PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *    05 W-NDISP2         PIC S999        COMP.                            
      *--                                                                       
           05 W-NDISP2         PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 W-NP1            PIC S999        COMP.                            
      *--                                                                       
           05 W-NP1            PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 W-NP2            PIC S999        COMP.                            
      *--                                                                       
           05 W-NP2            PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 W-DIFF           PIC S999        COMP.                            
      *--                                                                       
           05 W-DIFF           PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *    05 W-NBP-MAX-FG06   PIC S999        COMP.                            
      *--                                                                       
           05 W-NBP-MAX-FG06   PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *    05 W-NBP-MAX-LFACT  PIC S999        COMP.                            
      *--                                                                       
           05 W-NBP-MAX-LFACT  PIC S999 COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *    05 W-NBP-TRT        PIC S999        COMP.                            
      *--                                                                       
           05 W-NBP-TRT        PIC S999 COMP-5.                                 
      *}                                                                        
           05 W-MONTANTS-FACT.                                                  
            7 W-MT             PIC S9(11)V99   COMP-3.                          
            7 W-MTTVA          PIC S9(11)V99   COMP-3.                          
            7 W-TOTHT          PIC S9(11)V99   COMP-3.                          
            7 W-NETHT          PIC S9(11)V99   COMP-3.                          
            7 W-TOTESC         PIC S9(11)V99   COMP-3.                          
            7 W-TOTTVA         PIC S9(11)V99   COMP-3.                          
            7 W-TOTTTC         PIC S9(11)V99   COMP-3.                          
            7 W-TAUX           PIC S9(03)V99   COMP-3.                          
            7 W-TXESCPT        PIC S9(03)V99   COMP-3.                          
            7 Z-TXESCPT        PIC ZZ9,99.                                      
       01  VARIABLES-ALPHA.                                                     
           05 W-NSOC           PIC X(03).                                       
           05 W-NSOCF          PIC X(03).                                       
           05 W-NSOC-E         PIC X(03).                                       
           05 W-NSOC-LIB       PIC X(03).                                       
           05 W-NSOC-R         PIC X(03).                                       
           05 W-NSOCCOMPT      PIC X(03).                                       
           05 W-NSOCC          PIC X(03).                                       
           05 W-NSOCC-R        PIC X(03).                                       
      *                                                                         
DRA        05 W-NSOC-RECH      PIC X(03).                                       
DRA        05 W-NETAB          PIC X(03).                                       
DRA        05 W-NETAB-E        PIC X(03).                                       
DRA        05 W-NETAB-R        PIC X(03).                                       
      *                                                                         
           05 W-NSOCF-E        PIC X(03).                                       
           05 W-NSOCF-R        PIC X(03).                                       
           05 W-NLIEU          PIC X(03).                                       
           05 W-NLIEU-E        PIC X(03).                                       
           05 W-NLIEU-R        PIC X(03).                                       
           05 W-CCHRONO        PIC X(02).                                       
           05 W-SERV           PIC X(05).                                       
           05 W-SERVORIG       PIC X(05).                                       
           05 W-SERVDEST       PIC X(05).                                       
           05 W-LSERV          PIC X(20).                                       
           05 W-NATF           PIC X(05).                                       
AM         05 W-NORIG          PIC X(07).                                       
           05 W-NUMF           PIC X(07).                                       
           05 W-TYPF           PIC X(02).                                       
           05 W-DATF           PIC X(10).                                       
AM         05 W-DVALEUR        PIC X(8).                                        
           05 W-DCTA           PIC X(08).                                       
           05 W-TABLE          PIC X(05).                                       
           05 W-ER             PIC X(01).                                       
           05 W-DEST           PIC X(03).                                       
           05 W-DOC            PIC X(15).                                       
           05 W-LTYP           PIC X(25).                                       
           05 W-LNATF          PIC X(25).                                       
           05 W-LIB            PIC X(30).                                       
           05 W-LIBPRO         PIC X(11).                                       
           05 W-ASA            PIC X(01).                                       
           05 W-LIG            PIC X(80).                                       
           05 WFG-MESS         PIC X(30).                                       
AM         05 W-TYPE           PIC X(02).                                       
AM         05 W-NPARAG         PIC 9(01).                                       
AM         05 W-NLIGNE         PIC 9(2).                                        
AM         05 W-NLIG-MAX-FG06  PIC X(03).                                       
AM         05 W-NLIG-MAX-LFACT PIC X(03).                                       
AM         05 W-NLIG-TRT       PIC X(03).                                       
1295       05 W-TSOCFACT       PIC X(01).                                       
1295       05 W-NATCOM         PIC X(05).                                       
1197       05 W-DEST-FG02      PIC X(03).                                       
PHRI       05 W-CODEBDF        PIC X(02).                                       
PHRI       05 W-CODEGEO        PIC X(01).                                       
      *    ZONE POUR CENTRAGE D'UN LIBELLE                                      
           05 WCTR-ZONE.                                                        
            8 WCTR-L  PIC 99.                                                   
            8 WCTR-X  PIC X(80).                                                
            8 WCTR-XR PIC X(80).                                                
            8 WCTR-I1 PIC 99.                                                   
            8 WCTR-I2 PIC 99.                                                   
AM    *    ZONES DE LA SOUS-TABLE FGCOM                                         
AM         05 W-FGCOM-CTABLEG2.                                                 
AM          8 W-FGCOM-TYPE     PIC X(2).                                        
1295        8 W-FGCOM-NATCOM   PIC X(5).                                        
AM          8 W-FGCOM-NPARAG   PIC 9(1).                                        
AM          8 W-FGCOM-NLIGNE   PIC 9(2).                                        
AM         05 W-FGCOM-WTABLEG.                                                  
AM          8 W-FGCOM-ACTIF    PIC X(1).                                        
AM          8 W-FGCOM-COMMENT  PIC X(38).                                       
       01  VARIABLES-CONDITIONS.                                                
           05 W-EDITION-TP      PIC 9.                                          
           88   EDITION-BATCH       VALUE 0.                                    
           88   EDITION-TP          VALUE 1.                                    
           05 W-PAGE-NUM        PIC 9.                                          
           88   PAGE-NUM1     VALUE 0.                                          
           88   PAGE-NUM2     VALUE 1.                                          
           05 W-FIN-FG01       PIC 9.                                           
           88   FIN-FG01     VALUE 1.                                           
           05 W-FIN-FG06       PIC 9.                                           
           88   FIN-FG06     VALUE 1.                                           
           05 W-FIN-FG07       PIC 9.                                           
           88   FIN-FG07     VALUE 1.                                           
           05 W-TVA            PIC 9.                                           
           88   TVA-NON      VALUE 0.                                           
           88   TVA-OUI      VALUE 1.                                           
           05 W-REG            PIC 9.                                           
           88   REG-NON      VALUE 0.                                           
           88   REG-OUI      VALUE 1.                                           
           05 W-ESC            PIC 9.                                           
           88   ESC-NON      VALUE 0.                                           
           88   ESC-OUI      VALUE 1 THRU 2.                                    
           88   ESC-UNIQ     VALUE 1.                                           
           88   ESC-MULT     VALUE 2.                                           
AM         05 W-COMMENT        PIC 9.                                           
AM         88   DEBUT-COM    VALUE 0.                                           
AM         88   FIN-COM      VALUE 1.                                           
           05 TOP-TVA          PIC 9.                                           
           88   TVA-ZERO     VALUE 0.                                           
           88   TVA-OK       VALUE 1.                                           
      *    LIBELLES LEGAUX                                                      
       01     W-LIBELLES-LEGAUX.                                                
           05 W-LIBTVA  PIC X(28) VALUE 'TVA ACQUITTEE SUR LES DEBITS'.         
           05 W-LIBESC-NON PIC X(22) VALUE 'PAIEMENT SANS ESCOMPTE'.            
           05 W-LIBESC-OUI PIC X(22) VALUE 'PAIEMENT AVEC ESCOMPTE'.            
      *    MAX POUR LIGNES FACTURES : LIE A COPY FG220WS2                       
       01     WFG-MAX.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
1097  *    05 WFG01-MAX   PIC S999 COMP VALUE 400.                              
      *--                                                                       
           05 WFG01-MAX   PIC S999 COMP-5 VALUE 400.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 WFG06-MAX   PIC S999 COMP VALUE 100.                              
      *--                                                                       
           05 WFG06-MAX   PIC S999 COMP-5 VALUE 100.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *    05 WFGCOM-MAX  PIC S999 COMP VALUE 15.                               
      *--                                                                       
           05 WFGCOM-MAX  PIC S999 COMP-5 VALUE 15.                             
      *}                                                                        
      *    BORNES POUR DECLARE GA01                                             
       01  WPAG-ZONE.                                                           
              05 FILLER      PIC X(5) VALUE 'PAGE '.                            
              05 Z-NP1       PIC Z9.                                            
              05 FILLER      PIC X     VALUE '/'.                               
              05 Z-NP2       PIC Z9.                                            
      *    STOCKAGE TYPES DE DOCUMENT.                                          
       01  WTYP-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WTYP-MAX       PIC S999 COMP VALUE  050.                        
      *--                                                                       
           5    WTYP-MAX       PIC S999 COMP-5 VALUE  :MAXWTYP:.                
      *}                                                                        
           5    WTYP-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WTYP-NBP       PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WTYP-NBP       PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WTYP-POSTE                   OCCURS :MAXWTYP:.                  
             9  WTYP-TYPF      PIC X(2).                                        
             9  WTYP-LTYP      PIC X(25).                                       
             9  WTYP-TVA       PIC X(01).                                       
             9  WTYP-REG       PIC X(01).                                       
AM    *    STOCKAGE LIBELLES SPECIFIQUES PAR TYPE DE DOCUMENT                   
AM     01  WCOM-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WCOM-MAX       PIC S999 COMP VALUE  070.                        
      *--                                                                       
           5    WCOM-MAX       PIC S999 COMP-5 VALUE  :MAXWCOM:.                
      *}                                                                        
AM         5    WCOM-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *     7   WCOM-NBP       PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WCOM-NBP       PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
AM          7   WCOM-POSTE                   OCCURS :MAXWCOM:.                  
AM           9  WCOM-TYPF      PIC X(2).                                        
1295         9  WCOM-NATCOM    PIC X(5).                                        
AM           9  WCOM-NPARAG    PIC 9(1).                                        
AM           9  WCOM-NLIGNE    PIC 9(2).                                        
AM           9  WCOM-COMMENT   PIC X(76).                                       
      *    STOCKAGE LIBELLES CRITERES PAR NATURE                                
       01  WVEN-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WVEN-MAX       PIC S9999 COMP VALUE  2500.                      
      *--                                                                       
           5    WVEN-MAX       PIC S9999 COMP-5 VALUE  :MAXWVEN:.               
      *}                                                                        
           5    WVEN-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WVEN-NBP       PIC S9999 COMP VALUE 0.                          
      *--                                                                       
            7   WVEN-NBP       PIC S9999 COMP-5 VALUE 0.                        
      *}                                                                        
            7   WVEN-POSTE                   OCCURS :MAXWVEN:.                  
             9  WVEN-NATF      PIC X(05).                                       
             9  WVEN-CVEN      PIC X(05).                                       
             9  WVEN-LVEN      PIC X(25).                                       
      *    STOCKAGE LIBELLES NATURE                                             
       01  WNAT-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WNAT-MAX       PIC S999 COMP VALUE  300.                        
      *--                                                                       
           5    WNAT-MAX       PIC S999 COMP-5 VALUE  :MAXWNAT:.                
      *}                                                                        
           5    WNAT-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WNAT-NBP       PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WNAT-NBP       PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WNAT-POSTE                   OCCURS :MAXWNAT:.                  
             9  WNAT-NATF      PIC X(05).                                       
             9  WNAT-LNAT      PIC X(25).                                       
      *    STOCKAGE TAUX DE TVA ET MONTANT PAR TAUX                             
       01  WTVA-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WTVA-MAX       PIC S999 COMP VALUE  10.                         
      *--                                                                       
           5    WTVA-MAX       PIC S999 COMP-5 VALUE  10.                       
      *}                                                                        
           5    WTVA-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WTVA-NBP       PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WTVA-NBP       PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WTVA-POSTE                   OCCURS 10.                         
             8  WTVA-CTAUX     PIC X(5).                                        
             8  WTVA-DEFFET    PIC X(8).                                        
             8  WTVA-TAUXN-X.                                                   
              9 WTVA-TAUXN     PIC S9(03)V99 COMP-3.                            
             8  WTVA-TAUXA-X.                                                   
              9 WTVA-TAUXA     PIC S9(03)V99 COMP-3.                            
           5    WTVA-TABLE2.                                                    
            7   WTVA-POSTE2                  OCCURS 10.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      8  WTVA-TX        PIC S9(03)V99 COMP.                              
      *--                                                                       
             8  WTVA-TX        PIC S9(03)V99 COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      8  WTVA-MTTVA     PIC S9(11)V99 COMP.                              
      *--                                                                       
             8  WTVA-MTTVA     PIC S9(11)V99 COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      8  WTVA-MTHT      PIC S9(11)V99 COMP.                              
      *--                                                                       
             8  WTVA-MTHT      PIC S9(11)V99 COMP-5.                            
      *}                                                                        
PHRI  *    STOCKAGE CODE TVA INTRACOMMUNAUTAIRE                                 
"      01  WLIB2-ZONE.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
"     *    5    WLIB2-MAX       PIC S9 COMP VALUE  1.                           
      *--                                                                       
           5    WLIB2-MAX       PIC S9 COMP-5 VALUE  1.                         
      *}                                                                        
"          5    WLIB2-TABLE.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
"     *     7   WLIB2-NBP       PIC S9 COMP VALUE 0.                            
      *--                                                                       
            7   WLIB2-NBP       PIC S9 COMP-5 VALUE 0.                          
      *}                                                                        
"           7   WLIB2-POSTE.                                                    
PHRI         9  WLIB2-TVA-INTRA PIC X(25).                                      
      *    STOCKAGE LIEUX GA10                                                  
       01  WGA10-ZONE.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WGA10-MAX      PIC S999 COMP VALUE  500.                        
      *--                                                                       
           5    WGA10-MAX      PIC S999 COMP-5 VALUE  :MAXWGA10:.               
      *}                                                                        
           5    WGA10-TABLE.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WGA10-NBP      PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WGA10-NBP      PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WGA10-POSTE                  OCCURS :MAXWGA10:.                 
             9  WGA10-NSOC     PIC X(3).                                        
             9  WGA10-NLIEU    PIC X(3).                                        
             9  WGA10-NSOCC    PIC X(3).                                        
DRA          9  WGA10-NETAB    PIC X(3).                                        
PHRI         9  WGA10-CODEBDF  PIC X(2).                                        
      *                                                                         
      *    STOCKAGE SOCIETES                                                    
       01  WSTE-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WSTE-MAX       PIC S999 COMP VALUE  220.                        
      *--                                                                       
           5    WSTE-MAX       PIC S999 COMP-5 VALUE  :MAXWSTE:.                
      *}                                                                        
           5    WSTE-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WSTE-NBP       PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WSTE-NBP       PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WSTE-POSTE                   OCCURS :MAXWSTE:.                  
             9  WSTE-NSOC      PIC X(3).                                        
             9  WSTE-NSOCF     PIC X(3).                                        
             9  WSTE-LLIEU     PIC X(20).                                       
             9  WSTE-LADR1     PIC X(30).                                       
             9  WSTE-LADR2     PIC X(30).                                       
             9  WSTE-LADR3     PIC X(30).                                       
             9  WSTE-CPOSTAL   PIC X(05).                                       
             9  WSTE-LCOMMUNE  PIC X(30).                                       
      *                                                                         
      *                                                                         
      *    STOCKAGE SERVICES                                                    
       01  WSERV-ZONE.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WSERV-MAX      PIC S999 COMP VALUE  020.                        
      *--                                                                       
           5    WSERV-MAX      PIC S999 COMP-5 VALUE  :MAXWSERV:.               
      *}                                                                        
           5    WSERV-TABLE.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WSERV-NBP      PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WSERV-NBP      PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WSERV-POSTE                  OCCURS :MAXWSERV:                  
                               INDEXED ISERV.                                   
             9  WSERV-NSOCC    PIC X(3).                                        
             9  WSERV-SERV     PIC X(5).                                        
             9  WSERV-LSERV    PIC X(20).                                       
      *                                                                         
      *                                                                         
      *    STOCKAGE LIBELLE LEGAUX STE EN COURS                                 
       01  WLIB-ZONE.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    5    WLIB-MAX       PIC S999 COMP VALUE    4.                        
      *--                                                                       
           5    WLIB-MAX       PIC S999 COMP-5 VALUE    4.                      
      *}                                                                        
           5    WLIB-TABLE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     7   WLIB-NBP       PIC S999 COMP VALUE 0.                           
      *--                                                                       
            7   WLIB-NBP       PIC S999 COMP-5 VALUE 0.                         
      *}                                                                        
            7   WLIB-POSTE                   OCCURS   4.                        
             9  WLIB-LIB       PIC X(60).                                       
      *                                                                         
      *                                                                         
      *                                                                         
      *    STOCKAGE ADRESSE                                                     
       01  WADR-ZONE.                                                           
           5    WADR-ADR       PIC X(40) OCCURS 6 INDEXED IADR.                 
      *                                                                         
      *    STOCKAGE REFERENCE COMPTA                                            
       01  WCTA-ZONE.                                                           
           5    WCTA-LIG       PIC X(22) OCCURS 5.                              
      *                                                                         
      *                                                                         
      *    PREPARATION MONTANT HT/TVA/TTC                                       
       01     ZTVA-LIB.                                                         
           02 ZTVA-MTHT     PIC  ZZZ.ZZZ.ZZZ.ZZ9,99.                            
           02 ZTVA-TX       PIC            ZZZZ9,99.                            
           02 ZTVA-MTTVA    PIC ZZZZ.ZZZ.ZZZ.ZZ9,99.                            
      *    H = HAUT DE PAGE ; L = LIGNES ; B = BAS DE PAGE                      
AM     01  H00.                                                                 
AP         02 FILLER     PIC X(7)  VALUE  ' JFG011'.                            
AM         02 FILLER     PIC X(66) VALUE  SPACES.                               
AM         02 H00-CACID  PIC X(7).                                              
AM     01  H01.                                                                 
AM         02 FILLER     PIC X(13) VALUE '  N� ORIGINE '.                       
AM         02 H01-NORIG  PIC X(7).                                              
AM         02 FILLER     PIC X(4)  VALUE ' DU '.                                
AM         02 H01-DORIG  PIC X(10).                                             
PHRI       02 FILLER     PIC X(01) VALUE SPACES.                                
"          02 H01-TVA-INTRA.                                                    
"             03 H01-LIB-INTRA PIC X(09) VALUE SPACES.                          
"             03 H01-TX-INTRA  PIC X(25).                                       
"             03 FILLER        PIC X(01) VALUE SPACES.                          
PHRI       02 FILLER     PIC X(10) VALUE SPACES.                                
       01  L00.                                                                 
           02 FILLER PIC X(30) VALUE 'VENTIL DESIGNATION            '.          
           02 FILLER PIC X(30) VALUE '  QTE.FACT.   PRIX UNITAIRE  E'.          
           02 FILLER PIC X(20) VALUE 'SCPT.    TOTAL H.T. '.                    
       01  L01.                                                                 
           02 FILLER PIC X(30) VALUE '----- ------------------------'.          
           02 FILLER PIC X(30) VALUE '-  -------    ------------- --'.          
           02 FILLER PIC X(20) VALUE '----  ------------- '.                    
       01  L02.                                                                 
           02 L02-CVENT   PIC X(6).                                             
           02 L02-LIBC    PIC X(26).                                            
           02 L02-QTF     PIC Z(03)9,999.                                       
           02 L02-PUF     PIC Z(13)9,99.                                        
           02 L02-ESCPT   PIC Z(03)9,99  BLANK WHEN ZERO.                       
           02 L02-MTHT    PIC Z(11)9,99.                                        
       01  L03.                                                                 
           02 FILLER      PIC X(6) VALUE SPACE.                                 
           02 L03-LIBL    PIC X(70).                                            
       01  B00.                                                                 
           02 FILLER PIC X(50) VALUE ' '.                                       
           02 FILLER PIC X(12)     VALUE 'TOTAL H.T.'.                          
           02 B00-TOTHT  PIC ZZ.ZZZ.ZZZ.ZZ9,99 BLANK WHEN ZERO.                 
           02 B00-RE REDEFINES B00-TOTHT.                                       
              05 FILLER  PIC X(12).                                             
              05 B00-REP PIC X(05).                                             
       01  B01.                                                                 
           02 FILLER PIC X(50) VALUE ' '.                                       
           02 FILLER PIC X(12)     VALUE 'ESCOMPTE  '.                          
           02 B01-MTESC  PIC ZZ.ZZZ.ZZZ.ZZ9,99 BLANK WHEN ZERO.                 
       01  B02.                                                                 
           02 FILLER PIC X(30) VALUE '          NET H.T.    TVA     '.          
           02 FILLER PIC X(15) VALUE '    MONTANT TVA'.                         
       01  B03.                                                                 
           02 B03-LTVA   PIC X(50).                                             
           02 FILLER     PIC X(12) VALUE 'NET   H.T.'.                          
           02 B03-NETHT  PIC ZZ.ZZZ.ZZZ.ZZ9,99 BLANK WHEN ZERO.                 
       01  B04.                                                                 
           02 B04-LTVA   PIC X(50).                                             
           02 FILLER     PIC X(12) VALUE 'TOTAL TVA '.                          
           02 B04-MTTVA  PIC ZZ.ZZZ.ZZZ.ZZ9,99 BLANK WHEN ZERO.                 
       01  B05.                                                                 
           02 B05-LTVA   PIC X(50).                                             
           02 FILLER     PIC X(12) VALUE 'TOTAL TTC '.                          
           02 B05-MTTTC  PIC ZZ.ZZZ.ZZZ.ZZ9,99 BLANK WHEN ZERO.                 
      *                                                                         
       01  B06.                                                                 
           02 FILLER PIC X(30) VALUE 'VALEUR EN NOTRE PRELEVEMENT SU'.          
           02 FILLER PIC X(20) VALUE 'R COMPTE COURANT LE '.                    
           02 B06-DECH   PIC X(10).                                             
      *                                                                         
AM     01  B07.                                                                 
AM         02 FILLER PIC X(30) VALUE 'VIREMENT SUR VOTRE COMPTE COUR'.          
AM         02 FILLER PIC X(25) VALUE 'ANT EN DATE DE VALEUR DU '.               
AM         02 B07-DVALEUR  PIC X(10).                                           
AM         02 FILLER PIC X(15) VALUE  SPACES.                                   
                                                                                
