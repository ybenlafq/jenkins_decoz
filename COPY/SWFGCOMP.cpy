      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    FICHIER DU JOURNAL DE RAPPROCCHEMENT COMPTE COURANT                  
      *       COMPTABLE AVEC LE COMPTE COURANT DE TRESO                         
      *                                                                         
       01       FFGCOMP-DSECT.                                                  
           05   FFGCOMP-IDENT.                                                  
001          10 FFGCOMP-NSOCCOMPT        PIC X(03).                             
004          10 FFGCOMP-MOIS-TRT         PIC X(06).                             
010          10 FFGCOMP-DCOMPTA          PIC X(08).                             
           05   FFGCOMP-SUITE.                                                  
018          10 FFGCOMP-NSOC             PIC X(3).                              
021          10 FFGCOMP-NLIEU            PIC X(3).                              
024          10 FFGCOMP-DVALEUR          PIC X(8).                              
032          10 FFGCOMP-TYPPIECE         PIC X(2).                              
034          10 FFGCOMP-MONTANT          PIC S9(13)V9(2) COMP-3.                
042          10 FFGCOMP-LIBELLE          PIC X(20).                             
062          10 FFGCOMP-TYPE             PIC X(1).                              
063          10 FFGCOMP-FILLER           PIC X(08).                             
->070 *                                                                         
                                                                                
