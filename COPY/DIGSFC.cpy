      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *==> DL1-DBD ==> SEGMENT ************************* AIDA-COBOL *   00000200
      *    DIGDP0      DIGSF   GESTION TERMINAUX PARTICULIERS       *   00000300
      ************************************************ COPY DIGSFC  *   00000400
      *                                                                 00000500
      * DESCRIPTION VARIABLE E/S ------------------------------------   00000600
      *                                                                 00000700
       01  DIGSF PIC X(64).                                             00000800
      *                                                                 00000900
       01  DIGSF-SEG0 REDEFINES DIGSF.                                  00001000
      *                                                                 00001100
      *       CODE ECRAN                                                00001200
           05 DIGFF0-TER-ECR PIC XXXX.                                  00001300
      *                                                                 00001400
      *       IMPRIMANTES ASSOCIEES                                     00001500
           05 DIGFF1-TER-PR1 PIC XXXX.                                  00001600
           05 DIGFF2-TER-PR2 PIC XXXX.                                  00001700
      *                                                                 00001800
           05 DIGFF2-TER-FLR PIC X(52).                                 00002000
      *                                                                 00002001
      *----------------------------------------------------------*      00002010
      * ==> ATTENTION <==                                        *      00002020
      * CETTE DESCRIPTION COBOL-DIGSFC A UNE EQUIVALENCE EN      *      00002030
      * PL1-DIGSF .                                              *      00002040
      * LES MODIFICATIONS DOIVENT ETRE RECIPROQUES               *      00002050
      *----------------------------------------------------------*      00002060
       EJECT                                                            00002100
                                                                                
