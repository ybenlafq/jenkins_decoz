      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      **************************************************************    00010000
      *                                                                 00010100
      * COMMAREA PROGRAMMES DE DETERMINATION EXERCICE IMPUTATION        00020001
      *     ET JOURNAL PAR RAPPORT A GL                                 00030006
      *                                                                 00030007
      **************************************************************    00050000
       01 COMMMFV1.                                                     00060001
          02 COMMMFV1-I.                                                00060002
      *------------------------------ NUMERO ENTITE                     00070001
           03 COMM-MFV1-NENTITE        PIC  X(5).                       00080001
      *------------------------------ DATE DE LA PIECE SSAAMMJJ         00080002
           03 COMM-MFV1-DPIECE         PIC  X(8).                       00080003
      *------------------------------ DATE DU JOUR SSAAMMJJ             00160009
           03 COMM-MFV1-DATJOU         PIC X(8).                         00080  
      *------------------------------ FILLER                            00160009
           03 FILLER                   PIC X(10).                        00080  
          02 COMMMFV1-O.                                                00080004
      *------------------------------ EXERCICE FISCAL                   00110001
           03 COMM-MFV1-NEXERCICE      PIC  X(4).                       00120001
           03 COMM-MFV1-NEXERCI-N     REDEFINES  COMM-MFV1-NEXERCICE    00121009
                          PIC    9(4).                                  00122009
      *------------------------------ PERIODE COMPTABLE                 00130001
           03 COMM-MFV1-NPERIODE       PIC  X(2).                       00140001
           03 COMM-MFV1-NPERIODE-N  REDEFINES  COMM-MFV1-NPERIODE       00140002
                          PIC    9(2).                                  00140003
      *------------------------------ DATE COMPTABLE SSAAMMJJ           00150003
           03 COMM-MFV1-DCOMPT         PIC  X(8).                       00160008
      *------------------------------ CODE TYPE JOURNAL                 00160009
           03 COMM-MFV1-CJRN           PIC  X.                          00160010
              88  COMM-MFV1-CJRNP      VALUE  '1'.                      00160020
              88  COMM-MFV1-CJRNC      VALUE  '2'.                      00160030
              88  COMM-MFV1-CJRNF      VALUE  '3'.                      00160040
                                                                                
