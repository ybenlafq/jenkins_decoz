           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG1000.                                                            
           02  FG10-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FG10-CFRAIS                                                      
               PIC X(0006).                                                     
           02  FG10-NATFACT                                                     
               PIC X(0005).                                                     
           02  FG10-TAUX                                                        
               PIC S9(3)V9(0004) COMP-3.                                        
           02  FG10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG10-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG10-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG10-CFRAIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG10-CFRAIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG10-NATFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG10-NATFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG10-TAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG10-TAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
