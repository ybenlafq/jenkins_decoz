      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      ******************************************************************        
      * DESCRIPTION DES CLEFS UTILISEES PAR LE CHEMIN DIGFA0                    
      ******************************************************************        
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           02  DIGSA-DIGFA0.                                                    
               04  DIGSA-DIGFA0-IGNOMETA PIC X(06).                             
               04  DIGSA-DIGFA0-IGDATEDI PIC X(06).                             
               04  DIGSA-DIGFA0-IGDESTIN PIC X(09).                             
               04  DIGSA-DIGFA0-IGDOCUME PIC X(15).                             
       EJECT                                                                    
                                                                                
EMOD                                                                            
