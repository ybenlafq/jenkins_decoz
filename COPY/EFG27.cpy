      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG27   EFG27                                              00000020
      ***************************************************************** 00000030
       01   EFG27I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * DATE DE VALEUR                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALEURL      COMP PIC S9(4).                            00000170
      *--                                                                       
           02 MDVALEURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDVALEURF      PIC X.                                     00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MDVALEURI      PIC X(10).                                 00000200
      * N� DE PAGE                                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEI    PIC X(2).                                       00000250
      * NOMBRE DE PAGES                                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGEL      COMP PIC S9(4).                            00000270
      *--                                                                       
           02 MTOTPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTPAGEF      PIC X.                                     00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MTOTPAGEI      PIC X(2).                                  00000300
           02 MTABLEAUI OCCURS   14 TIMES .                             00000310
      * N� SOCIETE                                                      00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MNSOCI  PIC X(3).                                       00000360
      * LIBELLE                                                         00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000380
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MLIBELLEI    PIC X(20).                                 00000410
      * SOCIETE EN SIMU O/N                                             00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWALIML      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MWALIML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWALIMF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MWALIMI      PIC X.                                     00000460
      * DATE DU SOLDE                                                   00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSOLDEL     COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MDSOLDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDSOLDEF     PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MDSOLDEI     PIC X(8).                                  00000510
      * SENS DEBIT/CREDIT                                               00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDT-CRL      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MDT-CRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDT-CRF      PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MDT-CRI      PIC X.                                     00000560
      * MONTANT SOLDE                                                   00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTSOLDEL    COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MMTSOLDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMTSOLDEF    PIC X.                                     00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MMTSOLDEI    PIC X(14).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWAJOUTL     COMP PIC S9(4).                            00000620
      *--                                                                       
             03 MWAJOUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWAJOUTF     PIC X.                                     00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MWAJOUTI     PIC X.                                     00000650
      * MESSAGE ERREUR                                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      * CODE TRANSACTION                                                00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCODTRAI  PIC X(4).                                       00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MZONCMDI  PIC X(15).                                      00000790
      * CICS DE TRAVAIL                                                 00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MCICSI    PIC X(5).                                       00000840
      * NETNAME                                                         00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNETNAMI  PIC X(8).                                       00000890
      * CODE TERMINAL                                                   00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(5).                                       00000940
      ***************************************************************** 00000950
      * SDF: EFG27   EFG27                                              00000960
      ***************************************************************** 00000970
       01   EFG27O REDEFINES EFG27I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
      * DATE DU JOUR                                                    00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MDATJOUA  PIC X.                                          00001020
           02 MDATJOUC  PIC X.                                          00001030
           02 MDATJOUP  PIC X.                                          00001040
           02 MDATJOUH  PIC X.                                          00001050
           02 MDATJOUV  PIC X.                                          00001060
           02 MDATJOUO  PIC X(10).                                      00001070
      * HEURE                                                           00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MTIMJOUA  PIC X.                                          00001100
           02 MTIMJOUC  PIC X.                                          00001110
           02 MTIMJOUP  PIC X.                                          00001120
           02 MTIMJOUH  PIC X.                                          00001130
           02 MTIMJOUV  PIC X.                                          00001140
           02 MTIMJOUO  PIC X(5).                                       00001150
      * DATE DE VALEUR                                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDVALEURA      PIC X.                                     00001180
           02 MDVALEURC PIC X.                                          00001190
           02 MDVALEURP PIC X.                                          00001200
           02 MDVALEURH PIC X.                                          00001210
           02 MDVALEURV PIC X.                                          00001220
           02 MDVALEURO      PIC X(10).                                 00001230
      * N� DE PAGE                                                      00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MPAGEA    PIC X.                                          00001260
           02 MPAGEC    PIC X.                                          00001270
           02 MPAGEP    PIC X.                                          00001280
           02 MPAGEH    PIC X.                                          00001290
           02 MPAGEV    PIC X.                                          00001300
           02 MPAGEO    PIC 99.                                         00001310
      * NOMBRE DE PAGES                                                 00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTOTPAGEA      PIC X.                                     00001340
           02 MTOTPAGEC PIC X.                                          00001350
           02 MTOTPAGEP PIC X.                                          00001360
           02 MTOTPAGEH PIC X.                                          00001370
           02 MTOTPAGEV PIC X.                                          00001380
           02 MTOTPAGEO      PIC 99.                                    00001390
           02 MTABLEAUO OCCURS   14 TIMES .                             00001400
      * N� SOCIETE                                                      00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNSOCA  PIC X.                                          00001430
             03 MNSOCC  PIC X.                                          00001440
             03 MNSOCP  PIC X.                                          00001450
             03 MNSOCH  PIC X.                                          00001460
             03 MNSOCV  PIC X.                                          00001470
             03 MNSOCO  PIC X(3).                                       00001480
      * LIBELLE                                                         00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MLIBELLEA    PIC X.                                     00001510
             03 MLIBELLEC    PIC X.                                     00001520
             03 MLIBELLEP    PIC X.                                     00001530
             03 MLIBELLEH    PIC X.                                     00001540
             03 MLIBELLEV    PIC X.                                     00001550
             03 MLIBELLEO    PIC X(20).                                 00001560
      * SOCIETE EN SIMU O/N                                             00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MWALIMA      PIC X.                                     00001590
             03 MWALIMC PIC X.                                          00001600
             03 MWALIMP PIC X.                                          00001610
             03 MWALIMH PIC X.                                          00001620
             03 MWALIMV PIC X.                                          00001630
             03 MWALIMO      PIC X.                                     00001640
      * DATE DU SOLDE                                                   00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MDSOLDEA     PIC X.                                     00001670
             03 MDSOLDEC     PIC X.                                     00001680
             03 MDSOLDEP     PIC X.                                     00001690
             03 MDSOLDEH     PIC X.                                     00001700
             03 MDSOLDEV     PIC X.                                     00001710
             03 MDSOLDEO     PIC X(8).                                  00001720
      * SENS DEBIT/CREDIT                                               00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MDT-CRA      PIC X.                                     00001750
             03 MDT-CRC PIC X.                                          00001760
             03 MDT-CRP PIC X.                                          00001770
             03 MDT-CRH PIC X.                                          00001780
             03 MDT-CRV PIC X.                                          00001790
             03 MDT-CRO      PIC X.                                     00001800
      * MONTANT SOLDE                                                   00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MMTSOLDEA    PIC X.                                     00001830
             03 MMTSOLDEC    PIC X.                                     00001840
             03 MMTSOLDEP    PIC X.                                     00001850
             03 MMTSOLDEH    PIC X.                                     00001860
             03 MMTSOLDEV    PIC X.                                     00001870
             03 MMTSOLDEO    PIC Z(10)9,99.                             00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MWAJOUTA     PIC X.                                     00001900
             03 MWAJOUTC     PIC X.                                     00001910
             03 MWAJOUTP     PIC X.                                     00001920
             03 MWAJOUTH     PIC X.                                     00001930
             03 MWAJOUTV     PIC X.                                     00001940
             03 MWAJOUTO     PIC X.                                     00001950
      * MESSAGE ERREUR                                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLIBERRA  PIC X.                                          00001980
           02 MLIBERRC  PIC X.                                          00001990
           02 MLIBERRP  PIC X.                                          00002000
           02 MLIBERRH  PIC X.                                          00002010
           02 MLIBERRV  PIC X.                                          00002020
           02 MLIBERRO  PIC X(78).                                      00002030
      * CODE TRANSACTION                                                00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MZONCMDA  PIC X.                                          00002130
           02 MZONCMDC  PIC X.                                          00002140
           02 MZONCMDP  PIC X.                                          00002150
           02 MZONCMDH  PIC X.                                          00002160
           02 MZONCMDV  PIC X.                                          00002170
           02 MZONCMDO  PIC X(15).                                      00002180
      * CICS DE TRAVAIL                                                 00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCICSA    PIC X.                                          00002210
           02 MCICSC    PIC X.                                          00002220
           02 MCICSP    PIC X.                                          00002230
           02 MCICSH    PIC X.                                          00002240
           02 MCICSV    PIC X.                                          00002250
           02 MCICSO    PIC X(5).                                       00002260
      * NETNAME                                                         00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNETNAMA  PIC X.                                          00002290
           02 MNETNAMC  PIC X.                                          00002300
           02 MNETNAMP  PIC X.                                          00002310
           02 MNETNAMH  PIC X.                                          00002320
           02 MNETNAMV  PIC X.                                          00002330
           02 MNETNAMO  PIC X(8).                                       00002340
      * CODE TERMINAL                                                   00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MSCREENA  PIC X.                                          00002370
           02 MSCREENC  PIC X.                                          00002380
           02 MSCREENP  PIC X.                                          00002390
           02 MSCREENH  PIC X.                                          00002400
           02 MSCREENV  PIC X.                                          00002410
           02 MSCREENO  PIC X(5).                                       00002420
                                                                                
