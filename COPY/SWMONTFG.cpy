      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                         
      *==> DARTY ******************************************************         
      * ZONES DE TRAVAIL POUR LES TRAITEMENTS DES MONTANTS SAISIS     *         
      * SPECIFIQUE NETTING : JUSQU'A 10 CHIFFRE PARTIE ENTIERE        *         
      *****************************************************************         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
0795       05  W-MONT-USER-FG          PIC X(22).                               
0795       05  FILLER                  REDEFINES W-MONT-USER-FG.                
0795         10  FILLER                OCCURS 22.                               
               15  W-MONT-USER-CAR     PIC X.                                   
               15  W-MONT-USER-CAR9    REDEFINES W-MONT-USER-CAR PIC 9.         
      *                                                                         
0795       05  W-MONT-FILE-FG          PIC S9(11)V9(7) VALUE +0.                
      *                                                                         
           05  W-MONT-NB-ENT-USER      PIC S9(3) COMP-3 VALUE +0.               
           05  W-MONT-NB-DEC-USER      PIC S9(3) COMP-3 VALUE +0.               
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-MONT-NUMERIC         PIC  X             VALUE SPACE.           
           05  W-MONT-POS-DEB         PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-POS-VIR         PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-NB-ENT          PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-NB-DEC          PIC S9(3)   COMP-3 VALUE +0.              
0795       05  W-MONT-LONG-FG         PIC S9(3)   COMP-3 VALUE +22.             
           05  W-MONT-I               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-J               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-POIDS           PIC S9V9(9) COMP-3 VALUE +0.              
           05  W-MONT-SIGNE           PIC X              VALUE ' '.             
           05  W-MONT-FLAG            PIC X              VALUE SPACE.           
0795  *    05  W-MONT-EDIT-FG         PIC -(11)9,9(7)    VALUE ZERO.            
0795  *    05  W-MONT-REEDIT-FG REDEFINES W-MONT-EDIT-FG.                       
      *      10  FILLER               OCCURS 22.                                
      *        15  W-MONT-EDIT-CAR    PIC X.                                    
0795  *    05  W-MONT-TAMPON-FG.                                                
0795  *      10  W-MONT-TAMPON1-FG    PIC X            VALUE SPACE.             
0795  *      10  W-MONT-TAMPON2-FG    PIC X(21)        VALUE SPACE.             
      *                                                                         
             EJECT                                                              
                                                                                
