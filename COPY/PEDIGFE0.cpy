      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                         
      ************************************************** EXEC DLI ******        
      * PREPARATION DE L'ACCES AU SEGMENT DIGSEC PAR LE PATH DIGFE0             
      ******************************************************************        
      *                                                                         
       CLEF-DIGFE0             SECTION.                                         
      *                                                                         
      ************************************* PREPARATION CLEF-1                  
      *                                                                         
           MOVE 'DIGSE'  TO SEG-NAME-1.                                         
           MOVE 'DIGFE0'  TO KEY-NAME-1.                                        
           MOVE '64'  TO SEG-LONG-1.                                            
           MOVE '9'  TO KEY-LONG-1.                                             
           MOVE DIGSE-DIGFE0                                                    
                         TO KEY-VAL-1.                                          
      *                                                                         
           MOVE 'DIGFE0'  TO PATH-NAME.                                         
      *                                                                         
           IF PSB-NOT-OK THEN                                                   
                         PERFORM SCHEDULING-EXEC                                
           END-IF.                                                              
      *                                                                         
       FIN-CLEF-DIGFE0.    EXIT.                                                
                EJECT                                                           
                                                                                
EMOD                                                                            
