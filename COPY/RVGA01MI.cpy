           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGADR ADRESSAGE DE L OPERATEUR=>GA10   *        
      *----------------------------------------------------------------*        
       01  RVGA01MI.                                                            
           05  FGADR-CTABLEG2    PIC X(15).                                     
           05  FGADR-CTABLEG2-REDEF REDEFINES FGADR-CTABLEG2.                   
               10  FGADR-CACID           PIC X(07).                             
           05  FGADR-WTABLEG     PIC X(80).                                     
           05  FGADR-WTABLEG-REDEF  REDEFINES FGADR-WTABLEG.                    
               10  FGADR-WACTIF          PIC X(01).                             
               10  FGADR-NSOCLIEU        PIC X(06).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGADR-NSOCLIEU-N     REDEFINES FGADR-NSOCLIEU                
                                         PIC 9(06).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGADR-WGROUPE         PIC X(01).                             
               10  FGADR-SERVICE         PIC X(05).                             
               10  FGADR-AUTOR           PIC X(03).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGADR-AUTOR-N        REDEFINES FGADR-AUTOR                   
                                         PIC 9(03).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01MI-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGADR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGADR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGADR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGADR-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
