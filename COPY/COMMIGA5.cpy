      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *==> DARTY ******************************************************         
      *          * IMPRESSION IMMEDIATE ETAT / CICS                   *         
      *          * COMMAREA PGM CICS TIGA5                            *         
      ****************************************************** COMMIGA5 *         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-IGA5-LONG-COMMAREA PIC S9(4) COMP VALUE +134.                   
      *--                                                                       
       01  COMM-IGA5-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +134.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA-IGA5.                                                     
      *                                                                         
      *==>CLE ACCES ETAT                                                        
          02 COMM-IGA5-ETA                  PIC X(06).                          
          02 COMM-IGA5-DAT                  PIC X(06).                          
          02 COMM-IGA5-DST                  PIC X(09).                          
          02 COMM-IGA5-DOC                  PIC X(15).                          
      *                                                                         
      *==>DATE DU JOUR AAMMJJ                                                   
          02 COMM-IGA5-AMJ                  PIC X(06).                          
      *                                                                         
      *==>INFORMATIONS DL1                                                      
          02 COMM-IGA5-PCB                  PIC 9.                              
          02 COMM-IGA5-SCH                  PIC X.                              
      *                                                                         
      *==>IMPRIMANTES RECHERCHEES                                               
          02 COMM-IGA5-PR1                  PIC X(04).                          
          02 COMM-IGA5-PR2                  PIC X(04).                          
      *                                                                         
      *==>IMPRIMANTE FORCEE OU RETENUE                                          
          02 COMM-IGA5-PRT                  PIC X(04).                          
          02 COMM-IGA5-TYP                  PIC X(01).                          
      *                                                                         
      *==>TRAITEMENT DES ERREURS                                                
          02 COMM-IGA5-ERR                  PIC X(04).                          
          02 COMM-IGA5-MES                  PIC X(70).                          
      *                                                                         
           EJECT                                                                
                                                                                
