           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGRMP PARAMETRAGE REGLEMENT/REMUN�     *        
      *----------------------------------------------------------------*        
       01  RVGA01O0.                                                            
           05  FGRMP-CTABLEG2    PIC X(15).                                     
           05  FGRMP-CTABLEG2-REDEF REDEFINES FGRMP-CTABLEG2.                   
               10  FGRMP-NSOCTR          PIC X(03).                             
               10  FGRMP-NETABTR         PIC X(03).                             
           05  FGRMP-WTABLEG     PIC X(80).                                     
           05  FGRMP-WTABLEG-REDEF  REDEFINES FGRMP-WTABLEG.                    
               10  FGRMP-NATREMUN        PIC X(05).                             
               10  FGRMP-CTAUXTVA        PIC X(05).                             
               10  FGRMP-NBJOURS         PIC S9(02)       COMP-3.               
               10  FGRMP-TYPEDOC         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01O0-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGRMP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGRMP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGRMP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGRMP-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
