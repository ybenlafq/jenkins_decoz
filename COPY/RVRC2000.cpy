           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRC2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRC2000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRC2000.                                                            
           02  RC20-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RC20-NLIEU                                                       
               PIC X(0003).                                                     
           02  RC20-NOMFIC                                                      
               PIC X(0008).                                                     
           02  RC20-DATEFIC                                                     
               PIC X(0008).                                                     
           02  RC20-NSEQ                                                        
               PIC S9(13) COMP-3.                                               
           02  RC20-ENRFIC1                                                     
               PIC X(0128).                                                     
           02  RC20-ENRFIC2                                                     
               PIC X(0128).                                                     
           02  RC20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRC2000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRC2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-NOMFIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-NOMFIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-DATEFIC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-DATEFIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-ENRFIC1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-ENRFIC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-ENRFIC2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-ENRFIC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
