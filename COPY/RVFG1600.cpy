           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG1600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG1600.                                                            
           02  FG16-NSOC                                                        
               PIC X(0003).                                                     
           02  FG16-NLIEU                                                       
               PIC X(0003).                                                     
           02  FG16-SECORIG                                                     
               PIC X(0006).                                                     
           02  FG16-SECTION                                                     
               PIC X(0006).                                                     
           02  FG16-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG1600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG1600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG16-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG16-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG16-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG16-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG16-SECORIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG16-SECORIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG16-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG16-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG16-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG16-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
