      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG12   EFG12                                              00000020
      ***************************************************************** 00000030
       01   EFG12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(30).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPAGEI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MPAGEMAXI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSOCCI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSERVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSERVF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSERVI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCCF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLSOCCI   PIC X(30).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCTYPEI   PIC X(2).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPEL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTYPEF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLTYPEI   PIC X(30).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSEL1L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLSEL1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSEL1F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLSEL1I   PIC X(9).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNATL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNATF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLNATI    PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSEL2L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLSEL2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSEL2F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLSEL2I   PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLSOCI    PIC X(20).                                      00000730
           02 MTABFACI OCCURS   10 TIMES .                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECORIGL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MSECORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECORIGF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSECORIGI    PIC X(6).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSDESTL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MSDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSDESTF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSDESTI      PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDESTL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MLDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLDESTF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLDESTI      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECDESTL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MSECDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECDESTF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSECDESTI    PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNATUREL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MNATUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNATUREF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNATUREI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVENTL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MVENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVENTF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MVENTI  PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDEBL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MDDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDDEBF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDDEBI  PIC X(10).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MDFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDFINF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDFINI  PIC X(10).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTHTL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MMONTHTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMONTHTF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MMONTHTI     PIC X(13).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTVAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MCTVAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCTVAF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MCTVAI  PIC X.                                          00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFREQL  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MFREQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFREQF  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MFREQI  PIC X(2).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MZONCMDI  PIC X(15).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(58).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNETNAMI  PIC X(8).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MSCREENI  PIC X(4).                                       00001420
      ***************************************************************** 00001430
      * SDF: EFG12   EFG12                                              00001440
      ***************************************************************** 00001450
       01   EFG12O REDEFINES EFG12I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MFONCA    PIC X.                                          00001630
           02 MFONCC    PIC X.                                          00001640
           02 MFONCP    PIC X.                                          00001650
           02 MFONCH    PIC X.                                          00001660
           02 MFONCV    PIC X.                                          00001670
           02 MFONCO    PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNSOCA    PIC X.                                          00001700
           02 MNSOCC    PIC X.                                          00001710
           02 MNSOCP    PIC X.                                          00001720
           02 MNSOCH    PIC X.                                          00001730
           02 MNSOCV    PIC X.                                          00001740
           02 MNSOCO    PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNLIEUA   PIC X.                                          00001770
           02 MNLIEUC   PIC X.                                          00001780
           02 MNLIEUP   PIC X.                                          00001790
           02 MNLIEUH   PIC X.                                          00001800
           02 MNLIEUV   PIC X.                                          00001810
           02 MNLIEUO   PIC X(3).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLLIEUA   PIC X.                                          00001840
           02 MLLIEUC   PIC X.                                          00001850
           02 MLLIEUP   PIC X.                                          00001860
           02 MLLIEUH   PIC X.                                          00001870
           02 MLLIEUV   PIC X.                                          00001880
           02 MLLIEUO   PIC X(30).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MPAGEA    PIC X.                                          00001910
           02 MPAGEC    PIC X.                                          00001920
           02 MPAGEP    PIC X.                                          00001930
           02 MPAGEH    PIC X.                                          00001940
           02 MPAGEV    PIC X.                                          00001950
           02 MPAGEO    PIC 999.                                        00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MPAGEMAXA      PIC X.                                     00001980
           02 MPAGEMAXC PIC X.                                          00001990
           02 MPAGEMAXP PIC X.                                          00002000
           02 MPAGEMAXH PIC X.                                          00002010
           02 MPAGEMAXV PIC X.                                          00002020
           02 MPAGEMAXO      PIC 999.                                   00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSOCCA    PIC X.                                          00002050
           02 MSOCCC    PIC X.                                          00002060
           02 MSOCCP    PIC X.                                          00002070
           02 MSOCCH    PIC X.                                          00002080
           02 MSOCCV    PIC X.                                          00002090
           02 MSOCCO    PIC X(3).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MSERVA    PIC X.                                          00002120
           02 MSERVC    PIC X.                                          00002130
           02 MSERVP    PIC X.                                          00002140
           02 MSERVH    PIC X.                                          00002150
           02 MSERVV    PIC X.                                          00002160
           02 MSERVO    PIC X(5).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLSOCCA   PIC X.                                          00002190
           02 MLSOCCC   PIC X.                                          00002200
           02 MLSOCCP   PIC X.                                          00002210
           02 MLSOCCH   PIC X.                                          00002220
           02 MLSOCCV   PIC X.                                          00002230
           02 MLSOCCO   PIC X(30).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCTYPEA   PIC X.                                          00002260
           02 MCTYPEC   PIC X.                                          00002270
           02 MCTYPEP   PIC X.                                          00002280
           02 MCTYPEH   PIC X.                                          00002290
           02 MCTYPEV   PIC X.                                          00002300
           02 MCTYPEO   PIC X(2).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLTYPEA   PIC X.                                          00002330
           02 MLTYPEC   PIC X.                                          00002340
           02 MLTYPEP   PIC X.                                          00002350
           02 MLTYPEH   PIC X.                                          00002360
           02 MLTYPEV   PIC X.                                          00002370
           02 MLTYPEO   PIC X(30).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLSEL1A   PIC X.                                          00002400
           02 MLSEL1C   PIC X.                                          00002410
           02 MLSEL1P   PIC X.                                          00002420
           02 MLSEL1H   PIC X.                                          00002430
           02 MLSEL1V   PIC X.                                          00002440
           02 MLSEL1O   PIC X(9).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLNATA    PIC X.                                          00002470
           02 MLNATC    PIC X.                                          00002480
           02 MLNATP    PIC X.                                          00002490
           02 MLNATH    PIC X.                                          00002500
           02 MLNATV    PIC X.                                          00002510
           02 MLNATO    PIC X(20).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLSEL2A   PIC X.                                          00002540
           02 MLSEL2C   PIC X.                                          00002550
           02 MLSEL2P   PIC X.                                          00002560
           02 MLSEL2H   PIC X.                                          00002570
           02 MLSEL2V   PIC X.                                          00002580
           02 MLSEL2O   PIC X(20).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MLSOCA    PIC X.                                          00002610
           02 MLSOCC    PIC X.                                          00002620
           02 MLSOCP    PIC X.                                          00002630
           02 MLSOCH    PIC X.                                          00002640
           02 MLSOCV    PIC X.                                          00002650
           02 MLSOCO    PIC X(20).                                      00002660
           02 MTABFACO OCCURS   10 TIMES .                              00002670
             03 FILLER       PIC X(2).                                  00002680
             03 MSECORIGA    PIC X.                                     00002690
             03 MSECORIGC    PIC X.                                     00002700
             03 MSECORIGP    PIC X.                                     00002710
             03 MSECORIGH    PIC X.                                     00002720
             03 MSECORIGV    PIC X.                                     00002730
             03 MSECORIGO    PIC X(6).                                  00002740
             03 FILLER       PIC X(2).                                  00002750
             03 MSDESTA      PIC X.                                     00002760
             03 MSDESTC PIC X.                                          00002770
             03 MSDESTP PIC X.                                          00002780
             03 MSDESTH PIC X.                                          00002790
             03 MSDESTV PIC X.                                          00002800
             03 MSDESTO      PIC X(3).                                  00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MLDESTA      PIC X.                                     00002830
             03 MLDESTC PIC X.                                          00002840
             03 MLDESTP PIC X.                                          00002850
             03 MLDESTH PIC X.                                          00002860
             03 MLDESTV PIC X.                                          00002870
             03 MLDESTO      PIC X(3).                                  00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MSECDESTA    PIC X.                                     00002900
             03 MSECDESTC    PIC X.                                     00002910
             03 MSECDESTP    PIC X.                                     00002920
             03 MSECDESTH    PIC X.                                     00002930
             03 MSECDESTV    PIC X.                                     00002940
             03 MSECDESTO    PIC X(6).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MNATUREA     PIC X.                                     00002970
             03 MNATUREC     PIC X.                                     00002980
             03 MNATUREP     PIC X.                                     00002990
             03 MNATUREH     PIC X.                                     00003000
             03 MNATUREV     PIC X.                                     00003010
             03 MNATUREO     PIC X(5).                                  00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MVENTA  PIC X.                                          00003040
             03 MVENTC  PIC X.                                          00003050
             03 MVENTP  PIC X.                                          00003060
             03 MVENTH  PIC X.                                          00003070
             03 MVENTV  PIC X.                                          00003080
             03 MVENTO  PIC X(5).                                       00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MDDEBA  PIC X.                                          00003110
             03 MDDEBC  PIC X.                                          00003120
             03 MDDEBP  PIC X.                                          00003130
             03 MDDEBH  PIC X.                                          00003140
             03 MDDEBV  PIC X.                                          00003150
             03 MDDEBO  PIC X(10).                                      00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MDFINA  PIC X.                                          00003180
             03 MDFINC  PIC X.                                          00003190
             03 MDFINP  PIC X.                                          00003200
             03 MDFINH  PIC X.                                          00003210
             03 MDFINV  PIC X.                                          00003220
             03 MDFINO  PIC X(10).                                      00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MMONTHTA     PIC X.                                     00003250
             03 MMONTHTC     PIC X.                                     00003260
             03 MMONTHTP     PIC X.                                     00003270
             03 MMONTHTH     PIC X.                                     00003280
             03 MMONTHTV     PIC X.                                     00003290
             03 MMONTHTO     PIC -(9)9,99.                              00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MCTVAA  PIC X.                                          00003320
             03 MCTVAC  PIC X.                                          00003330
             03 MCTVAP  PIC X.                                          00003340
             03 MCTVAH  PIC X.                                          00003350
             03 MCTVAV  PIC X.                                          00003360
             03 MCTVAO  PIC X.                                          00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MFREQA  PIC X.                                          00003390
             03 MFREQC  PIC X.                                          00003400
             03 MFREQP  PIC X.                                          00003410
             03 MFREQH  PIC X.                                          00003420
             03 MFREQV  PIC X.                                          00003430
             03 MFREQO  PIC X(2).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MZONCMDA  PIC X.                                          00003460
           02 MZONCMDC  PIC X.                                          00003470
           02 MZONCMDP  PIC X.                                          00003480
           02 MZONCMDH  PIC X.                                          00003490
           02 MZONCMDV  PIC X.                                          00003500
           02 MZONCMDO  PIC X(15).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLIBERRA  PIC X.                                          00003530
           02 MLIBERRC  PIC X.                                          00003540
           02 MLIBERRP  PIC X.                                          00003550
           02 MLIBERRH  PIC X.                                          00003560
           02 MLIBERRV  PIC X.                                          00003570
           02 MLIBERRO  PIC X(58).                                      00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCICSA    PIC X.                                          00003670
           02 MCICSC    PIC X.                                          00003680
           02 MCICSP    PIC X.                                          00003690
           02 MCICSH    PIC X.                                          00003700
           02 MCICSV    PIC X.                                          00003710
           02 MCICSO    PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSCREENA  PIC X.                                          00003810
           02 MSCREENC  PIC X.                                          00003820
           02 MSCREENP  PIC X.                                          00003830
           02 MSCREENH  PIC X.                                          00003840
           02 MSCREENV  PIC X.                                          00003850
           02 MSCREENO  PIC X(4).                                       00003860
                                                                                
