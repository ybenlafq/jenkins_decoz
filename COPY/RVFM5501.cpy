           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFM5500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVFM5501                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5501.                                                            
           02  FM55-CODE                                                        
               PIC X(0003).                                                     
           02  FM55-LIBELLE                                                     
               PIC X(0030).                                                     
           02  FM55-CODEGEO                                                     
               PIC X(0001).                                                     
           02  FM55-CODEBDF                                                     
               PIC X(0002).                                                     
           02  FM55-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVFM5501                                    
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-CODE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-CODE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-CODEGEO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-CODEGEO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-CODEBDF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-CODEBDF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
