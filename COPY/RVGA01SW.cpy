           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGCLO SOC. GESTION EN FIN D ACTIVITE   *        
      *----------------------------------------------------------------*        
       01  RVGA01SW.                                                            
           05  FGCLO-CTABLEG2    PIC X(15).                                     
           05  FGCLO-CTABLEG2-REDEF REDEFINES FGCLO-CTABLEG2.                   
               10  FGCLO-NSOC            PIC X(03).                             
               10  FGCLO-NLIEU           PIC X(03).                             
           05  FGCLO-WTABLEG     PIC X(80).                                     
           05  FGCLO-WTABLEG-REDEF  REDEFINES FGCLO-WTABLEG.                    
               10  FGCLO-WACTIF          PIC X(01).                             
               10  FGCLO-DFIN            PIC X(08).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGCLO-DFIN-N         REDEFINES FGCLO-DFIN                    
                                         PIC 9(08).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGCLO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGCLO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGCLO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGCLO-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
