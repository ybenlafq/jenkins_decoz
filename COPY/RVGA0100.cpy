      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
           EJECT                                                                
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      **********************************************************                
      *   COPY DE LA TABLE RVGA0100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0100                         
      **********************************************************                
       01  RVGA0100.                                                            
           02  GA01-CTABLEG1                                                    
               PIC X(0005).                                                     
           02  GA01-CTABLEG2                                                    
               PIC X(0015).                                                     
           02  GA01-WTABLEG                                                     
               PIC X(0080).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA0100                                  
      **********************************************************                
       01  RVGA0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA01-CTABLEG1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA01-CTABLEG1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA01-CTABLEG2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA01-CTABLEG2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA01-WTABLEG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GA01-WTABLEG-F                                                   
               PIC S9(4) COMP-5.                                                
                                                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
 
      *}                                                                        
