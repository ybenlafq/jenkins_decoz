           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGTYP TYPES DE FACTURATION             *        
      *----------------------------------------------------------------*        
       01  RVGA01L0.                                                            
           05  FGTYP-CTABLEG2    PIC X(15).                                     
           05  FGTYP-CTABLEG2-REDEF REDEFINES FGTYP-CTABLEG2.                   
               10  FGTYP-CTYPE           PIC X(02).                             
           05  FGTYP-WTABLEG     PIC X(80).                                     
           05  FGTYP-WTABLEG-REDEF  REDEFINES FGTYP-WTABLEG.                    
               10  FGTYP-WACTIF          PIC X(01).                             
               10  FGTYP-LTYPE           PIC X(25).                             
               10  FGTYP-WAVOIR          PIC X(01).                             
               10  FGTYP-SENSDC          PIC X(01).                             
               10  FGTYP-FLAGS           PIC X(12).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01L0-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
 
      *}                                                                        
