           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFM9300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM9300.                                                            
           02  FM93-CRITLIEU1                                                   
               PIC X(0003).                                                     
           02  FM93-CRITLIEU2                                                   
               PIC X(0003).                                                     
           02  FM93-LIBLIEU                                                     
               PIC X(0030).                                                     
           02  FM93-LIEUPCA                                                     
               PIC X(0003).                                                     
           02  FM93-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM93-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM9300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM9300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM93-CRITLIEU1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM93-CRITLIEU1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM93-CRITLIEU2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM93-CRITLIEU2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM93-LIBLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM93-LIBLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM93-LIEUPCA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM93-LIEUPCA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM93-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM93-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM93-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  FM93-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
 
      *}                                                                        
