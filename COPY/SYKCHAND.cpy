      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *****************************************************************         
      * GESTION DES CONDITIONS D'ERREURS CICS NON PREVUES             *         
      ****************************************************** SYKCHAND *         
      *                                                                         
       INIT-HANDLE SECTION.                                                     
      *                                                                         
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS HANDLE ABEND                                                   
                        LABEL (ABANDON-ABEND)                                   
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�00007   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0                                   
      *dfhei*      SERVICE LABEL                                                
      *dfhei*} decommente EXEC                                                  
      *{Post-translation Comment-Declar-var
      *      GO TO ABANDON-ABEND DEPENDING ON DFHEIGDI.                          
      *}
      *                                                                         
       FIN-INIT-HANDLE.                                                         
         EXIT.                                                                  
       EJECT                                                                    
                                                                                
                                                                                
