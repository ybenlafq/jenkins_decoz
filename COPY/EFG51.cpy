      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG10   EFG10                                              00000020
      ***************************************************************** 00000030
       01   EFG51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCEL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCCEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCCEF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCCEI  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCEL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLSOCCEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLSOCCEF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLSOCCEI  PIC X(25).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCEL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSOCEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCEF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSOCEI   PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUEL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNLIEUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUEF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNLIEUEI  PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUEL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLLIEUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUEF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLLIEUEI  PIC X(25).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCRL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MNSOCCRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCCRF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MNSOCCRI  PIC X(3).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCRL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLSOCCRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLSOCCRF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLSOCCRI  PIC X(25).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNSOCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCRF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNSOCRI   PIC X(3).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEURL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNLIEURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEURF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNLIEURI  PIC X(3).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEURL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLLIEURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEURF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLLIEURI  PIC X(25).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCTYPEI   PIC X(2).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPEL   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MLTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTYPEF   PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLTYPEI   PIC X(30).                                      00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATUREL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNATUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNATUREF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNATUREI  PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNATUREL      COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MLNATUREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLNATUREF      PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLNATUREI      PIC X(30).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMFACTL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MNUMFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMFACTF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNUMFACTI      PIC X(7).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPIECEL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MDPIECEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPIECEF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MDPIECEI  PIC X(10).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MDANNULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDANNULF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MDANNULI  PIC X(10).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMVTL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MDMVTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMVTF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MDMVTI    PIC X(10).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMESSAGEL      COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MMESSAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMESSAGEF      PIC X.                                     00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MMESSAGEI      PIC X(31).                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCOMPTAL      COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MDCOMPTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCOMPTAF      PIC X.                                     00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MDCOMPTAI      PIC X(10).                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MDECHL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDECHF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MDECHI    PIC X(10).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDREGLL   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MDREGLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDREGLF   PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MDREGLI   PIC X(10).                                      00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALEURL      COMP PIC S9(4).                            00001040
      *--                                                                       
           02 MDVALEURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDVALEURF      PIC X.                                     00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MDVALEURI      PIC X(10).                                 00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONFIRML      COMP PIC S9(4).                            00001080
      *--                                                                       
           02 MCONFIRML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCONFIRMF      PIC X.                                     00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MCONFIRMI      PIC X(3).                                  00001110
      * MESSAGE ERREUR                                                  00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MLIBERRI  PIC X(78).                                      00001160
      * CODE TRANSACTION                                                00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCODTRAI  PIC X(4).                                       00001210
      * CICS DE TRAVAIL                                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      * NETNAME                                                         00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MNETNAMI  PIC X(8).                                       00001310
      * CODE TERMINAL                                                   00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MSCREENI  PIC X(5).                                       00001360
      ***************************************************************** 00001370
      * SDF: EFG10   EFG10                                              00001380
      ***************************************************************** 00001390
       01   EFG51O REDEFINES EFG51I.                                    00001400
           02 FILLER    PIC X(12).                                      00001410
      * DATE DU JOUR                                                    00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDATJOUA  PIC X.                                          00001440
           02 MDATJOUC  PIC X.                                          00001450
           02 MDATJOUP  PIC X.                                          00001460
           02 MDATJOUH  PIC X.                                          00001470
           02 MDATJOUV  PIC X.                                          00001480
           02 MDATJOUO  PIC X(10).                                      00001490
      * HEURE                                                           00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MTIMJOUA  PIC X.                                          00001520
           02 MTIMJOUC  PIC X.                                          00001530
           02 MTIMJOUP  PIC X.                                          00001540
           02 MTIMJOUH  PIC X.                                          00001550
           02 MTIMJOUV  PIC X.                                          00001560
           02 MTIMJOUO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNSOCCEA  PIC X.                                          00001590
           02 MNSOCCEC  PIC X.                                          00001600
           02 MNSOCCEP  PIC X.                                          00001610
           02 MNSOCCEH  PIC X.                                          00001620
           02 MNSOCCEV  PIC X.                                          00001630
           02 MNSOCCEO  PIC X(3).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLSOCCEA  PIC X.                                          00001660
           02 MLSOCCEC  PIC X.                                          00001670
           02 MLSOCCEP  PIC X.                                          00001680
           02 MLSOCCEH  PIC X.                                          00001690
           02 MLSOCCEV  PIC X.                                          00001700
           02 MLSOCCEO  PIC X(25).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNSOCEA   PIC X.                                          00001730
           02 MNSOCEC   PIC X.                                          00001740
           02 MNSOCEP   PIC X.                                          00001750
           02 MNSOCEH   PIC X.                                          00001760
           02 MNSOCEV   PIC X.                                          00001770
           02 MNSOCEO   PIC X(3).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNLIEUEA  PIC X.                                          00001800
           02 MNLIEUEC  PIC X.                                          00001810
           02 MNLIEUEP  PIC X.                                          00001820
           02 MNLIEUEH  PIC X.                                          00001830
           02 MNLIEUEV  PIC X.                                          00001840
           02 MNLIEUEO  PIC X(3).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLLIEUEA  PIC X.                                          00001870
           02 MLLIEUEC  PIC X.                                          00001880
           02 MLLIEUEP  PIC X.                                          00001890
           02 MLLIEUEH  PIC X.                                          00001900
           02 MLLIEUEV  PIC X.                                          00001910
           02 MLLIEUEO  PIC X(25).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNSOCCRA  PIC X.                                          00001940
           02 MNSOCCRC  PIC X.                                          00001950
           02 MNSOCCRP  PIC X.                                          00001960
           02 MNSOCCRH  PIC X.                                          00001970
           02 MNSOCCRV  PIC X.                                          00001980
           02 MNSOCCRO  PIC X(3).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLSOCCRA  PIC X.                                          00002010
           02 MLSOCCRC  PIC X.                                          00002020
           02 MLSOCCRP  PIC X.                                          00002030
           02 MLSOCCRH  PIC X.                                          00002040
           02 MLSOCCRV  PIC X.                                          00002050
           02 MLSOCCRO  PIC X(25).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNSOCRA   PIC X.                                          00002080
           02 MNSOCRC   PIC X.                                          00002090
           02 MNSOCRP   PIC X.                                          00002100
           02 MNSOCRH   PIC X.                                          00002110
           02 MNSOCRV   PIC X.                                          00002120
           02 MNSOCRO   PIC X(3).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNLIEURA  PIC X.                                          00002150
           02 MNLIEURC  PIC X.                                          00002160
           02 MNLIEURP  PIC X.                                          00002170
           02 MNLIEURH  PIC X.                                          00002180
           02 MNLIEURV  PIC X.                                          00002190
           02 MNLIEURO  PIC X(3).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MLLIEURA  PIC X.                                          00002220
           02 MLLIEURC  PIC X.                                          00002230
           02 MLLIEURP  PIC X.                                          00002240
           02 MLLIEURH  PIC X.                                          00002250
           02 MLLIEURV  PIC X.                                          00002260
           02 MLLIEURO  PIC X(25).                                      00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCTYPEA   PIC X.                                          00002290
           02 MCTYPEC   PIC X.                                          00002300
           02 MCTYPEP   PIC X.                                          00002310
           02 MCTYPEH   PIC X.                                          00002320
           02 MCTYPEV   PIC X.                                          00002330
           02 MCTYPEO   PIC X(2).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLTYPEA   PIC X.                                          00002360
           02 MLTYPEC   PIC X.                                          00002370
           02 MLTYPEP   PIC X.                                          00002380
           02 MLTYPEH   PIC X.                                          00002390
           02 MLTYPEV   PIC X.                                          00002400
           02 MLTYPEO   PIC X(30).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MNATUREA  PIC X.                                          00002430
           02 MNATUREC  PIC X.                                          00002440
           02 MNATUREP  PIC X.                                          00002450
           02 MNATUREH  PIC X.                                          00002460
           02 MNATUREV  PIC X.                                          00002470
           02 MNATUREO  PIC X(5).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MLNATUREA      PIC X.                                     00002500
           02 MLNATUREC PIC X.                                          00002510
           02 MLNATUREP PIC X.                                          00002520
           02 MLNATUREH PIC X.                                          00002530
           02 MLNATUREV PIC X.                                          00002540
           02 MLNATUREO      PIC X(30).                                 00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNUMFACTA      PIC X.                                     00002570
           02 MNUMFACTC PIC X.                                          00002580
           02 MNUMFACTP PIC X.                                          00002590
           02 MNUMFACTH PIC X.                                          00002600
           02 MNUMFACTV PIC X.                                          00002610
           02 MNUMFACTO      PIC X(7).                                  00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MDPIECEA  PIC X.                                          00002640
           02 MDPIECEC  PIC X.                                          00002650
           02 MDPIECEP  PIC X.                                          00002660
           02 MDPIECEH  PIC X.                                          00002670
           02 MDPIECEV  PIC X.                                          00002680
           02 MDPIECEO  PIC X(10).                                      00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MDANNULA  PIC X.                                          00002710
           02 MDANNULC  PIC X.                                          00002720
           02 MDANNULP  PIC X.                                          00002730
           02 MDANNULH  PIC X.                                          00002740
           02 MDANNULV  PIC X.                                          00002750
           02 MDANNULO  PIC X(10).                                      00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MDMVTA    PIC X.                                          00002780
           02 MDMVTC    PIC X.                                          00002790
           02 MDMVTP    PIC X.                                          00002800
           02 MDMVTH    PIC X.                                          00002810
           02 MDMVTV    PIC X.                                          00002820
           02 MDMVTO    PIC X(10).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MMESSAGEA      PIC X.                                     00002850
           02 MMESSAGEC PIC X.                                          00002860
           02 MMESSAGEP PIC X.                                          00002870
           02 MMESSAGEH PIC X.                                          00002880
           02 MMESSAGEV PIC X.                                          00002890
           02 MMESSAGEO      PIC X(31).                                 00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MDCOMPTAA      PIC X.                                     00002920
           02 MDCOMPTAC PIC X.                                          00002930
           02 MDCOMPTAP PIC X.                                          00002940
           02 MDCOMPTAH PIC X.                                          00002950
           02 MDCOMPTAV PIC X.                                          00002960
           02 MDCOMPTAO      PIC X(10).                                 00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MDECHA    PIC X.                                          00002990
           02 MDECHC    PIC X.                                          00003000
           02 MDECHP    PIC X.                                          00003010
           02 MDECHH    PIC X.                                          00003020
           02 MDECHV    PIC X.                                          00003030
           02 MDECHO    PIC X(10).                                      00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MDREGLA   PIC X.                                          00003060
           02 MDREGLC   PIC X.                                          00003070
           02 MDREGLP   PIC X.                                          00003080
           02 MDREGLH   PIC X.                                          00003090
           02 MDREGLV   PIC X.                                          00003100
           02 MDREGLO   PIC X(10).                                      00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MDVALEURA      PIC X.                                     00003130
           02 MDVALEURC PIC X.                                          00003140
           02 MDVALEURP PIC X.                                          00003150
           02 MDVALEURH PIC X.                                          00003160
           02 MDVALEURV PIC X.                                          00003170
           02 MDVALEURO      PIC X(10).                                 00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MCONFIRMA      PIC X.                                     00003200
           02 MCONFIRMC PIC X.                                          00003210
           02 MCONFIRMP PIC X.                                          00003220
           02 MCONFIRMH PIC X.                                          00003230
           02 MCONFIRMV PIC X.                                          00003240
           02 MCONFIRMO      PIC X(3).                                  00003250
      * MESSAGE ERREUR                                                  00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MLIBERRA  PIC X.                                          00003280
           02 MLIBERRC  PIC X.                                          00003290
           02 MLIBERRP  PIC X.                                          00003300
           02 MLIBERRH  PIC X.                                          00003310
           02 MLIBERRV  PIC X.                                          00003320
           02 MLIBERRO  PIC X(78).                                      00003330
      * CODE TRANSACTION                                                00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MCODTRAA  PIC X.                                          00003360
           02 MCODTRAC  PIC X.                                          00003370
           02 MCODTRAP  PIC X.                                          00003380
           02 MCODTRAH  PIC X.                                          00003390
           02 MCODTRAV  PIC X.                                          00003400
           02 MCODTRAO  PIC X(4).                                       00003410
      * CICS DE TRAVAIL                                                 00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MCICSA    PIC X.                                          00003440
           02 MCICSC    PIC X.                                          00003450
           02 MCICSP    PIC X.                                          00003460
           02 MCICSH    PIC X.                                          00003470
           02 MCICSV    PIC X.                                          00003480
           02 MCICSO    PIC X(5).                                       00003490
      * NETNAME                                                         00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
      * CODE TERMINAL                                                   00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MSCREENA  PIC X.                                          00003600
           02 MSCREENC  PIC X.                                          00003610
           02 MSCREENP  PIC X.                                          00003620
           02 MSCREENH  PIC X.                                          00003630
           02 MSCREENV  PIC X.                                          00003640
           02 MSCREENO  PIC X(5).                                       00003650
                                                                                
