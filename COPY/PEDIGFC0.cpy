      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                         
      ************************************************** EXEC DLI ******        
      * PREPARATION DE L'ACCES AU SEGMENT DIGSCC PAR LE PATH DIGFC0             
      ******************************************************************        
      *                                                                         
       CLEF-DIGFC0             SECTION.                                         
      *                                                                         
      ************************************* PREPARATION CLEF-1                  
      *                                                                         
           MOVE 'DIGSC'  TO SEG-NAME-1.                                         
           MOVE 'DIGFC0'  TO KEY-NAME-1.                                        
           MOVE '158'  TO SEG-LONG-1.                                           
           MOVE '12'  TO KEY-LONG-1.                                            
           MOVE DIGSC-DIGFC0                                                    
                         TO KEY-VAL-1.                                          
      *                                                                         
           MOVE 'DIGFC0'  TO PATH-NAME.                                         
      *                                                                         
           IF PSB-NOT-OK THEN                                                   
                         PERFORM SCHEDULING-EXEC                                
           END-IF.                                                              
      *                                                                         
       FIN-CLEF-DIGFC0.    EXIT.                                                
                EJECT                                                           
                                                                                
EMOD                                                                            
