      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                 00010001
      *==> DARTY ****************************************************** 00020000
      *          * TRAITEMENT DE DATE                                 * 00021002
      *    BATCH * ZONE DE WORKING DU PROGRAMME APPELANT              * 00030002
      *          * ZONE DE LINKAGE DU PROGRAMME APPELE    BETDATC     * 00040002
      *          *                                                    * 00041000
      ************************************************* COPY WORKDATC * 00050000
      *                                                                 00060000
       01  WORK-BETDATC.                                                00070000
      *                                                                 00080000
          02 GFDATA                      PIC X.                         00090000
      *                                                                 00100000
          02 GFJJMMSSAA.                                                00110000
             05 GFJOUR                   PIC XX.                        00120000
             05 GFJJ REDEFINES GFJOUR    PIC 99.                        00130000
             05 GFMOIS                   PIC XX.                        00140000
             05 GFMM REDEFINES GFMOIS    PIC 99.                        00150000
             05 GFSIECLE                 PIC XX.                        00160000
             05 GFSS REDEFINES GFSIECLE  PIC 99.                        00170000
             05 GFANNEE                  PIC XX.                        00180000
             05 GFAA REDEFINES GFANNEE   PIC 99.                        00190000
      *                                                                 00200000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00210000
          02 GFQNTA                      PIC 999.                       00220000
          02 GFQNT0                      PIC 9(5).                      00230000
      *                                                                 00240000
      *   RESULTATS SEMAINE                                             00250000
          02 GFSMN                       PIC 9.                         00260000
          02 GFSMN-LIB-C                 PIC X(3).                      00270000
          02 GFSMN-LIB-L                 PIC X(8).                      00280000
      *                                                                 00290000
      *   LIBELLE MOIS                                                  00300000
          02 GFMOI-LIB-C                 PIC X(3).                      00310000
          02 GFMOI-LIB-L                 PIC X(9).                      00320000
      *                                                                 00330000
      * DIFFERENTES EXPRESSIONS DES DATES                               00340000
      *   FORME SSAAMMJJ                                                00350000
          02 GFSAMJ-0                    PIC X(8).                      00360000
      *   FORME AAMMJJ                                                  00370000
          02 GFAMJ-1                     PIC X(6).                      00380000
      *   FORME JJMMSSAA                                                00390000
          02 GFJMSA-2                    PIC X(8).                      00400000
      *   FORME JJMMAA                                                  00410000
          02 GFJMA-3                     PIC X(6).                      00420000
      *   FORME JJ/MM/AA                                                00430000
          02 GFJMA-4                     PIC X(8).                      00440000
      *   FORME JJ/MM/SSAA                                              00450000
          02 GFJMSA-5                    PIC X(10).                     00460000
      *                                                                 00470000
          02 GFVDAT                      PIC X.                         00480000
          02 GFBISS                      PIC 9.                         00490000
      *                                                                 00500000
          02 GFWEEK.                                                    00510000
             05 GFSEMSS                  PIC 99.                        00520000
             05 GFSEMAA                  PIC 99.                        00530000
             05 GFSEMNU                  PIC 99.                        00540000
      *                                                                 00550000
          02 GFAJOUX                     PIC XX.                        00550100
          02 GFAJOUP REDEFINES GFAJOUX   PIC 99.                        00550200
          02 FILLER                      PIC X(6).                      00560000
      *                                                                 00570000
          02 GF-MESS-ERR                 PIC X(60).                     00580000
                                                                                
