#!/bin/ksh

USER=M907
db2 connect to ${MT_DB_LOGIN}
db2 set current schema=M907
db2 EXPORT TO ${MT_TMP}/runstat1.lst OF DEL MODIFIED BY nochardel MESSAGES ${MT_TMP}/runstat1.msg "select distinct 'select '''||strip(tabname)||''' from SYSIBM.SYSDUMMY1 where (select count(*) from '||strip(tabname)||') > '||CARD||';' from SYSCAT.TABLES where TABSCHEMA = upper('$USER') and TYPE = 'T'"
echo "comparing nb rows in tables and cardinality in statistics"
db2 -xtf ${MT_TMP}/runstat1.lst | egrep '[A-Z]' >${MT_TMP}/tables_a_runstater.lst
echo "set current schema=M907;" > ${MT_TMP}/runstat2.lst
awk '{print "RUNSTATS ON TABLE " $1 " ON ALL COLUMNS WITH DISTRIBUTION ON ALL COLUMNS AND SAMPLED DETAILED INDEXES ALL;"}' ${MT_TMP}/tables_a_runstater.lst >>${MT_TMP}/runstat2.lst
echo "nb tables on which stats are needed :"
wc -l ${MT_TMP}/runstat2.lst
echo " computing statistics on needed tables "
db2 -xvtf ${MT_TMP}/runstat2.lst >${MT_LOG}/runstat2.log
export RUNSTATS=$(wc -l ${MT_TMP}/runstat2.lst | awk '{print $1}')
db2 connect reset
db2 terminate
