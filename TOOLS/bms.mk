#SHELL += -x

.SUFFIXES: .bms .map

CICSMAP := cicsmap
BMSFILES := $(wildcard *.bms)
MAPFILES := $(addsuffix .map,$(basename $(notdir $(BMSFILES))))
MAPDIR := ../../map

all: $(MAPFILES)

.bms.map:
	-@echo "compilation de $<" ; \
	cd $(MAPDIR) ; \
	$(CICSMAP) -m $* ../DECOZ/BMS/$< ; \
	mv $* ../DECOZ/COPYMAP/$*.cpy
