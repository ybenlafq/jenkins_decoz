#!/usr/bin/perl

use File::Basename;
require 5.005;

if ( $ARGV[0] eq "-o" ) { $ext = $ARGV[1]; shift( @ARGV ); shift( @ARGV ); }
if ( $ARGV[0] eq "-f" ) { $file_make = $ARGV[1]; shift( @ARGV ); shift( @ARGV ); }

$flag=0;

open(E1, "$file_make") || die "File $file_make open failed ...\n\n";
open(W1, ">$file_make.tmp");

while ( <E1> )
{
	if ( /^# DO NOT DELETE/ ) { print W1; $flag=1; }
	if ( $flag ) { close(E1); last; }
	print W1;
}

if ( !$flag ) { print W1 "# DO NOT DELETE\n"; }

for($z=0; $z<=$#ARGV; $z+=1)
{
	( $open_file ) = $ARGV[$z] =~ /([^\.]+)/g;
	open (E1, "$open_file.deps") || die "$open_file.deps open failed ....";
	print W1 "$open_file.$ext: $open_file.deps\n";
	while ( <E1> )
	{
		print W1 "$open_file.$ext: $_"
	}
	close( E1 );
}

close( W1 );

rename("$file_make.tmp", "$file_make");
system("chmod 644 $file_make");


