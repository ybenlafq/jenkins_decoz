#!/bin/ksh

echo "prepparing the list of rebind requests"
db2 connect to $MT_DB_LOGIN
db2 EXPORT TO ${MT_TMP}/rebind.lst OF DEL MODIFIED BY nochardel MESSAGES ${MT_TMP}/rebind.msg "select 'REBIND PACKAGE '||trim(PKGSCHEMA)||'.'||PKGNAME||CASE WHEN PKGVERSION = ' ' THEN '' else ' VERSION '||PKGVERSION END||';'
 from SYSCAT.PACKAGES where OWNER = upper('$MT_DB_USER')"
echo "executing the rebind packages "
db2 -svtf ${MT_TMP}/rebind.lst >${MT_LOG}/rebind.log
tail ${MT_LOG}/rebind.log
db2 connect reset
db2 terminate
