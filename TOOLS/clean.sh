#!/bin/ksh


if [[ $ROOT_DIR == "" ]]
then
   echo "ROOT_DIR non positionnee!"
   exit
else
   cd $ROOT_DIR/EXE && rm -f *
   cd $ROOT_DIR/MAP && rm -f *
fi


if [[ $DATA != "" ]]
then 
   cd $DATA && rm -f *
fi


if [[ $MT_LOG != "" ]]
then
   cd $MT_LOG && rm -f *
fi


if [[ $MT_TMP != "" ]]
then
   cd $MT_TMP && rm -f *
fi


cicscp stop region CICSA 2>/dev/null
cicscp destroy region CICSA -f 2>/dev/null

cat >/tmp/DestroySfs <<!eof
CICSAcicsnlqfile
CICSAcicsnrectsqfil
CICSAcicsplqfile
CICSAcicsrectsqfile
CICSAcicstdqlgfile
CICSAcicstdqnofile
CICSAcicstdqphfile
FIGPRT
!eof

cat /tmp/DestroySfs | while read fic
do
  sfsadmin destroy file -server /.:/cics/sfs/pilote $fic
done 2>/dev/null
