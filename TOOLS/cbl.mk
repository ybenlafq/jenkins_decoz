SHELL += -x

BUILD := buildmake.sh
COBOLDIR := BATCH CICS TECHNIQUE/ASM_CICS TECHNIQUE/ASM_BATCH TECHNIQUE/ESYT

all: cbl


cbl:
	-@echo "Compilation des programmes cobol convertis"
	$(foreach d, $(COBOLDIR), cd $(d) ; $(BUILD) ; $(MAKE); cd -;)
