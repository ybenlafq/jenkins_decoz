#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG14FP.ksh                       --- VERSION DU 06/05/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG14F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/08 AT 15.27.34 BY BURTECA
#    STANDARDS: P  JOBSET: FG14FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# ********************************************************************
#  QUIESCE DES TABLESPACES RSRC10FP RSRC20 RSFG02 AVANT MAJ
#  REPRISE: OUI
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON MONDAY    2016/02/08 AT 15.27.34 BY BURTECA
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG14FP
# *                           FREQ...: D
# *                           TITLE..: 'NETTING SAP  '
# *                           APPL...: REPPARIS
# *                           BACKOUT: JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# ********************************************************************
#  QUIESCE DES TABLESPACES RSRC10FP RSRC20 RSFG02 AVANT MAJ
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG14FPAA PGM=DSNUTILB   ** ID=AAA
# ***********************************
#
#FG14FPAA
#FG14FPAA Delete by Metaware, DSNUTILB avec QUIESCE ou REPAIR
#FG14FPAA
#
       JUMP_LABEL=FG14FPAB
       ;;
(FG14FPAB)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  IEBGENER DE LA VALEUR DU RBA
#    REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME ET BACKOUT=JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG14FPAD PGM=IEBGENER   ** ID=AAF
# ***********************************
       JUMP_LABEL=FG14FPAD
       ;;
(FG14FPAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SYSUT1 ${DATA}/PXX0.RBA.QFG14FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG14FPAE
       ;;
(FG14FPAE)
       m_CondExec 00,EQ,FG14FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG140 :  PREPARATION DES ECRITURES COMPTABLES COSYS
#            +++         DES ECRITURES POUR POSTING G.L PARIS ET METZ
#            +++         DES ECRITURES COMPTA DANS GCV
#            +++         DES ECRITURES COMPTA DANS GCT
#    REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME ET BACKOUT=JOBSET
#    ==> VERIFIER LE BACKOUT FG14FPR1: RECOVER TO RBA DE RSRC10FP RSRC
#                                      RSFG02
# ********************************************************************
# 
# ***********************************
# *   STEP FG14FPAG PGM=IKJEFT01   ** ID=AAK
# ***********************************
       JUMP_LABEL=FG14FPAG
       ;;
(FG14FPAG)
# 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES CORRESPONDANCES PERIODES
#    RSFG11   : NAME=RSFG11,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG11 /dev/null
# ******* TABLE DE CONTROLE DES TRANSFERT APPC
#    RSRC10FP : NAME=RSRC10FP,MODE=U - DYNAM=YES
# -X-FG14FPR1 - IS UPDATED IN PLACE WITH MODE=U
       m_FileAssign -d SHR RSRC10FP /dev/null
# ******  TABLE APPC DES EDITIONS COMPTABLES
#    RSRC20   : NAME=RSRC20,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSRC20 /dev/null
# ******* TABLE COMPTABLE GROUPE
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG02 /dev/null
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG14FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* FICHIER DES ECRITURES COMPTABLES POUR COSYS
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 FCOSYS ${DATA}/PXX0.F07.BFG140AP
# ******* FICHIERS  GCT TOUTES FILIALES
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT907 ${DATA}/PXX0.F07.FG14FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT989 ${DATA}/PXX0.F89.FG14FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT945 ${DATA}/PXX0.F45.FG14FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT961 ${DATA}/PXX0.F61.FG14FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT991 ${DATA}/PXX0.F91.FG14FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT996 ${DATA}/PXX0.F96.FG14FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT997 ${DATA}/PXX0.F44.FG14FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT916 ${DATA}/PXX0.F16.FG14FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT908 ${DATA}/PXX0.F08.FG14FPX
       m_FileAssign -d SHR FGCT975 /dev/null
       m_FileAssign -d SHR FGCT994 /dev/null
# 
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR
# ********* SI FSIMU = REEL ==> MISE A JOUR
       m_FileAssign -i FSIMU
REEL
_end
# ********* FSOCSIMU = SOCIETE/ENTITE GL A SIMULER
       m_FileAssign -i FSOCSIMU

_end
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG140 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG14FPAH
       ;;
(FG14FPAH)
       m_CondExec 04,GE,FG14FPAG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG115 : GENERATION D'UN FICHIER A DESTINATION D'HADOOP
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG14FPAJ PGM=IKJEFT01   ** ID=AAP
# ***********************************
       JUMP_LABEL=FG14FPAJ
       ;;
(FG14FPAJ)
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX
#    RSFI10   : NAME=RSFG02,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFI10 /dev/null
# ******  FICHIER TRIE DES MUTATIONS A FACTURER
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 CSVFG02 ${DATA}/PXX0.F07.BFG115AP
# ******  DATE JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG14FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******

       m_ProgramExec -b BFG115 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG14FPAK
       ;;
(FG14FPAK)
       m_CondExec 04,GE,FG14FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  FORMATAGE SYSIN PUT FTP FTFG14FP
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG14FPAM PGM=EZACFSM1   ** ID=AAU
# ***********************************
#       JUMP_LABEL=FG14FPAM
#       ;;
#(FG14FPAM)
#       m_FileAssign -i SYSIN
#PUT 'PXX0.F07.BFG115AP(0)' ${YR4}${LMON}${LDAY_RTFG02}ZIP
#QUIT
#_end
#       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM.FG14FPAM.FTFG14FP
#       m_ProgramExec EZACFSM1 
# ********************************************************************
#  ENVOI FTP SUR LA GATEWAY DU FTFG14FP
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG14FPAQ PGM=FTP        ** ID=AAZ
# ***********************************
# 
# ********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG14FPZA
       ;;
(FG14FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG14FPAM.FTFG14FP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
