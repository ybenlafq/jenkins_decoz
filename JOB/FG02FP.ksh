#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG02FP.ksh                       --- VERSION DU 06/05/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG02F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.32.13 BY BURTEC2
#    STANDARDS: P  JOBSET: FG02FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# ********************************************************************
#  TRI DES FICHIERS TRESO AVEC OMIT DES ENREGS DE DEBUT ET FIN
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON THURSDAY  2013/06/27 AT 15.32.12 BY BURTEC2
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG02FP
# *                           FREQ...: 5W
# *                           TITLE..: 'FIS : TRESORERIE'
# *                           APPL...: REPPARIS
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# ********************************************************************
#  TRI DES FICHIERS TRESO AVEC OMIT DES ENREGS DE DEBUT ET FIN
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAA PGM=SORT       ** ID=AAA
# ***********************************
       JUMP_LABEL=FG02FPAA
       ;;
(FG02FPAA)
       m_OutputAssign -c "*" SYSOUT
# **************************************
#  DEPENDANCES POUR PLAN :             *
# **************************************
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.FTP.F99.TRESO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG02FPAA.BFG002A
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,27,A,46,8,A),FORMAT=CH 
  OMIT COND=(1,5,EQ,C'DEBUT',OR,1,3,EQ,C'FIN'),FORMAT=CH 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG02FPAB
       ;;
(FG02FPAB)
       m_CondExec 00,EQ,FG02FPAA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG002A
#  -------------
#  TRAITEMENT DU FICHIER TRESO ISSU DU PC DA LA TRESORERIE
#    (PXX0.F99.TRESO)
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAD PGM=IKJEFT01   ** ID=AAF
# ***********************************
       JUMP_LABEL=FG02FPAD
       ;;
(FG02FPAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFG10   : NAME=RSFG10,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG10 /dev/null
# ------  TABLES EN MAJ
#    RSAN00   : NAME=RSAN00FP,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
# ------  FICHIER TRESORERIE TRIE
       m_FileAssign -d SHR -g +1 FFMICRO ${DATA}/PTEM.FG02FPAA.BFG002A
# ------  DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG02FP.R$[RUN](FG02FPAD),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------  FICHIER EN PREPARATION POUR BFG000
       m_FileAssign -d NEW,CATLG,DELETE -r 185 -t LSEQ -g +1 FFG01 ${DATA}/PTEM.FG02FPAD.BFG002XP
# ------  COMPTE-RENDU D'EXECUTION IFG002A
       m_FileAssign -d NEW,CATLG IMPRIM ${DATA}/FG02FPA.FG02FPAD.IMPRIM
# ------  LISTE DES ANOAMLIES (FACTURES REJETEES)
       m_FileAssign -d NEW,CATLG ILISTE ${DATA}/FG02FPA.FG02FPAD.ILISTE

       m_ProgramExec -b BFG002A 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG02FPAE
       ;;
(FG02FPAE)
       m_CondExec 04,GE,FG02FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER FFG01 (BFG002XP) TRESO
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAG PGM=SORT       ** ID=AAK
# ***********************************
       JUMP_LABEL=FG02FPAG
       ;;
(FG02FPAG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG02FPAD.BFG002XP
       m_FileAssign -d NEW,CATLG,DELETE -r 185 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG02FPAG.BFG002YP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,54,A),FORMAT=CH 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG02FPAH
       ;;
(FG02FPAH)
       m_CondExec 00,EQ,FG02FPAG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG000
#  ------------
#  ALIMENTATION DE LA TABLE RTFG01 : FACTURATION GROUPE
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAJ PGM=IKJEFT01   ** ID=AAP
# ***********************************
       JUMP_LABEL=FG02FPAJ
       ;;
(FG02FPAJ)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA99   : NAME=RSGA99FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA99 /dev/null
# ------  TABLES EN MAJ
#    RSAN00   : NAME=RSAN00FP,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
# ------  FICHIER DES FACTURES TRESO
       m_FileAssign -d SHR -g +1 FFG01 ${DATA}/PTEM.FG02FPAG.BFG002YP
# ------  DATE DU JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG02FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------  EDITIONS DE CONTROLE
       m_FileAssign -d NEW,CATLG FGVENANO ${DATA}/FG02FPA.FG02FPAJ.FGVENANO
       m_FileAssign -d NEW,CATLG FGNATANO ${DATA}/FG02FPA.FG02FPAJ.FGNATANO
       m_FileAssign -d NEW,CATLG FGFRQANO ${DATA}/FG02FPA.FG02FPAJ.FGFRQANO
       m_FileAssign -d NEW,CATLG FGANNANO ${DATA}/FG02FPA.FG02FPAJ.FGANNANO
       m_FileAssign -d NEW,CATLG FG10ANO ${DATA}/FG02FPA.FG02FPAJ.FG10ANO
       m_FileAssign -d NEW,CATLG FG01ANO ${DATA}/FG02FPA.FG02FPAJ.FG01ANO
# 

       m_ProgramExec -b BFG000A 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG02FPAK
       ;;
(FG02FPAK)
       m_CondExec 04,GE,FG02FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  SAUVEGARDE DES FICHIERS TRESO SUR UN FICHIER HISTORIQUE
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAM PGM=IDCAMS     ** ID=AAU
# ***********************************
       JUMP_LABEL=FG02FPAM
       ;;
(FG02FPAM)
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +1 IN1 ${DATA}/PTEM.FG02FPAA.BFG002A
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0.F99.TRESOHIS
# IDCAMS REPRO
   m_FileRepro -i IN1 -o OUT1
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***
# ************************************************
       JUMP_LABEL=FG02FPAN
       ;;
(FG02FPAN)
       m_CondExec 16,NE,FG02FPAM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  SUPPRESSION DES GENERATIONS DES FICHIERS TESO SI TRAITEMENT OK
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAQ PGM=IDCAMS     ** ID=AAZ
# ***********************************
       JUMP_LABEL=FG02FPAQ
       ;;
(FG02FPAQ)
       m_OutputAssign -c "*" SYSPRINT
# 
       m_FileAssign -d SHR IN1 /dev/null
# 
   m_FileDelete "${DATA}/PXX0.FTP.F99.TRESO.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***
# ************************************************
       JUMP_LABEL=FG02FPAR
       ;;
(FG02FPAR)
       m_CondExec 16,NE,FG02FPAQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  CREATION D'UNE GENERATION A VIDE DU FICHIER TRESO POUR NE PAS
#  PLANTER LE LENDEMAIN SI PAS DE TRANSFERT
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG02FPAT PGM=IDCAMS     ** ID=ABE
# ***********************************
       JUMP_LABEL=FG02FPAT
       ;;
(FG02FPAT)
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0.FTP.F99.TRESO
# IDCAMS REPRO
   m_FileRepro -i IN1 -o OUT1
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***
# ************************************************
       JUMP_LABEL=FG02FPAU
       ;;
(FG02FPAU)
       m_CondExec 16,NE,FG02FPAT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG02FPZA
       ;;
(FG02FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG02FPAA.BFG002A.*"
   m_FileDelete "${DATA}/PTEM.FG02FPAD.BFG002XP.*"
   m_FileDelete "${DATA}/PTEM.FG02FPAG.BFG002YP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
