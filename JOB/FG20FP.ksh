#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG20FP.ksh                       --- VERSION DU 06/05/2016 14:20
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG20F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 14.40.36 BY BURTECA
#    STANDARDS: P  JOBSET: FG20FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# 
# ********************************************************************
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE
#     REPRISE : OUI
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON THURSDAY  2015/08/20 AT 14.40.36 BY BURTECA
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG20FP
# *                           FREQ...: W
# *                           TITLE..: 'EDIT FACT FIS'
# *                           APPL...: REPPARIS
# *                           BACKOUT: JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ********************************************************************
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE
#     REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAA PGM=DSNUTILB   ** ID=AAA
# ***********************************
#
#FG20FPAA
#FG20FPAA Delete by Metaware, DSNUTILB avec QUIESCE ou REPAIR
#FG20FPAA
#
       JUMP_LABEL=FG20FPAB
       ;;
(FG20FPAB)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#           PRINT DE LA VALEUR DU RBA
#           REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAD PGM=IEBGENER   ** ID=AAF
# ***********************************
       JUMP_LABEL=FG20FPAD
       ;;
(FG20FPAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ******* FICHIER CONTENANT LE NUMERO DE RBA
       m_FileAssign -d SHR -g +1 SYSUT1 ${DATA}/PXX0.RBA.QFG20FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG20FPAE
       ;;
(FG20FPAE)
       m_CondExec 00,EQ,FG20FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  FERMETURE DE LA BASE D'IMPRESSION SOUS CIDAR ET TRANSACTION FG10
#  DISABLE DES TRANSACTIONS FF62 ET FF64 SOUS CINCP
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAG PGM=PGMSVC34   ** ID=AAK
# ***********************************
# ********************************************************************
#   WAIT POUR ATTENDRE LA FERMETURE
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAJ PGM=WAITSS     ** ID=AAP
# ***********************************
# ********************************************************************
#  BFX901 : CHARGEMENT DE LA TABLE RTFX90 NETTING
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAM PGM=IKJEFT01   ** ID=AAU
# ***********************************
       JUMP_LABEL=FG20FPAM
       ;;
(FG20FPAM)
# ******* UNLOAD DE LA TABLE FX00 DU GROUPE (BGA01G)
       m_FileAssign -d SHR -g +0 FFX90 ${DATA}/PNCGG.F99.FX00M907
# ********TABLE DES FACTURES INTER-SOCIETE
#    RSFX90   : NAME=RSFX90P,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFX90 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFX901 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPAN
       ;;
(FG20FPAN)
       m_CondExec 04,GE,FG20FPAM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  RELOAD DE LA TABLE RTFM93 A PARTIR DU TS RSFM93G :JOB UNLOAD BGA01G
#  SOUS RDAR
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAQ PGM=DSNUTILB   ** ID=AAZ
# ***********************************
       JUMP_LABEL=FG20FPAQ
       ;;
(FG20FPAQ)
       m_FileAssign -d NEW SORTOUT ${TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM93   : NAME=RSFM93FP,MODE=(U,N) - DYNAM=YES
       m_FileAssign -d SHR RSFM93 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG.F99.UNLOAD.RSFM93G
       m_FileAssign -d NEW SYSUT1 ${TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d NEW,CATLG,CATLG SYSERR01 ${LOG}/M907.RTFM93.log
       m_FileAssign -d SHR MT_CTL01 ${MT_CTL_FILES}/M907.RTFM93.sysload
       m_DBTableLoad -c MT_CTL01 -i SYSREC -e SYSERR01 -t M907.RTFM93
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPAR
       ;;
(FG20FPAR)
       m_CondExec 04,GE,FG20FPAQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG180 : CHANGEMENT DES DATES D'ECHEANCES DE FACTURE FAISANT
#           INTERVENIR UNE SOCI�T� DE GESTION DONT LA SOCI�T� COMPTABL
#           VA ETRE INT�GR�E A UNE AUTRE SOCI�T� LE 1ER JOUR DU MOIS
#           SUIVANT.
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAT PGM=IKJEFT01   ** ID=ABE
# ***********************************
       JUMP_LABEL=FG20FPAT
       ;;
(FG20FPAT)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ********TABLE DES FACTURES INTER-SOCIETE
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
# -X-FG20FPR1 - IS UPDATED IN PLACE WITH MODE=U
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* ETAT DE CONTROLE(ARCHIVE E.O.S DESCENDU A LA DEMANDE)
       m_FileAssign -d NEW,CATLG IFG180 ${DATA}/FG20FPA.FG20FPAT.IFG180
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG180 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPAU
       ;;
(FG20FPAU)
       m_CondExec 04,GE,FG20FPAT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG110 : EXTRACTION DES FACTURES INTER-SOCIETE NON ENCORE COMTABILI
#              CREATION FICHIER FFGCTA : FACTURES A EDITEES
#              CREATION FICHIER FFGANO : ANOMALIES
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPAX PGM=IKJEFT01   ** ID=ABJ
# ***********************************
       JUMP_LABEL=FG20FPAX
       ;;
(FG20FPAX)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES ECS NETTING
#    RSFX90P  : NAME=RSFX90P,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFX90P /dev/null
# ******* TABLE DES LIEUX   (RDAR)
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES CORRESPONDANCES JOURNAUX
#    RSFG12   : NAME=RSFG12,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG12 /dev/null
# ******* TABLE DES CORRESPONDANCES DES COMPTES
#    RSFG13   : NAME=RSFG13,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG13 /dev/null
# 
# ********TABLE DES FACTURES INTER-SOCIETE
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG02 /dev/null
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* FICHIER DES FACTURES  A EDITER
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FFGCTA ${DATA}/PTEM.FG20FPAX.BFG110AP
# ******* FICHIER DES ANOMALIES A EDITER
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FFGANO ${DATA}/PTEM.FG20FPAX.BFG110BP
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR
# ********* SI FSIMU = REEL ==> MISE A JOUR
       m_FileAssign -i FSIMU
REEL
_end
# ********* FSOCSIMU = SOCIETE/ENTITE GL A SIMULER
       m_FileAssign -i FSOCSIMU

_end
# ********* FCOMPTA  = DATE COMPTABLE DE SIMULATION
       m_FileAssign -i FCOMPTA

_end
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG110 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPAY
       ;;
(FG20FPAY)
       m_CondExec 04,GE,FG20FPAX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES A EDITER
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBA PGM=SORT       ** ID=ABO
# ***********************************
       JUMP_LABEL=FG20FPBA
       ;;
(FG20FPBA)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPAX.BFG110AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPBA.BFG210AP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,13,A,37,8,A,157,3,A,14,23,A),FORMAT=CH 
  RECORD F 180 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPBB
       ;;
(FG20FPBB)
       m_CondExec 00,EQ,FG20FPBA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG210  : EDITION DES FACTURES INTER-SOCIETE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBD PGM=BFG210     ** ID=ABT
# ***********************************
       JUMP_LABEL=FG20FPBD
       ;;
(FG20FPBD)
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g +1 FFGCTA ${DATA}/PTEM.FG20FPBA.BFG210AP
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FG20FPBD),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
       m_FileAssign -d NEW,CATLG IFG210 ${DATA}/FG20FPA.FG20FPBD.IFG210
       m_ProgramExec BFG210 
# ********************************************************************
#         TRI DU FICHIER DES ANOMALIES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBG PGM=SORT       ** ID=ABY
# ***********************************
       JUMP_LABEL=FG20FPBG
       ;;
(FG20FPBG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPAX.BFG110BP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPBG.BFG200AP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,37,A),FORMAT=CH 
  RECORD F 180 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPBH
       ;;
(FG20FPBH)
       m_CondExec 00,EQ,FG20FPBG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG200  : EDITION DES ANOMALIES D'EXTRACTION DES FACTURES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBJ PGM=IKJEFT01   ** ID=ACD
# ***********************************
       JUMP_LABEL=FG20FPBJ
       ;;
(FG20FPBJ)
# ******* TABLE DES MESSAGES
#    RSGA99   : NAME=RSGA99FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA99 /dev/null
# ******* FICHIER DES ANOMALIES
       m_FileAssign -d SHR -g +1 FFGANO ${DATA}/PTEM.FG20FPBG.BFG200AP
# ******* PARAMETRE FDATE
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FG20FPBJ),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
       m_FileAssign -d NEW,CATLG IFG200 ${DATA}/FG20FPA.FG20FPBJ.IFG200
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG200 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPBK
       ;;
(FG20FPBK)
       m_CondExec 04,GE,FG20FPBJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG220 : EXTRACTION DES FACTURES INTER-SOCIETE NON ENCORE COMTABILI
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBM PGM=IKJEFT01   ** ID=ACI
# ***********************************
       JUMP_LABEL=FG20FPBM
       ;;
(FG20FPBM)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES FACTURES INTER-SOCIETE
#    RSFG01   : NAME=RSFG01,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES
#    RSFG02   : NAME=RSFG02,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG02 /dev/null
# ******* TABLE DES COMMENTAIRES SUR FACTURE
#    RSFG06   : NAME=RSFG06,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG06 /dev/null
# ******* TABLE DES DETAILS DES FACTURATIONS MANUELLES
#    RSFG07   : NAME=RSFG07,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG07 /dev/null
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* FICHIER DES FACTURES  A EDITER
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG20FPBM.BFG220AP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG220 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPBN
       ;;
(FG20FPBN)
       m_CondExec 04,GE,FG20FPBM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES POUR LES FILIALES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBQ PGM=SORT       ** ID=ACN
# ***********************************
       JUMP_LABEL=FG20FPBQ
       ;;
(FG20FPBQ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPBM.BFG220AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPBQ.BFG220BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
  RECORD F 175 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPBR
       ;;
(FG20FPBR)
       m_CondExec 00,EQ,FG20FPBQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG230  : CREATION DE L'ETAT JFG011 DANS L'IMPRESSION GENERALISE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBT PGM=DFSRRC00   ** ID=ACS
# ***********************************
       JUMP_LABEL=FG20FPBT
       ;;
(FG20FPBT)
# ACS     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG200RC1),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG200RC1
# ******  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG20FPBQ.BFG220BP(+1),DISP=SHR
# ******  BASE IMPRESSION GENERALISEE
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG20FPR2)
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#         TRI DU FICHIER DES FACTURES POUR PARIS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPBX PGM=SORT       ** ID=ACX
# ***********************************
       JUMP_LABEL=FG20FPBX
       ;;
(FG20FPBX)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPBM.BFG220AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG20FPA.FG20FPBX.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPBY
       ;;
(FG20FPBY)
       m_CondExec 00,EQ,FG20FPBX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES POUR 505/913/930
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCA PGM=SORT       ** ID=ADC
# ***********************************
       JUMP_LABEL=FG20FPCA
       ;;
(FG20FPCA)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPBM.BFG220AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG20FPA.FG20FPCA.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=((15,3,CH,EQ,C'505'),OR,(15,3,CH,EQ,C'913'),OR,          * 
                (15,3,CH,EQ,C'930')) 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPCB
       ;;
(FG20FPCB)
       m_CondExec 00,EQ,FG20FPCA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES (E)
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCD PGM=SORT       ** ID=ADH
# ***********************************
       JUMP_LABEL=FG20FPCD
       ;;
(FG20FPCD)
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EDITION DES FACTURES POUR CUMUL
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPBM.BFG220AP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPCD.BFG220DP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(24,1,CH,EQ,C'E') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPCE
       ;;
(FG20FPCE)
       m_CondExec 00,EQ,FG20FPCD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES POUR CUMUL MISCROLIST
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCG PGM=SORT       ** ID=ADM
# ***********************************
       JUMP_LABEL=FG20FPCG
       ;;
(FG20FPCG)
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EDITION DES FACTURES POUR CUMUL
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0.F07.BFG220CP
       m_FileAssign -d SHR -g +1 -C ${DATA}/PTEM.FG20FPCD.BFG220DP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0.F07.BFG220CP
       m_FileAssign -i SYSIN
  MERGE FIELDS=COPY 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPCH
       ;;
(FG20FPCH)
       m_CondExec 00,EQ,FG20FPCG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# *** PARTIE AJOUTER PAR PHIL LE 251095 ******************************
# ********************************************************************
#  BFG100 : CALCUL ET GENERATION DES ECRITURES REGLEMENTS
#              CREATION FICHIER FFGREGL: FACTURATION REGLEES
#              CREATION FICHIER FFGCTA : EDITION ETAT COMPTA
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCJ PGM=IKJEFT01   ** ID=ADR
# ***********************************
       JUMP_LABEL=FG20FPCJ
       ;;
(FG20FPCJ)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX   (RDAR)
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# 
# ******* TABLE DES CORRESPONDANCES JOURNAUX
#    RSFG12   : NAME=RSFG12,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG12 /dev/null
# ******* TABLE DES FACTURES INTER-SOCIETE
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG02 /dev/null
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* ETAT REGLEMENT (POUR J.FAIDY)
       m_FileAssign -d NEW,CATLG IFG100 ${DATA}/FG20FPA.FG20FPCJ.IFG100
# ******* FICHIER DE L ETAT DE COMPTA (LRECL 180)
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FFGCTA ${DATA}/PTEM.FG20FPCJ.BFG100AP
# ******* FICHIER POUR FACTURATION REGLEES (LRECL 090)
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 FFGREGL ${DATA}/PTEM.FG20FPCJ.BFG100BP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG100 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPCK
       ;;
(FG20FPCK)
       m_CondExec 04,GE,FG20FPCJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DE L ETAT COMPTA
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCM PGM=SORT       ** ID=ADW
# ***********************************
       JUMP_LABEL=FG20FPCM
       ;;
(FG20FPCM)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPCJ.BFG100AP
# ******* FICHIER DE DE L ETA COMPTA
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPCM.BFG100CP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,36,A),FORMAT=CH 
  RECORD F 180 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPCN
       ;;
(FG20FPCN)
       m_CondExec 00,EQ,FG20FPCM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG210  : EDITION COMPTABILITE REGLEMENTS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCQ PGM=BFG210     ** ID=AEB
# ***********************************
       JUMP_LABEL=FG20FPCQ
       ;;
(FG20FPCQ)
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g +1 FFGCTA ${DATA}/PTEM.FG20FPCM.BFG100CP
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FG20FPCQ),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
       m_FileAssign -d NEW,CATLG IFG210 ${DATA}/FG20FPA.FG20FPCQ.IFG210
       m_ProgramExec BFG210 
# ********************************************************************
#         TRI DU FICHIER REGLEMENTS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCT PGM=SORT       ** ID=AEG
# ***********************************
       JUMP_LABEL=FG20FPCT
       ;;
(FG20FPCT)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPCJ.BFG100BP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPCT.BFG100DP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,13,A,26,14,A,14,12,A),FORMAT=CH 
  RECORD F 090 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPCU
       ;;
(FG20FPCU)
       m_CondExec 00,EQ,FG20FPCT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG215 : EDITION DES FACTURATIONS ARRIVES A ECHEANCE
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPCX PGM=IKJEFT01   ** ID=AEL
# ***********************************
       JUMP_LABEL=FG20FPCX
       ;;
(FG20FPCX)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* FICHIER DES REGLEMENT A EDITER
       m_FileAssign -d SHR -g +1 FFGREGL ${DATA}/PTEM.FG20FPCT.BFG100DP
# 
# ******* FICHIER DES REGLEMENTS A TRIER
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG20FPCX.BFG100EP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG215 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPCY
       ;;
(FG20FPCY)
       m_CondExec 04,GE,FG20FPCX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES REGLEMENTS ( SORTIE DU PGM BFG215)
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDA PGM=SORT       ** ID=AEQ
# ***********************************
       JUMP_LABEL=FG20FPDA
       ;;
(FG20FPDA)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPCX.BFG100EP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG20FPDA.BFG100FP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
  RECORD F 175 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPDB
       ;;
(FG20FPDB)
       m_CondExec 00,EQ,FG20FPDA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDD PGM=DFSRRC00   ** ID=AEV
# ***********************************
       JUMP_LABEL=FG20FPDD
       ;;
(FG20FPDD)
# AEV     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG200RC2),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG200RC2
# ******  FICHIER DES REGLEMENTS
# MW FFGEDG   DD  DSN=PTEM.FG20FPDA.BFG100FP(+1),DISP=SHR
# ******  BASE IMPRESSION GENERALISEE
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG20FPR3)
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#         TRI DU FICHIER DES FACTURES DE PARIS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDG PGM=SORT       ** ID=AFA
# ***********************************
       JUMP_LABEL=FG20FPDG
       ;;
(FG20FPDG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPCX.BFG100EP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG20FPA.FG20FPDG.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPDH
       ;;
(FG20FPDH)
       m_CondExec 00,EQ,FG20FPDG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES REGLEMENTS  (LA TOTALITE )
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDJ PGM=SORT       ** ID=AFF
# ***********************************
       JUMP_LABEL=FG20FPDJ
       ;;
(FG20FPDJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPCX.BFG100EP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG20FPA.FG20FPDJ.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPDK
       ;;
(FG20FPDK)
       m_CondExec 00,EQ,FG20FPDJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         MERGE DES DEUX FICHIERS FFGCTA (COMPTA + REGLEMENTS )
#         POUR EDITION
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDM PGM=SORT       ** ID=AFK
# ***********************************
       JUMP_LABEL=FG20FPDM
       ;;
(FG20FPDM)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG20FPCJ.BFG100AP
       m_FileAssign -d SHR -g +1 -C ${DATA}/PTEM.FG20FPAX.BFG110AP
# ******* FICHIER D'EDITION FACTURES + REGLEMENTS
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0.F07.REGLFACT.JOUR
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,6,A,14,3,A,37,8,A),FORMAT=CH 
  OMIT COND=(166,1,CH,NE,C'O') 
  RECORD F 180 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG20FPDN
       ;;
(FG20FPDN)
       m_CondExec 00,EQ,FG20FPDM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# * FIN DE L INSERTION ***********************************************
# ********************************************************************
#  BFG211 : EDITION
#  IFG211 : JOURNAL DE DEVERSEMENT LA FACTURATION INTER-SOCIETE
#  IFG212 : RECAPITULATIF DES JOURNAUX DE LA FACTURATION INTER-SOCIETE
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDQ PGM=IKJEFT01   ** ID=AFP
# ***********************************
       JUMP_LABEL=FG20FPDQ
       ;;
(FG20FPDQ)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG20FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* FICHIER DES FACTURES  A EDITER
       m_FileAssign -d SHR -g +1 FFGCTA ${DATA}/PEX0.F07.REGLFACT.JOUR
# 
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR
# ********* SI FSIMU = REEL ==> MISE A JOUR
       m_FileAssign -i FSIMU
REEL
_end
# ******* EDITIONS (VOIR COMMENT STEP)
       m_FileAssign -d NEW,CATLG IFG211 ${DATA}/FG20FPA.FG20FPDQ.IFG211
       m_FileAssign -d NEW,CATLG IFG212 ${DATA}/FG20FPA.FG20FPDQ.IFG212
# 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG211 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG20FPDR
       ;;
(FG20FPDR)
       m_CondExec 04,GE,FG20FPDQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  REOUVERTURE DE LA BASE D'IMPRESSION SOUS CIDAR
#  ENABLE  DES TRANSACTIONS FF62 ET FF64 SOUS CINCP
# ********************************************************************
# 
# ***********************************
# *   STEP FG20FPDT PGM=PGMSVC34   ** ID=AFU
# ***********************************
# ********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG20FPZA
       ;;
(FG20FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG20FPAX.BFG110AP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPAX.BFG110BP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPBA.BFG210AP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPBG.BFG200AP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPBM.BFG220AP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPBQ.BFG220BP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPCD.BFG220DP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPCJ.BFG100AP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPCJ.BFG100BP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPCM.BFG100CP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPCT.BFG100DP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPCX.BFG100EP.*"
   m_FileDelete "${DATA}/PTEM.FG20FPDA.BFG100FP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
