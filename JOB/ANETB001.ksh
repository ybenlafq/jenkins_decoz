#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ANETB001.ksh                       --- VERSION DU 06/05/2016 12:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j ANETB001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
       JUMP_LABEL=INITSQL
       ;;
(INITSQL)
       m_OutputAssign -c "*" SYSPUNCH
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSREC00
       m_FileAssign -i SYSIN
INSERT INTO M907.RTGA01 (CTABLEG1, CTABLEG2, WTABLEG)
   VALUES ('FGCAL', '20160111', 'O012016NNO');
_end
       m_ExecSQL -f SYSIN -o SYSREC00
#  MW - CONTOURNEMENT POUR FICHIERS SI ABSENTS
# MISSING1 EXEC PGM=IEFBR14
# GAS      DD DSN=IF02001.NETTING.GAS,DISP=(NEW,CATLG,DELETE),
#              DCB=(RECFM=FB,LRECL=80,BLKSIZE=27920),
#              UNIT=(SEM350,26),SPACE=(27920,(3,1),RLSE)
       JUMP_LABEL=MISSING2
       ;;
(MISSING2)
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSUT1 ${DATA}/BURTECA.F07.DAFG01V
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 SYSUT2 ${DATA}/PXX0.F75.DAFG01V
       m_FileRepro -i SYSUT1 -o SYSUT2
#  - DECH DE LA TABLE M907.RTFM93
       JUMP_LABEL=MISSING3
       ;;
(MISSING3)
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSPUNCH /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC00 ${DATA}/PNCGG.F99.UNLOAD.RSFM93G
       m_FileAssign -i SYSIN
SELECT * FROM M907.RTFM93;
_end
       m_ExecSQL -f SYSIN -o SYSREC00
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
