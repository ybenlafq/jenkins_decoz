#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG91FP.ksh                       --- VERSION DU 06/05/2016 12:48
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG91F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/07/20 AT 09.51.58 BY PREPA2
#    STANDARDS: P  JOBSET: FG91FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# ********************************************************************
#   UNLOAD DE LA TABLE RTFG02
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON TUESDAY   2010/07/20 AT 09.51.58 BY PREPA2
# *    JOBSET INFORMATION:    NAME...: FG91FP
# *                           FREQ...: M
# *                           TITLE..: 'PURGE RTFG02'
# *                           APPL...: REPPARIS
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# ********************************************************************
#   UNLOAD DE LA TABLE RTFG02
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG91FPAA PGM=PTLDRIVM   ** ID=AAA
# ***********************************
# { Post-translation replace-fastunload-by-dsntiaul
# //FG91FPAA EXEC PGM=PTLDRIVM,PARM=('EP=UTLGLCTL/RMAC,,,FG91FP.U0'),
# //             COND=((1,EQ,YES),(50,LT,FROM),(50,GT,TO),(1000,LT))
# //PTILIB  DD DSN=SYS2.KRYSTAL.LOADLIB,DISP=SHR
# //PTIPARM DD DSN=SYS2.KRYSTAL.PARMLIB,DISP=SHR
# //PTIXMSG DD DSN=SYS2.KRYSTAL.XMESSAGE,DISP=SHR
# //ST01MSG  DD  SYSOUT=*
# //PTIMSG01 DD  SYSOUT=*
# //ST02MSG  DD  SYSOUT=*
# //PTIMSG03 DD  SYSOUT=*
# //ST03MSG  DD  SYSOUT=*
# //PTIMSG02 DD  SYSOUT=*
# //PTIMSG   DD  SYSOUT=*
# //***************************************
# //* DEPENDANCES POUR PLAN :
# //***************************************
# //SYSTSPRT DD  SYSOUT=*
# //SYSPRINT DD  SYSOUT=*
# //SYSUDUMP DD  SYSOUT=*
# //SYSPUNCH DD  DUMMY
# //SYSABEND DD  SYSOUT=*
# //DFSPARM DD *
#  OPTION MAINSIZE=192000K,FILSZ=E4444444
#  DEBUG  ABEND
# //SORTDIAG DD  DUMMY
# //SYSREC01 DD  DSN=PXX0.F07.FG02UNP(+1),DISP=(NEW,CATLG,CATLG),
# //             DCB=(DSCBM,RECFM=FB,LRECL=210,BLKSIZE=27930),
# //             UNIT=(TEM350,26),SPACE=(27930,(54136,10828),RLSE)
# //SYSIN  DD *
#   FASTUNLOAD
#   SQL-ACCESS EXTENSION
#   OUTPUT-FORMAT D
#   VALIDATE-HEADER NO
#   SELECT * FROM M907.RTFG02 ORDER CLUSTERED;
       JUMP_LABEL=FG91FPAA
       ;;
(FG91FPAA)
       m_StepLibSet DSNA10.SDSNLOAD:DSNA10.SDSNEXIT:DSNA10.DBAG.RUNLIB.LOAD
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSPUNCH /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 210 -g +1 SYSREC00 ${DATA}/PXX0.F07.FG02UNP
       m_FileAssign -i SYSIN
SELECT * FROM M907.RTFG02;
_end
       m_ExecSQL -f SYSIN -o SYSREC00
# } Post-translation
# ********************************************************************
#  TRI DU FICHIER D'UNLOAD POUR LE PGM BFG910
# ********************************************************************
# 
# ***********************************
# *   STEP FG91FPAD PGM=SORT       ** ID=AAF
# ***********************************
       JUMP_LABEL=FG91FPAD
       ;;
(FG91FPAD)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PXX0.F07.FG02UNP
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PTEM.FG91FPAD.BFG910AP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,13,A,14,10,D),FORMAT=CH 
  RECORD F 210 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG91FPAE
       ;;
(FG91FPAE)
       m_CondExec 00,EQ,FG91FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG910
#  ------------
#  PURGE DU FICHIER D'UNLOAD
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG91FPAG PGM=IKJEFT01   ** ID=AAK
# ***********************************
       JUMP_LABEL=FG91FPAG
       ;;
(FG91FPAG)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLES EN LECTURE
#    RTGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
# ------- PARAMETRE DATE
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG91FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------- FICHIERS D'UNLOAD TRIE
       m_FileAssign -d SHR -g +1 FFG02IN ${DATA}/PTEM.FG91FPAD.BFG910AP
# ------- FICHIERS SERVANT A RELOADER LA TABLE RTFG02
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 FFG02O ${DATA}/PTEM.FG91FPAG.BFG910BP

       m_ProgramExec -b BFG910 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG91FPAH
       ;;
(FG91FPAH)
       m_CondExec 04,GE,FG91FPAG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG910BP POUR LE LOAD DE LA RTFG02
# ********************************************************************
# 
# ***********************************
# *   STEP FG91FPAJ PGM=SORT       ** ID=AAP
# ***********************************
       JUMP_LABEL=FG91FPAJ
       ;;
(FG91FPAJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG91FPAG.BFG910BP
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PTEM.FG91FPAJ.BFG910CP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,26,A),FORMAT=CH 
  RECORD F 210 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG91FPAK
       ;;
(FG91FPAK)
       m_CondExec 00,EQ,FG91FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  LOAD DE LA TABLE RTFG02
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG91FPAM PGM=DSNUTILB   ** ID=AAU
# ***********************************
       JUMP_LABEL=FG91FPAM
       ;;
(FG91FPAM)
       m_FileAssign -d NEW SYSUT1 ${TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS
       m_FileAssign -d NEW SORTOUT ${TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ------ FICHIER DE LOAD
       m_FileAssign -d SHR -g +1 SYSREC ${DATA}/PTEM.FG91FPAJ.BFG910CP
# ------ TABLE RTFG02
#    RSFG02   : NAME=RSFG02,MODE=(U,N) - DYNAM=YES
# -X-FG02PFR1 - IS UPDATED IN PLACE WITH MODE=(U,N)
       m_FileAssign -d SHR RSFG02 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d NEW,CATLG,CATLG SYSERR01 ${LOG}/M907.RTFG02.log
       m_FileAssign -d SHR MT_CTL01 ${MT_CTL_FILES}/M907.RTFG02.sysload
       m_DBTableLoad -c MT_CTL01 -i SYSREC -e SYSERR01 -t M907.RTFG02
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG91FPAN
       ;;
(FG91FPAN)
       m_CondExec 04,GE,FG91FPAM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#   RUNSTAT DU TABLESPACE RTFG02
# ********************************************************************
#   REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG91FPAQ PGM=DSNUTILB   ** ID=AAZ
# ***********************************
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG91FPAR
       ;;
(FG91FPAR)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG91FPZA
       ;;
(FG91FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PXX0.F07.FG02UNP.*"
   m_FileDelete "${DATA}/PTEM.FG91FPAD.BFG910AP.*"
   m_FileDelete "${DATA}/PTEM.FG91FPAG.BFG910BP.*"
   m_FileDelete "${DATA}/PTEM.FG91FPAJ.BFG910CP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
