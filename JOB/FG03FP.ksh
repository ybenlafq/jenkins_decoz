#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG03FP.ksh                       --- VERSION DU 06/05/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG03F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 14.37.42 BY BURTECA
#    STANDARDS: P  JOBSET: FG03FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# 
# ********************************************************************
#  QUIESCE DES TABLES RTFG01 ET RTFG05 MAJ DANS LE PGM BFG150
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON THURSDAY  2015/08/20 AT 14.37.42 BY BURTECA
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG03FP
# *                           FREQ...: D
# *                           TITLE..: 'FIS : TRESORERIE'
# *                           APPL...: REPPARIS
# *                           BACKOUT: JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ********************************************************************
#  QUIESCE DES TABLES RTFG01 ET RTFG05 MAJ DANS LE PGM BFG150
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAA PGM=DSNUTILB   ** ID=AAA
# ***********************************
#
#FG03FPAA
#FG03FPAA Delete by Metaware, DSNUTILB avec QUIESCE ou REPAIR
#FG03FPAA
#
       JUMP_LABEL=FG03FPAB
       ;;
(FG03FPAB)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PRINT DE LA VALEUR DU RBA
# ********************************************************************
#  REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAD PGM=IEBGENER   ** ID=AAF
# ***********************************
       JUMP_LABEL=FG03FPAD
       ;;
(FG03FPAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ******* FICHIER CONTENANT LE NUMERO DE RBA
       m_FileAssign -d SHR -g +1 SYSUT1 ${DATA}/PXX0.RBA.QFG03FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG03FPAE
       ;;
(FG03FPAE)
       m_CondExec 00,EQ,FG03FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG150
#  ------------
#  CALCUL DE LA RENUMERATION
# ********************************************************************
#  REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAG PGM=IKJEFT01   ** ID=AAK
# ***********************************
       JUMP_LABEL=FG03FPAG
       ;;
(FG03FPAG)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFG05   : NAME=RSFG05,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG05 /dev/null
# ------  TABLES EN MAJ
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
# -X-FG03FPR0 - IS UPDATED IN PLACE WITH MODE=U
       m_FileAssign -d SHR RSFG01 /dev/null
#    RSFG05   : NAME=RSFG05,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG05 /dev/null
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG02 /dev/null
# ------  DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG03FP.R$[RUN](FG03FPAG),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------  FICHIER ENTRANT DANS LE BFG260
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFGTAB ${DATA}/PTEM.FG03FPAG.BFG260AP
# ------  FICHIER ENTRANT DANS LE BFG261
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FFGJRN ${DATA}/PTEM.FG03FPAG.BFG261AP
# ------  FICHIER ENTRANT DANS LE BFG262
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FFGCOMP ${DATA}/PTEM.FG03FPAG.BFG262AP
# ------  COMPTE-RENDU D'EXECUTION IFG150
       m_FileAssign -d NEW,CATLG IFG150 ${DATA}/FG03FPA.FG03FPAG.IFG150

       m_ProgramExec -b BFG150 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG03FPAH
       ;;
(FG03FPAH)
       m_CondExec 04,GE,FG03FPAG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG260AP
# ********************************************************************
#  REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAJ PGM=SORT       ** ID=AAP
# ***********************************
       JUMP_LABEL=FG03FPAJ
       ;;
(FG03FPAJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPAG.BFG260AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG03FPAJ.BFG260BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,11,A,59,1,A),FORMAT=CH 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPAK
       ;;
(FG03FPAK)
       m_CondExec 00,EQ,FG03FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG260
#  ------------
#  PREPARATION POUR EDITION DU TABLEAU DE RENUMERATION
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAM PGM=IKJEFT01   ** ID=AAU
# ***********************************
       JUMP_LABEL=FG03FPAM
       ;;
(FG03FPAM)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  DATE DU JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG03FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------  FICHIER ISSU DU TRI PRECEDANT
       m_FileAssign -d SHR -g +1 FFGTAB ${DATA}/PTEM.FG03FPAJ.BFG260BP
# ------  FICHIER POUR CREATION ETAT DANS LA BASE EDITION
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG03FPAM.BFG260CP
# ------  EDITION DE CONTROLE
       m_FileAssign -d NEW,CATLG IFG260 ${DATA}/FG03FPA.FG03FPAM.IFG260
# 

       m_ProgramExec -b BFG260 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG03FPAN
       ;;
(FG03FPAN)
       m_CondExec 04,GE,FG03FPAM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER POUR EDITION A PARIS DANS DISPATCH
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAQ PGM=SORT       ** ID=AAZ
# ***********************************
       JUMP_LABEL=FG03FPAQ
       ;;
(FG03FPAQ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPAM.BFG260CP
# ******* EDITION DU TABLEAU DE REMUNERATION
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG03FPA.FG03FPAQ.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPAR
       ;;
(FG03FPAR)
       m_CondExec 00,EQ,FG03FPAQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG260CP POUR PGM BFG230
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAT PGM=SORT       ** ID=ABE
# ***********************************
       JUMP_LABEL=FG03FPAT
       ;;
(FG03FPAT)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPAM.BFG260CP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG03FPAT.BFG260DP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPAU
       ;;
(FG03FPAU)
       m_CondExec 00,EQ,FG03FPAT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG261AP
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPAX PGM=SORT       ** ID=ABJ
# ***********************************
       JUMP_LABEL=FG03FPAX
       ;;
(FG03FPAX)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPAG.BFG261AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG03FPAX.BFG261BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,11,A,18,08,D,56,1,A),FORMAT=CH 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPAY
       ;;
(FG03FPAY)
       m_CondExec 00,EQ,FG03FPAX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG261
#  ------------
#  PREPARATION POUR EDITION DU TABLEAU DE RENUMERATION
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBA PGM=IKJEFT01   ** ID=ABO
# ***********************************
       JUMP_LABEL=FG03FPBA
       ;;
(FG03FPBA)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  DATE DU JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG03FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------  FICHIER ISSU DU TRI PRECEDANT
       m_FileAssign -d SHR -g +1 FFGJRN ${DATA}/PTEM.FG03FPAX.BFG261BP
# ------  FICHIER POUR CREATION ETAT DANS LA BASE EDITION
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG03FPBA.BFG261CP
# ------  EDITION DE CONTROLE
       m_FileAssign -d NEW,CATLG IFG261 ${DATA}/FG03FPA.FG03FPBA.IFG261
# 

       m_ProgramExec -b BFG261 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG03FPBB
       ;;
(FG03FPBB)
       m_CondExec 04,GE,FG03FPBA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER POUR EDITION A PARIS DANS DISPATCH
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBD PGM=SORT       ** ID=ABT
# ***********************************
       JUMP_LABEL=FG03FPBD
       ;;
(FG03FPBD)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPBA.BFG261CP
# ******* JOURNAL DE REMUNERATION
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG03FPA.FG03FPBD.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPBE
       ;;
(FG03FPBE)
       m_CondExec 00,EQ,FG03FPBD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG261CP POUR PGM BFG230
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBG PGM=SORT       ** ID=ABY
# ***********************************
       JUMP_LABEL=FG03FPBG
       ;;
(FG03FPBG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPBA.BFG261CP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG03FPBG.BFG261DP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPBH
       ;;
(FG03FPBH)
       m_CondExec 00,EQ,FG03FPBG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG262AP
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBJ PGM=SORT       ** ID=ACD
# ***********************************
       JUMP_LABEL=FG03FPBJ
       ;;
(FG03FPBJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPAG.BFG262AP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG03FPBJ.BFG262BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,17,A,24,08,D,62,1,A),FORMAT=CH 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPBK
       ;;
(FG03FPBK)
       m_CondExec 00,EQ,FG03FPBJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG262
#  ------------
#  PREPARATION POUR EDITION DU TABLEAU DE RENUMERATION
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBM PGM=IKJEFT01   ** ID=ACI
# ***********************************
       JUMP_LABEL=FG03FPBM
       ;;
(FG03FPBM)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  DATE DU JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG03FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------  FICHIER ISSU DU TRI PRECEDANT
       m_FileAssign -d SHR -g +1 FFGCOMP ${DATA}/PTEM.FG03FPBJ.BFG262BP
# ------  FICHIER POUR CREATION ETAT DANS LA BASE EDITION
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG03FPBM.BFG262CP
# ------  EDITION DE CONTROLE
       m_FileAssign -d NEW,CATLG IFG262 ${DATA}/FG03FPA.FG03FPBM.IFG262
# 

       m_ProgramExec -b BFG262 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG03FPBN
       ;;
(FG03FPBN)
       m_CondExec 04,GE,FG03FPBM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER POUR EDITION A PARIS DANS DISPATCH
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBQ PGM=SORT       ** ID=ACN
# ***********************************
       JUMP_LABEL=FG03FPBQ
       ;;
(FG03FPBQ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPBM.BFG262CP
# ******* RAPPROCHEMENT COMPTABLE
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG03FPA.FG03FPBQ.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPBR
       ;;
(FG03FPBR)
       m_CondExec 00,EQ,FG03FPBQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG262CP POUR PGM BFG230
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBT PGM=SORT       ** ID=ACS
# ***********************************
       JUMP_LABEL=FG03FPBT
       ;;
(FG03FPBT)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG03FPBM.BFG262CP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG03FPBT.BFG262DP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG03FPBU
       ;;
(FG03FPBU)
       m_CondExec 00,EQ,FG03FPBT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG230             ETAT IFG260
#  ------------             -----------
#  CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISEE
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPBX PGM=DFSRRC00   ** ID=ACX
# ***********************************
       JUMP_LABEL=FG03FPBX
       ;;
(FG03FPBX)
# ACX     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG260RC1),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG260RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ------  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG03FPAT.BFG260DP(+1),DISP=SHR
# ------  BASE IMPRESSION GENERALISEE
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG03FPR1)
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#  PGM : BFG230             ETAT IFG261
#  ------------             -----------
#  CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISEE
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPCA PGM=DFSRRC00   ** ID=ADC
# ***********************************
       JUMP_LABEL=FG03FPCA
       ;;
(FG03FPCA)
# ADC     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG261RC1),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG261RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ------  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG03FPBG.BFG261DP(+1),DISP=SHR
# ------  BASE IMPRESSION GENERALISEE
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG03FPR2)
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#  PGM : BFG230             ETAT IFG262
#  ------------             -----------
#  CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISEE
# ********************************************************************
# 
# ***********************************
# *   STEP FG03FPCD PGM=DFSRRC00   ** ID=ADH
# ***********************************
       JUMP_LABEL=FG03FPCD
       ;;
(FG03FPCD)
# ADH     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG262RC1),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG262RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ------  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG03FPBT.BFG262DP(+1),DISP=SHR
# ------  BASE IMPRESSION GENERALISEE
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG03FPR3)
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG03FPZA
       ;;
(FG03FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG03FPAG.BFG260AP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPAG.BFG261AP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPAG.BFG262AP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPAJ.BFG260BP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPAM.BFG260CP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPAT.BFG260DP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPAX.BFG261BP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPBA.BFG261CP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPBG.BFG261DP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPBJ.BFG262BP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPBM.BFG262CP.*"
   m_FileDelete "${DATA}/PTEM.FG03FPBT.BFG262DP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
