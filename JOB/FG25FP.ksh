#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG25FP.ksh                       --- VERSION DU 06/05/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG25F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 14.44.40 BY BURTECA
#    STANDARDS: P  JOBSET: FG25FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# ********************************************************************
#  BFG250 : CREATION DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIE
#                    EMISES ET RECUES PAR SOCIETE (JFG012)
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON THURSDAY  2015/08/20 AT 14.44.40 BY BURTECA
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG25FP
# *                           FREQ...: D
# *                           TITLE..: 'RECAP FACTFIS'
# *                           APPL...: REPPARIS
# *                           BACKOUT: JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# ********************************************************************
#  BFG250 : CREATION DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIE
#                    EMISES ET RECUES PAR SOCIETE (JFG012)
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG25FPAA PGM=IKJEFT01   ** ID=AAA
# ***********************************
       JUMP_LABEL=FG25FPAA
       ;;
(FG25FPAA)
# **************************************
#  DEPENDANCES POUR PLAN :             *
#    OBLIGATOIRE POUR LOGIQUE APPL     *
#    APRES JOB TETE DE NUIT            *
# **************************************
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX GROUPE
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES FACTURES INTER-SOCIETE
#    RSFG01   : NAME=RSFG01,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES
#    RSFG02   : NAME=RSFG02,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG02 /dev/null
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG25FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# 
# ******* FICHIER RECAPITULATIF DES FACTURES  A EDITER
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG25FPAA.BFG250AP
# 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG250 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG25FPAB
       ;;
(FG25FPAB)
       m_CondExec 04,GE,FG25FPAA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIETE
#                       DES FILIALES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG25FPAD PGM=SORT       ** ID=AAF
# ***********************************
       JUMP_LABEL=FG25FPAD
       ;;
(FG25FPAD)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG25FPAA.BFG250AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG25FPAD.BFG250BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG25FPAE
       ;;
(FG25FPAE)
       m_CondExec 00,EQ,FG25FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG25FPAG PGM=DFSRRC00   ** ID=AAK
# ***********************************
       JUMP_LABEL=FG25FPAG
       ;;
(FG25FPAG)
# AAK     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG250RC1),RSTRT=SAME
# MW DFSRESLB DD DSN=IMSVS.RESLIB,DISP=SHR
# MW IMSACB  DD DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG250RC1
# ******  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG25FPAD.BFG250BP(+1),DISP=SHR
# ******  BASE IMPRESSION GENERALISEE
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG25FPR1)
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#         TRI DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIETE
#                        POUR PARIS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG25FPAJ PGM=SORT       ** ID=AAP
# ***********************************
       JUMP_LABEL=FG25FPAJ
       ;;
(FG25FPAJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG25FPAA.BFG250AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG25FPA.FG25FPAJ.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG25FPAK
       ;;
(FG25FPAK)
       m_CondExec 00,EQ,FG25FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG25FPZA
       ;;
(FG25FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG25FPAA.BFG250AP.*"
   m_FileDelete "${DATA}/PTEM.FG25FPAD.BFG250BP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
