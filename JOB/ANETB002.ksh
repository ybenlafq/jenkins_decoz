#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ANETB002.ksh                       --- VERSION DU 06/05/2016 12:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j ANETB002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
       JUMP_LABEL=MISSING
       ;;
(MISSING)
       m_FileAssign -d NEW,CATLG,DELETE -r 80 GAS ${DATA}/IF02001.NETTING.GAS
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 DAFG01V ${DATA}/PXX0.F75.DAFG01V
       m_ProgramExec IEFBR14 
       JUMP_LABEL=INITSQL
       ;;
(INITSQL)
       m_OutputAssign -c "*" SYSPUNCH
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSREC00
       m_FileAssign -i SYSIN
INSERT INTO M907.RTGA01 (CTABLEG1, CTABLEG2, WTABLEG)
   VALUES ('FGCAL', '20160111', 'O012016ONN');
_end
       m_ExecSQL -f SYSIN -o SYSREC00
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
