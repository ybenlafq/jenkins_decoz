#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG13FP.ksh                       --- VERSION DU 06/05/2016 14:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG13F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/02 AT 09.37.40 BY BURTECA
#    STANDARDS: P  JOBSET: FG13FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# 
# ********************************************************************
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE
#     REPRISE : OUI
# ********************************************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON WEDNSDAY  2015/09/02 AT 09.37.40 BY BURTECA
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG13FP
# *                           FREQ...: M
# *                           TITLE..: 'EDIT FACT FIS'
# *                           APPL...: REPPARIS
# *                           BACKOUT: JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ********************************************************************
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE
#     REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAA PGM=DSNUTILB   ** ID=AAA
# ***********************************
#
#FG13FPAA
#FG13FPAA Delete by Metaware, DSNUTILB avec QUIESCE ou REPAIR
#FG13FPAA
#
       JUMP_LABEL=FG13FPAB
       ;;
(FG13FPAB)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#           PRINT DE LA VALEUR DU RBA
#           REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAD PGM=IEBGENER   ** ID=AAF
# ***********************************
       JUMP_LABEL=FG13FPAD
       ;;
(FG13FPAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ******* FICHIER CONTENANT LE NUMERO DE RBA
       m_FileAssign -d SHR -g +1 SYSUT1 ${DATA}/PXX0.RBA.QFG13FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPAE
       ;;
(FG13FPAE)
       m_CondExec 00,EQ,FG13FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BFG130 :EXTRACTION DES FACTURES DES FRAIS ANNEXES AUX MUTATIONS DE
#                                  MARCHANDISES DANS RTFG01
#          CREATION FIC FFGRMUT:   EDITION FACTURES DESTINEE A
#                                  L'IMPRESSION GENERALISEE
# 
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAG PGM=IKJEFT01   ** ID=AAK
# ***********************************
       JUMP_LABEL=FG13FPAG
       ;;
(FG13FPAG)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES CALCUL DES FRAIS ANNEXES DES FACTURES INTERSOCIETE
#    RSFG10   : NAME=RSFG10,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSFG10 /dev/null
# 
# ******* TABLE DES FACTURES INTER-SOCIETE
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
# -X-FG13FPR1 - IS UPDATED IN PLACE WITH MODE=U
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES COMMENTAIRES SUR FACTURES
#    RSFG06   : NAME=RSFG06,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG06 /dev/null
# ******* TABLE DETAILS D'UNE VENTILATION DE FACTURES INTERSOCIETES
#    RSFG07   : NAME=RSFG07,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG07 /dev/null
# 
# ******* CARTE PARAMETRE POUR REPRISE
       m_FileAssign -i FREPRISE

_end
       m_FileAssign -i FDCOMPTA

_end
       m_FileAssign -i FSOCCTA

_end
# 
# ******* DATE JOUR JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG13FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* DATE DU MOIS MMSSAA
# MW FMOIS  DD DSN=CORTEX4.P.PARAM.FG13FP.R$[RUN](FMOIS),DISP=SHR
       m_FileAssign -i FMOIS
012016
_end
# 
# ******* FICHIER DES FACTURES ANNEXES A EDITER
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFGRMUT ${DATA}/PTEM.FG13FPAG.BFG130AP
# ******* FICHIER DES ESCOMPTES
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FFGESCPT ${DATA}/PTEM.FG13FPAG.BFG245AP
# 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG130 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG13FPAH
       ;;
(FG13FPAH)
       m_CondExec 04,GE,FG13FPAG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES A EDITER
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAJ PGM=SORT       ** ID=AAP
# ***********************************
       JUMP_LABEL=FG13FPAJ
       ;;
(FG13FPAJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAG.BFG130AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG13FPAJ.BFG130BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(7,47,A),FORMAT=CH 
  SUM  FIELDS=(54,4,58,7,65,7,72,7,79,7),FORMAT=PD 
  RECORD F 100 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPAK
       ;;
(FG13FPAK)
       m_CondExec 00,EQ,FG13FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG240  : MISE EN FORME DU FICHIER D'EDITION DE FACTURE ANNEXES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAM PGM=IKJEFT01   ** ID=AAU
# ***********************************
       JUMP_LABEL=FG13FPAM
       ;;
(FG13FPAM)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# 
# ****** DATE : JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG13FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* DATE DU MOIS MMSSAA
# MW FMOIS  DD DSN=CORTEX4.P.PARAM.FG13FP.R$[RUN](FMOIS),DISP=SHR
       m_FileAssign -i FMOIS
012016
_end
# 
       m_FileAssign -d SHR -g +1 FFGRMUT ${DATA}/PTEM.FG13FPAJ.BFG130BP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG13FPAM.BFG240AP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDGA ${DATA}/PTEM.FG13FPAM.BFG240CP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG240 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG13FPAN
       ;;
(FG13FPAN)
       m_CondExec 04,GE,FG13FPAM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES FILIALES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAQ PGM=SORT       ** ID=AAZ
# ***********************************
       JUMP_LABEL=FG13FPAQ
       ;;
(FG13FPAQ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG13FPAQ.BFG240BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPAR
       ;;
(FG13FPAR)
       m_CondExec 00,EQ,FG13FPAQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAT PGM=DFSRRC00   ** ID=ABE
# ***********************************
       JUMP_LABEL=FG13FPAT
       ;;
(FG13FPAT)
# BE      IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG130RC1),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG130RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ******  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG13FPAQ.BFG240BP(+1),DISP=SHR
# ******  BASE IMPRESSION GENERALISEE
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
# IGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG13FPR2)
# IGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#         TRI DU FICHIER DES FACTURES DE PARIS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPAX PGM=SORT       ** ID=ABJ
# ***********************************
       JUMP_LABEL=FG13FPAX
       ;;
(FG13FPAX)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG13FPA.FG13FPAX.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPAY
       ;;
(FG13FPAY)
       m_CondExec 00,EQ,FG13FPAX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE PARIS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ******************************************************** SANS MODIF
# 
# ***********************************
# *   STEP FG13FPBA PGM=SORT       ** ID=ABO
# ***********************************
       JUMP_LABEL=FG13FPBA
       ;;
(FG13FPBA)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012P),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBB
       ;;
(FG13FPBB)
       m_CondExec 00,EQ,FG13FPBA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE DPM
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ****************************************************** NEW *********
# 
# ***********************************
# *   STEP FG13FPBD PGM=SORT       ** ID=ABT
# ***********************************
       JUMP_LABEL=FG13FPBD
       ;;
(FG13FPBD)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012D),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AD
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'991') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBE
       ;;
(FG13FPBE)
       m_CondExec 00,EQ,FG13FPBD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE DAL
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPBG PGM=SORT       ** ID=ABY
# ***********************************
       JUMP_LABEL=FG13FPBG
       ;;
(FG13FPBG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012M),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AM
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'989') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBH
       ;;
(FG13FPBH)
       m_CondExec 00,EQ,FG13FPBG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE LUXEMBOURG
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPBJ PGM=SORT       ** ID=ACD
# ***********************************
       JUMP_LABEL=FG13FPBJ
       ;;
(FG13FPBJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012X),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AX
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'908') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBK
       ;;
(FG13FPBK)
       m_CondExec 00,EQ,FG13FPBJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE MGI OUEST
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************* NEW ******
# 
# ***********************************
# *   STEP FG13FPBM PGM=SORT       ** ID=ACI
# ***********************************
       JUMP_LABEL=FG13FPBM
       ;;
(FG13FPBM)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012O),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AO
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'916') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBN
       ;;
(FG13FPBN)
       m_CondExec 00,EQ,FG13FPBM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE LYON
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************* NEW ******
# 
# ***********************************
# *   STEP FG13FPBQ PGM=SORT       ** ID=ACN
# ***********************************
       JUMP_LABEL=FG13FPBQ
       ;;
(FG13FPBQ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012Y),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AY
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'945') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBR
       ;;
(FG13FPBR)
       m_CondExec 00,EQ,FG13FPBQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE LILLE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************* NEW ******
# 
# ***********************************
# *   STEP FG13FPBT PGM=SORT       ** ID=ACS
# ***********************************
       JUMP_LABEL=FG13FPBT
       ;;
(FG13FPBT)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012L),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AL
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'961') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBU
       ;;
(FG13FPBU)
       m_CondExec 00,EQ,FG13FPBT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES FACTURES DE CAPROFEM
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************* NEW ******
# 
# ***********************************
# *   STEP FG13FPBX PGM=SORT       ** ID=ACX
# ***********************************
       JUMP_LABEL=FG13FPBX
       ;;
(FG13FPBX)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES
#  //SORTOUT  DD  SYSOUT=(9,JFG012K),LRECL=133,RECFM=FBA,SPIN=UNALLOC
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AK
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=((15,3,CH,EQ,C'997'),OR,(15,3,CH,EQ,C'801')) 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPBY
       ;;
(FG13FPBY)
       m_CondExec 00,EQ,FG13FPBX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES ESCOMPTES A EDITER
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCA PGM=SORT       ** ID=ADC
# ***********************************
       JUMP_LABEL=FG13FPCA
       ;;
(FG13FPCA)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPAG.BFG245AP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG13FPCA.BFG245BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(7,41,A),FORMAT=CH 
  RECORD F 70 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPCB
       ;;
(FG13FPCB)
       m_CondExec 00,EQ,FG13FPCA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG245  : MISE EN FORME DU FICHIER D'EDITION DES ESCOMPTES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCD PGM=IKJEFT01   ** ID=ADH
# ***********************************
       JUMP_LABEL=FG13FPCD
       ;;
(FG13FPCD)
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)
#         M907.RTGA01
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10FP /dev/null
# 
# ****** DATE : JJMMSSAA
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG13FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ******* DATE DU MOIS MMSSAA
# MW FMOIS  DD DSN=CORTEX4.P.PARAM.FG13FP.R$[RUN](FMOIS),DISP=SHR
       m_FileAssign -i FMOIS
012016
_end
# 
       m_FileAssign -d SHR -g +1 FFGESCPT ${DATA}/PTEM.FG13FPCA.BFG245BP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM.FG13FPCD.BFG245CP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT

       m_ProgramExec -b BFG245 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG13FPCE
       ;;
(FG13FPCE)
       m_CondExec 04,GE,FG13FPCD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#         TRI DU FICHIER DES ESCOMPTES FILIALES
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCG PGM=SORT       ** ID=ADM
# ***********************************
       JUMP_LABEL=FG13FPCG
       ;;
(FG13FPCG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPCD.BFG245CP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG13FPCG.BFG245DP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  OMIT COND=(15,3,CH,EQ,C'907') 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPCH
       ;;
(FG13FPCH)
       m_CondExec 00,EQ,FG13FPCG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCJ PGM=DFSRRC00   ** ID=ADR
# ***********************************
       JUMP_LABEL=FG13FPCJ
       ;;
(FG13FPCJ)
# DR      IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001
#               LOG=(YES,FG130RC3),RSTRT=SAME
#         DD DSN=IMSVS.RESLIB,
#         DISP=SHR
# DFSRESLB DD DSN=IMSVS.RESLIB,
#          DISP=SHR
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS.F99.FG130RC3
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ******  FICHIER DES FACTURES A INSERER
# MW FFGEDG   DD  DSN=PTEM.FG13FPCG.BFG245DP(+1),DISP=SHR
# ******  BASE IMPRESSION GENERALISEE
# IGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG13FPR4)
# IGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO
# MW DIGVP0  DD DSN=PPA0.F07.PDIGVP0P,DISP=SHR
# MW DIGVIP  DD DSN=PPA0.F07.PDIGVI0P,DISP=SHR
       m_FileAssign -i DFSVSAMP
32768,30
28672,30
24576,6
20480,6
16384,10
12288,10
8192,10
4096,48
2048,20
1024,20
512,32
_end
# MW TRPDRVR DD DSN=SYS2.DL2420.LOAD.PDSE.ELIM,DISP=SHR
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR
       m_FileAssign -i DDITV02
RMAC,SYS1,DSNMIN10,,R,,,TPIGG,BFG230
_end
       m_ProgramExec IEFBR14 "DBB,TRPMTV01,GGD070,"
# ********************************************************************
#         TRI DU FICHIER DES ESCOMPTES DE PARIS
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCM PGM=SORT       ** ID=ADW
# ***********************************
       JUMP_LABEL=FG13FPCM
       ;;
(FG13FPCM)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG13FPCD.BFG245CP
# ******* FICHIER D'EDITION DES FACTURES
       m_FileAssign -d NEW,CATLG SORTOUT ${DATA}/FG13FPA.FG13FPCM.SORTOUT
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,42,A),FORMAT=CH 
  INCLUDE COND=(15,3,CH,EQ,C'907') 
  OUTREC FIELDS=(43,133) 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG13FPCN
       ;;
(FG13FPCN)
       m_CondExec 00,EQ,FG13FPCM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012P FACTURE NETTING POUR PARIS
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCQ PGM=IEBGENER   ** ID=AEB
# ***********************************
       JUMP_LABEL=FG13FPCQ
       ;;
(FG13FPCQ)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AP
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPCQ.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPCR
       ;;
(FG13FPCR)
       m_CondExec 00,EQ,FG13FPCQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012D FACTURE NETTING POUR MARSEILLE
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCT PGM=IEBGENER   ** ID=AEG
# ***********************************
       JUMP_LABEL=FG13FPCT
       ;;
(FG13FPCT)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AD
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPCT.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPCU
       ;;
(FG13FPCU)
       m_CondExec 00,EQ,FG13FPCT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012M FACTURE NETTING POUR METZ
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPCX PGM=IEBGENER   ** ID=AEL
# ***********************************
       JUMP_LABEL=FG13FPCX
       ;;
(FG13FPCX)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AM
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPCX.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPCY
       ;;
(FG13FPCY)
       m_CondExec 00,EQ,FG13FPCX 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012X FACTURE NETTING POUR LUXEMBOURG
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPDA PGM=IEBGENER   ** ID=AEQ
# ***********************************
       JUMP_LABEL=FG13FPDA
       ;;
(FG13FPDA)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AX
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPDA.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPDB
       ;;
(FG13FPDB)
       m_CondExec 00,EQ,FG13FPDA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012O FACTURE NETTING POUR MGI OUEST
# ********************************************************************
#  AEV      STEP  PGM=IEBGENER,LANG=UTIL
# 
#  SYSUT1   FILE  NAME=BFG241AO,MODE=I
#  SYSUT2   REPORT SYSOUT=(9,JFG012O),FREE=CLOSE
#  //SYSIN    DD DUMMY
# ********************************************************************
#  EDITION JFG012Y FACTURE NETTING POUR LYON
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPDD PGM=IEBGENER   ** ID=AEV
# ***********************************
       JUMP_LABEL=FG13FPDD
       ;;
(FG13FPDD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AY
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPDD.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPDE
       ;;
(FG13FPDE)
       m_CondExec 00,EQ,FG13FPDD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012L FACTURE NETTING POUR LILLE
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPDG PGM=IEBGENER   ** ID=AFA
# ***********************************
       JUMP_LABEL=FG13FPDG
       ;;
(FG13FPDG)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AL
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPDG.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPDH
       ;;
(FG13FPDH)
       m_CondExec 00,EQ,FG13FPDG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  EDITION JFG012K FACTURE NETTING POUR CAPROFEM
# ********************************************************************
# 
# ***********************************
# *   STEP FG13FPDJ PGM=IEBGENER   ** ID=AFF
# ***********************************
       JUMP_LABEL=FG13FPDJ
       ;;
(FG13FPDJ)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# 
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AK
       m_FileAssign -d NEW,CATLG SYSUT2 ${DATA}/FG13FPA.FG13FPDJ.SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***
# ************************************************
       JUMP_LABEL=FG13FPDK
       ;;
(FG13FPDK)
       m_CondExec 00,EQ,FG13FPDJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG13FPZA
       ;;
(FG13FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG13FPAG.BFG130AP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPAG.BFG245AP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPAJ.BFG130BP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPAM.BFG240AP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPAM.BFG240CP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPAQ.BFG240BP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPCA.BFG245BP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPCD.BFG245CP.*"
   m_FileDelete "${DATA}/PTEM.FG13FPCG.BFG245DP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
