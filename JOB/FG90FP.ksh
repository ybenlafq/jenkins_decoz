#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG90FP.ksh                       --- VERSION DU 06/05/2016 12:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG90F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/18 AT 17.23.22 BY BURTECA
#    STANDARDS: P  JOBSET: FG90FP
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)
# --------------------------------------------------------------------
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL
# *************************************
# *  DELETE DU FICHIER CUMUL DU MOIS
# *************************************
# --------------------------------------------------------------------
# 
# 
# ********************************************************************
# *    GENERATED ON SATURDAY  2015/04/18 AT 17.23.22 BY BURTECA
# *    ENVIRONMENT: IPO1      STD....: P
# *    JOBSET INFORMATION:    NAME...: FG90FP
# *                           FREQ...: M
# *                           TITLE..: 'PURGE NETTING'
# *                           APPL...: REPPARIS
# ********************************************************************
# 
# ***********************************
# *   STEP YES      PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=YES
       ;;
(YES)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP NO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=NO
       ;;
(NO)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP FROM     PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=FROM
       ;;
(FROM)
       #Untranslated program Utility  name CZX3PSRC
# 
# ***********************************
# *   STEP TO       PGM=CZX3PSRC   **
# ***********************************
       JUMP_LABEL=TO
       ;;
(TO)
       #Untranslated program Utility  name CZX3PSRC
# *************************************
# *  DELETE DU FICHIER CUMUL DU MOIS
# *************************************
# 
# ***********************************
# *   STEP FG90FPAA PGM=IDCAMS     ** ID=AAA
# ***********************************
       JUMP_LABEL=FG90FPAA
       ;;
(FG90FPAA)
       m_OutputAssign -c "*" SYSPRINT
# **************************************
#  DEPENDANCES POUR PLAN :             *
#    OBLIGATOIRE POUR LOGIQUE APPL     *
#    APRES JOB TETE DE NUIT            *
# **************************************
       m_FileAssign -d SHR IN1 /dev/null
   m_FileDelete ${DATA}/PXX0.F07.BFG950BP
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***
# ************************************************
       JUMP_LABEL=FG90FPAB
       ;;
(FG90FPAB)
       m_CondExec 16,NE,FG90FPAA 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG900
#  ------------
#  PURGE DES TABLES DU NETTING ON GARDE 3 MOIS EN LIGNE
#  CREATION DE 3 FICHIERS REPRIS DANS LE BFG950
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAD PGM=IKJEFT01   ** ID=AAF
# ***********************************
       JUMP_LABEL=FG90FPAD
       ;;
(FG90FPAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLES EN LECTURE
#    RTGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
# ------- TABLES EN M.A.J
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG01 /dev/null
#    RSFG05   : NAME=RSFG05,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG05 /dev/null
#    RSFG06   : NAME=RSFG06,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG06 /dev/null
#    RSFG07   : NAME=RSFG07,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSFG07 /dev/null
# ------- PARAMETRE DATE
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG90FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end
# ------- FICHIERS REPRIS DANS LE BFG950
       m_FileAssign -d NEW,CATLG,DELETE -r 239 -t LSEQ -g +1 FFG01P ${DATA}/PNCGP.F07.BFG901AP
       m_FileAssign -d NEW,CATLG,DELETE -r 118 -t LSEQ -g +1 FFG06P ${DATA}/PNCGP.F07.BFG906AP
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FFG07P ${DATA}/PNCGP.F07.BFG907AP

       m_ProgramExec -b BFG900 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG90FPAE
       ;;
(FG90FPAE)
       m_CondExec 04,GE,FG90FPAD 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG901AP POUR LE PGM BFG950
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAG PGM=SORT       ** ID=AAK
# ***********************************
       JUMP_LABEL=FG90FPAG
       ;;
(FG90FPAG)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PNCGP.F07.BFG901AP
       m_FileAssign -d NEW,CATLG,DELETE -r 239 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG90FPAG.BFG901BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,15,A),FORMAT=CH 
  RECORD F 239 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG90FPAH
       ;;
(FG90FPAH)
       m_CondExec 00,EQ,FG90FPAG 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG906AP POUR LE PGM BFG950
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAJ PGM=SORT       ** ID=AAP
# ***********************************
       JUMP_LABEL=FG90FPAJ
       ;;
(FG90FPAJ)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PNCGP.F07.BFG906AP
       m_FileAssign -d NEW,CATLG,DELETE -r 118 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG90FPAJ.BFG906BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,17,A),FORMAT=CH 
  RECORD F 118 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG90FPAK
       ;;
(FG90FPAK)
       m_CondExec 00,EQ,FG90FPAJ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG901AP POUR LE PGM BFG950
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAM PGM=SORT       ** ID=AAU
# ***********************************
       JUMP_LABEL=FG90FPAM
       ;;
(FG90FPAM)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PNCGP.F07.BFG907AP
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM.FG90FPAM.BFG907BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(1,15,A),FORMAT=CH 
  RECORD F 81 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG90FPAN
       ;;
(FG90FPAN)
       m_CondExec 00,EQ,FG90FPAM 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BFG950
#  ------------
#  CREATION D'UN FICHIER POUR MICROFICHE ORSID
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAQ PGM=IKJEFT01   ** ID=AAZ
# ***********************************
       JUMP_LABEL=FG90FPAQ
       ;;
(FG90FPAQ)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLES EN LECTURE
#    RTGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA10 /dev/null
# ------- FICHIERS ISSU DU BFG900
       m_FileAssign -d SHR -g +1 FFG01P ${DATA}/PTEM.FG90FPAG.BFG901BP
       m_FileAssign -d SHR -g +1 FFG06P ${DATA}/PTEM.FG90FPAJ.BFG906BP
       m_FileAssign -d SHR -g +1 FFG07P ${DATA}/PTEM.FG90FPAM.BFG907BP
# ------- FICHIER POUR MICROFICHE
       m_FileAssign -d NEW,CATLG,DELETE -r 179 -t LSEQ -g +1 FFGFACT ${DATA}/PTEM.FG90FPAQ.BFG950AP
# ------- PARAMETRE DATE
# MW FDATE  DD DSN=CORTEX4.P.PARAM.FG90FP.R$[RUN](FDATE),DISP=SHR
       m_FileAssign -i FDATE
11012016
_end

       m_ProgramExec -b BFG950 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************
       JUMP_LABEL=FG90FPAR
       ;;
(FG90FPAR)
       m_CondExec 04,GE,FG90FPAQ 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI DU FICHIER BFG950AP POUR MICROFICHE ORSID
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAT PGM=SORT       ** ID=ABE
# ***********************************
       JUMP_LABEL=FG90FPAT
       ;;
(FG90FPAT)
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +1 SORTIN ${DATA}/PTEM.FG90FPAQ.BFG950AP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.F07.BFG950BP
       m_FileAssign -i SYSIN
  SORT-EBCDIC 
  SORT FIELDS=(14,33,CH,A,1,5,PD,A) 
  OUTREC FIELDS=(47,133) 
  RECORD F 133 
_end
       m_FileSort -s SYSIN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       JUMP_LABEL=FG90FPAU
       ;;
(FG90FPAU)
       m_CondExec 00,EQ,FG90FPAT 1,EQ,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSDBOUT /dev/null
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  ZIP DES FICHERS
# ********************************************************************
# ABJ      STEP  PGM=PKZIP
# SYSPRINT REPORT SYSOUT=*
# SYSUDUMP REPORT SYSOUT=*
# SYSABEND REPORT SYSOUT=*
# SYSIN    DATA  *
#  -ECHO
#  -ACTION(ADD)
#  -COMPRESSION_LEVEL(NORMAL)
#  -TEXT
#  -ARCHIVE(":FGFACP"")
#  -ARCHUNIT(SEM350)
#  -ZIPPED_DSN(":BFG950BP"",FGFACP.TXT)
#  ":BFG950BP""
#          DATAEND
# ********************************************************************
#  ZIP AVEC JAVAZIP
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPAX PGM=JVMLDM76   ** ID=ABJ
# ***********************************
# ****** FICHIER EN SORTIE DE ZIP
# *
# ***********************************
#  TRANSFERT VERS XFB-GATEWAY
# ***********************************
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT
# CFTIN    DATA  *
# SEND PART=XFBPRO,
#      IDF=CDROMZIP,
#      FNAME=":FGFACP"",
#      NFNAME=PURNETT.ZIP
#          DATAEND
# ********************************************************************
#  TRANSFERT PAR CFT BL MICROLIST
# ********************************************************************
# ABT      STEP  PGM=IEFBR14,PATTERN=CFT
# CFTIN    DATA  CLASS=VAR,PARMS=FG90FP
# ********************************************************************
#  ENVOI FTP SUR LA GATEWAY DU FTFG90FP
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPBA PGM=FTP        ** ID=ABO
# ***********************************
# 
# *********************************************************
# ** PREPARATION DU SEND FTP DU BL MICROFICHE          ****
# *********************************************************
# 
# ***********************************
# *   STEP FG90FPBD PGM=EZACFSM1   ** ID=ABT
# ***********************************
#       JUMP_LABEL=FG90FPBD
#       ;;
#(FG90FPBD)
#       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM.FG90FPBD.FTFG90FP
#       m_FileAssign -i SYSIN
#PUT 'SYS3.BLIAISON.CARD(PURNETT)' PURNETT_DU${LMON}${LDAY}${LYR4}
#QUIT
#_end
#       m_ProgramExec EZACFSM1 
# ********************************************************************
#  ENVOI FTP SUR LA GATEWAY DU BORDEREAU DE LAISON PURNETT_DU
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP FG90FPBG PGM=FTP        ** ID=ABY
# ***********************************
# 
# ********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=FG90FPZA
       ;;
(FG90FPZA)
       m_OutputAssign -c "*" SYSPRINT
   m_FileDelete "${DATA}/PTEM.FG90FPAG.BFG901BP.*"
   m_FileDelete "${DATA}/PTEM.FG90FPAJ.BFG906BP.*"
   m_FileDelete "${DATA}/PTEM.FG90FPAM.BFG907BP.*"
   m_FileDelete "${DATA}/PTEM.FG90FPAQ.BFG950AP.*"
   m_FileDelete "${DATA}/PTEM.FG90FPBD.FTFG90FP.*"
   m_RcSet 0
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
# *********************************************************************
# --------------------------------------------------------------------
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
