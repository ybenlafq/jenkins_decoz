.PHONY: all clean target_c clean_c target_TFG51_pco clean_TFG51_pco target_TFG50_pco clean_TFG50_pco target_TFG29_pco clean_TFG29_pco target_TFG28_pco clean_TFG28_pco target_TFG27_pco clean_TFG27_pco target_TFG25_pco clean_TFG25_pco target_TFG24_pco clean_TFG24_pco target_TFG23_pco clean_TFG23_pco target_TFG22_pco clean_TFG22_pco target_TFG21_cbl clean_TFG21_cbl target_TFG20_pco clean_TFG20_pco target_TFG14_pco clean_TFG14_pco target_TFG13_pco clean_TFG13_pco target_TFG12_pco clean_TFG12_pco target_TFG11_pco clean_TFG11_pco target_TFG10_pco clean_TFG10_pco target_TETDATC_cbl clean_TETDATC_cbl target_MFG30_pco clean_MFG30_pco target_MFFNUM_cbl clean_MFFNUM_cbl target__settings clean__settings target__launches clean__launches 

all: target_TFG51_pco target_TFG50_pco target_TFG29_pco target_TFG28_pco target_TFG27_pco target_TFG25_pco target_TFG24_pco target_TFG23_pco target_TFG22_pco target_TFG21_cbl target_TFG20_pco target_TFG14_pco target_TFG13_pco target_TFG12_pco target_TFG11_pco target_TFG10_pco target_TETDATC_cbl target_MFG30_pco target_MFFNUM_cbl 

target_TFG51_pco: ../../bin/TFG51.so

-include .cobolit.tmp.133.54.150.220/TFG51-31cc04aca28221edbf5baf544d3c095e87a73bbd.d

../../bin/TFG51.so: TFG51.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG51.pco'
	cobc TFG51.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG51.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG51.so" -MT "../../bin/TFG51.so" -MF .cobolit.tmp.133.54.150.220/TFG51-31cc04aca28221edbf5baf544d3c095e87a73bbd.d

clean_TFG51_pco: 
	rm -f "../../bin/TFG51.so" .cobolit.tmp.133.54.150.220/TFG51-31cc04aca28221edbf5baf544d3c095e87a73bbd.d

target_TFG50_pco: ../../bin/TFG50.so

-include .cobolit.tmp.133.54.150.220/TFG50-750f5c1d704cfcd66998154e399aad362087d550.d

../../bin/TFG50.so: TFG50.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG50.pco'
	cobc TFG50.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG50.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG50.so" -MT "../../bin/TFG50.so" -MF .cobolit.tmp.133.54.150.220/TFG50-750f5c1d704cfcd66998154e399aad362087d550.d

clean_TFG50_pco: 
	rm -f "../../bin/TFG50.so" .cobolit.tmp.133.54.150.220/TFG50-750f5c1d704cfcd66998154e399aad362087d550.d

target_TFG29_pco: ../../bin/TFG29.so

-include .cobolit.tmp.133.54.150.220/TFG29-1cdc1cc00176f71fbd02a9b5c7a39f59506c110b.d

../../bin/TFG29.so: TFG29.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG29.pco'
	cobc TFG29.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG29.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG29.so" -MT "../../bin/TFG29.so" -MF .cobolit.tmp.133.54.150.220/TFG29-1cdc1cc00176f71fbd02a9b5c7a39f59506c110b.d

clean_TFG29_pco: 
	rm -f "../../bin/TFG29.so" .cobolit.tmp.133.54.150.220/TFG29-1cdc1cc00176f71fbd02a9b5c7a39f59506c110b.d

target_TFG28_pco: ../../bin/TFG28.so

-include .cobolit.tmp.133.54.150.220/TFG28-d65812d6ffa292fc63b897258d3cdb447c946b56.d

../../bin/TFG28.so: TFG28.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG28.pco'
	cobc TFG28.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG28.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG28.so" -MT "../../bin/TFG28.so" -MF .cobolit.tmp.133.54.150.220/TFG28-d65812d6ffa292fc63b897258d3cdb447c946b56.d

clean_TFG28_pco: 
	rm -f "../../bin/TFG28.so" .cobolit.tmp.133.54.150.220/TFG28-d65812d6ffa292fc63b897258d3cdb447c946b56.d

target_TFG27_pco: ../../bin/TFG27.so

-include .cobolit.tmp.133.54.150.220/TFG27-94ef6bb1cf538e11b48fa3801f57ced784ba368e.d

../../bin/TFG27.so: TFG27.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG27.pco'
	cobc TFG27.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG27.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG27.so" -MT "../../bin/TFG27.so" -MF .cobolit.tmp.133.54.150.220/TFG27-94ef6bb1cf538e11b48fa3801f57ced784ba368e.d

clean_TFG27_pco: 
	rm -f "../../bin/TFG27.so" .cobolit.tmp.133.54.150.220/TFG27-94ef6bb1cf538e11b48fa3801f57ced784ba368e.d

target_TFG25_pco: ../../bin/TFG25.so

-include .cobolit.tmp.133.54.150.220/TFG25-06da431c904c1094818daf327f9139026e3b8435.d

../../bin/TFG25.so: TFG25.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG25.pco'
	cobc TFG25.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG25.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG25.so" -MT "../../bin/TFG25.so" -MF .cobolit.tmp.133.54.150.220/TFG25-06da431c904c1094818daf327f9139026e3b8435.d

clean_TFG25_pco: 
	rm -f "../../bin/TFG25.so" .cobolit.tmp.133.54.150.220/TFG25-06da431c904c1094818daf327f9139026e3b8435.d

target_TFG24_pco: ../../bin/TFG24.so

-include .cobolit.tmp.133.54.150.220/TFG24-7cd49f70224b8873c13aa5771b56752d6181a502.d

../../bin/TFG24.so: TFG24.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG24.pco'
	cobc TFG24.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG24.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG24.so" -MT "../../bin/TFG24.so" -MF .cobolit.tmp.133.54.150.220/TFG24-7cd49f70224b8873c13aa5771b56752d6181a502.d

clean_TFG24_pco: 
	rm -f "../../bin/TFG24.so" .cobolit.tmp.133.54.150.220/TFG24-7cd49f70224b8873c13aa5771b56752d6181a502.d

target_TFG23_pco: ../../bin/TFG23.so

-include .cobolit.tmp.133.54.150.220/TFG23-1e922820d5aa05fedd514c12d69137df1fb4ecee.d

../../bin/TFG23.so: TFG23.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG23.pco'
	cobc TFG23.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG23.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG23.so" -MT "../../bin/TFG23.so" -MF .cobolit.tmp.133.54.150.220/TFG23-1e922820d5aa05fedd514c12d69137df1fb4ecee.d

clean_TFG23_pco: 
	rm -f "../../bin/TFG23.so" .cobolit.tmp.133.54.150.220/TFG23-1e922820d5aa05fedd514c12d69137df1fb4ecee.d

target_TFG22_pco: ../../bin/TFG22.so

-include .cobolit.tmp.133.54.150.220/TFG22-db4b639dcc81959d38ebd3a2739fba4b75cda2e1.d

../../bin/TFG22.so: TFG22.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG22.pco'
	cobc TFG22.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG22.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG22.so" -MT "../../bin/TFG22.so" -MF .cobolit.tmp.133.54.150.220/TFG22-db4b639dcc81959d38ebd3a2739fba4b75cda2e1.d

clean_TFG22_pco: 
	rm -f "../../bin/TFG22.so" .cobolit.tmp.133.54.150.220/TFG22-db4b639dcc81959d38ebd3a2739fba4b75cda2e1.d

target_TFG21_cbl: ../../bin/TFG21.so

-include .cobolit.tmp.133.54.150.220/TFG21-d9441765d6dec5561823eb368671d258033e8507.d

../../bin/TFG21.so: TFG21.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG21.cbl'
	cobc TFG21.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG21.cbl" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG21.so" -MT "../../bin/TFG21.so" -MF .cobolit.tmp.133.54.150.220/TFG21-d9441765d6dec5561823eb368671d258033e8507.d

clean_TFG21_cbl: 
	rm -f "../../bin/TFG21.so" .cobolit.tmp.133.54.150.220/TFG21-d9441765d6dec5561823eb368671d258033e8507.d

target_TFG20_pco: ../../bin/TFG20.so

-include .cobolit.tmp.133.54.150.220/TFG20-d065ff5c49eced4fef89fc9ea1567c1b5654b627.d

../../bin/TFG20.so: TFG20.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG20.pco'
	cobc TFG20.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG20.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG20.so" -MT "../../bin/TFG20.so" -MF .cobolit.tmp.133.54.150.220/TFG20-d065ff5c49eced4fef89fc9ea1567c1b5654b627.d

clean_TFG20_pco: 
	rm -f "../../bin/TFG20.so" .cobolit.tmp.133.54.150.220/TFG20-d065ff5c49eced4fef89fc9ea1567c1b5654b627.d

target_TFG14_pco: ../../bin/TFG14.so

-include .cobolit.tmp.133.54.150.220/TFG14-083ca26ad1030896f759d3a697ae2dfb056010b4.d

../../bin/TFG14.so: TFG14.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG14.pco'
	cobc TFG14.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG14.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG14.so" -MT "../../bin/TFG14.so" -MF .cobolit.tmp.133.54.150.220/TFG14-083ca26ad1030896f759d3a697ae2dfb056010b4.d

clean_TFG14_pco: 
	rm -f "../../bin/TFG14.so" .cobolit.tmp.133.54.150.220/TFG14-083ca26ad1030896f759d3a697ae2dfb056010b4.d

target_TFG13_pco: ../../bin/TFG13.so

-include .cobolit.tmp.133.54.150.220/TFG13-6f359c090e6e03ab8f9b562d883a7633553129d6.d

../../bin/TFG13.so: TFG13.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG13.pco'
	cobc TFG13.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG13.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG13.so" -MT "../../bin/TFG13.so" -MF .cobolit.tmp.133.54.150.220/TFG13-6f359c090e6e03ab8f9b562d883a7633553129d6.d

clean_TFG13_pco: 
	rm -f "../../bin/TFG13.so" .cobolit.tmp.133.54.150.220/TFG13-6f359c090e6e03ab8f9b562d883a7633553129d6.d

target_TFG12_pco: ../../bin/TFG12.so

-include .cobolit.tmp.133.54.150.220/TFG12-feb9f0265e4f43249b847fefb8d190e137df45fb.d

../../bin/TFG12.so: TFG12.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG12.pco'
	cobc TFG12.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG12.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG12.so" -MT "../../bin/TFG12.so" -MF .cobolit.tmp.133.54.150.220/TFG12-feb9f0265e4f43249b847fefb8d190e137df45fb.d

clean_TFG12_pco: 
	rm -f "../../bin/TFG12.so" .cobolit.tmp.133.54.150.220/TFG12-feb9f0265e4f43249b847fefb8d190e137df45fb.d

target_TFG11_pco: ../../bin/TFG11.so

-include .cobolit.tmp.133.54.150.220/TFG11-d9631f4de3df8207ef60339d4d61ce00a6e18a60.d

../../bin/TFG11.so: TFG11.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG11.pco'
	cobc TFG11.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG11.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG11.so" -MT "../../bin/TFG11.so" -MF .cobolit.tmp.133.54.150.220/TFG11-d9631f4de3df8207ef60339d4d61ce00a6e18a60.d

clean_TFG11_pco: 
	rm -f "../../bin/TFG11.so" .cobolit.tmp.133.54.150.220/TFG11-d9631f4de3df8207ef60339d4d61ce00a6e18a60.d

target_TFG10_pco: ../../bin/TFG10.so

-include .cobolit.tmp.133.54.150.220/TFG10-2dd399f1818579549b7410887eeb4cab99e879b0.d

../../bin/TFG10.so: TFG10.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TFG10.pco'
	cobc TFG10.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TFG10.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TFG10.so" -MT "../../bin/TFG10.so" -MF .cobolit.tmp.133.54.150.220/TFG10-2dd399f1818579549b7410887eeb4cab99e879b0.d

clean_TFG10_pco: 
	rm -f "../../bin/TFG10.so" .cobolit.tmp.133.54.150.220/TFG10-2dd399f1818579549b7410887eeb4cab99e879b0.d

target_TETDATC_cbl: ../../bin/TETDATC.so

-include .cobolit.tmp.133.54.150.220/TETDATC-9f5c9dc640ab43e5143f8bdd992196da173e9f75.d

../../bin/TETDATC.so: TETDATC.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/TETDATC.cbl'
	cobc TETDATC.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh TETDATC.cbl" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/TETDATC.so" -MT "../../bin/TETDATC.so" -MF .cobolit.tmp.133.54.150.220/TETDATC-9f5c9dc640ab43e5143f8bdd992196da173e9f75.d

clean_TETDATC_cbl: 
	rm -f "../../bin/TETDATC.so" .cobolit.tmp.133.54.150.220/TETDATC-9f5c9dc640ab43e5143f8bdd992196da173e9f75.d

target_MFG30_pco: ../../bin/MFG30.so

-include .cobolit.tmp.133.54.150.220/MFG30-b4a1e0ccee912a251ba011ba146ef103c790db60.d

../../bin/MFG30.so: MFG30.pco
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/MFG30.pco'
	cobc MFG30.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh MFG30.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/MFG30.so" -MT "../../bin/MFG30.so" -MF .cobolit.tmp.133.54.150.220/MFG30-b4a1e0ccee912a251ba011ba146ef103c790db60.d

clean_MFG30_pco: 
	rm -f "../../bin/MFG30.so" .cobolit.tmp.133.54.150.220/MFG30-b4a1e0ccee912a251ba011ba146ef103c790db60.d

target_MFFNUM_cbl: ../../bin/MFFNUM.so

-include .cobolit.tmp.133.54.150.220/MFFNUM-2313a5c5f9bde8e3aade8d1c44e37468870ee37e.d

../../bin/MFFNUM.so: MFFNUM.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/CICS/MFFNUM.cbl'
	cobc MFFNUM.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -ext cbl -Wall -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citproall.sh MFFNUM.cbl" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/cics/lib" -L "/opt/ibm/db2/V10.5/lib32" -o "../../bin/MFFNUM.so" -MT "../../bin/MFFNUM.so" -MF .cobolit.tmp.133.54.150.220/MFFNUM-2313a5c5f9bde8e3aade8d1c44e37468870ee37e.d

clean_MFFNUM_cbl: 
	rm -f "../../bin/MFFNUM.so" .cobolit.tmp.133.54.150.220/MFFNUM-2313a5c5f9bde8e3aade8d1c44e37468870ee37e.d



clean: clean_TFG51_pco clean_TFG50_pco clean_TFG29_pco clean_TFG28_pco clean_TFG27_pco clean_TFG25_pco clean_TFG24_pco clean_TFG23_pco clean_TFG22_pco clean_TFG21_cbl clean_TFG20_pco clean_TFG14_pco clean_TFG13_pco clean_TFG12_pco clean_TFG11_pco clean_TFG10_pco clean_TETDATC_cbl clean_MFG30_pco clean_MFFNUM_cbl 

target_c: 
target__settings: 
target__launches: 


clean_c: 
clean__settings: 
clean__launches: 

