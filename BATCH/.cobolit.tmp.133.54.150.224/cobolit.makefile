.PHONY: all clean target_c clean_c target_ZABAUDIT_cbl clean_ZABAUDIT_cbl target_HELLO_cbl clean_HELLO_cbl target_BFX901_pco clean_BFX901_pco target_BFTPCA_pco clean_BFTPCA_pco target_BFGDOC_cbl clean_BFGDOC_cbl target_BFG950_pco clean_BFG950_pco target_BFG910_pco clean_BFG910_pco target_BFG900_pco clean_BFG900_pco target_BFG262_pco clean_BFG262_pco target_BFG261_pco clean_BFG261_pco target_BFG260_pco clean_BFG260_pco target_BFG250_pco clean_BFG250_pco target_BFG245_pco clean_BFG245_pco target_BFG240_pco clean_BFG240_pco target_BFG220_pco clean_BFG220_pco target_BFG215_pco clean_BFG215_pco target_BFG211_pco clean_BFG211_pco target_BFG210_cbl clean_BFG210_cbl target_BFG200_pco clean_BFG200_pco target_BFG180_pco clean_BFG180_pco target_BFG150_pco clean_BFG150_pco target_BFG140_pco clean_BFG140_pco target_BFG130_pco clean_BFG130_pco target_BFG120_pco clean_BFG120_pco target_BFG115_pco clean_BFG115_pco target_BFG110_pco clean_BFG110_pco target_BFG100_pco clean_BFG100_pco target_BFG002A_pco clean_BFG002A_pco target_BFG002_pco clean_BFG002_pco target_BFG001N_pco clean_BFG001N_pco target_BFG000A_pco clean_BFG000A_pco target_BFG000_pco clean_BFG000_pco target_BETDATC_cbl clean_BETDATC_cbl target__settings clean__settings target__launches clean__launches 

all: target_ZABAUDIT_cbl target_HELLO_cbl target_BFX901_pco target_BFTPCA_pco target_BFGDOC_cbl target_BFG950_pco target_BFG910_pco target_BFG900_pco target_BFG262_pco target_BFG261_pco target_BFG260_pco target_BFG250_pco target_BFG245_pco target_BFG240_pco target_BFG220_pco target_BFG215_pco target_BFG211_pco target_BFG210_cbl target_BFG200_pco target_BFG180_pco target_BFG150_pco target_BFG140_pco target_BFG130_pco target_BFG120_pco target_BFG115_pco target_BFG110_pco target_BFG100_pco target_BFG002A_pco target_BFG002_pco target_BFG001N_pco target_BFG000A_pco target_BFG000_pco target_BETDATC_cbl 

target_ZABAUDIT_cbl: ../../bin/ZABAUDIT.so

-include .cobolit.tmp.133.54.150.224/ZABAUDIT-3389e7de10a53b47cb4d163b89ab72be7b0a5994.d

../../bin/ZABAUDIT.so: ZABAUDIT.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/ZABAUDIT.cbl'
	cobc ZABAUDIT.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/ZABAUDIT.so" -MT "../../bin/ZABAUDIT.so" -MF .cobolit.tmp.133.54.150.224/ZABAUDIT-3389e7de10a53b47cb4d163b89ab72be7b0a5994.d

clean_ZABAUDIT_cbl: 
	rm -f "../../bin/ZABAUDIT.so" .cobolit.tmp.133.54.150.224/ZABAUDIT-3389e7de10a53b47cb4d163b89ab72be7b0a5994.d

target_HELLO_cbl: ../../bin/HELLO.so

-include .cobolit.tmp.133.54.150.224/HELLO-47240c00efe949fbcd592196e09efc577b96b967.d

../../bin/HELLO.so: HELLO.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/HELLO.cbl'
	cobc HELLO.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/HELLO.so" -MT "../../bin/HELLO.so" -MF .cobolit.tmp.133.54.150.224/HELLO-47240c00efe949fbcd592196e09efc577b96b967.d

clean_HELLO_cbl: 
	rm -f "../../bin/HELLO.so" .cobolit.tmp.133.54.150.224/HELLO-47240c00efe949fbcd592196e09efc577b96b967.d

target_BFX901_pco: ../../bin/BFX901.so

-include .cobolit.tmp.133.54.150.224/BFX901-35b5615eb5fd1b37eea9629b620b929d0e56ebe7.d

../../bin/BFX901.so: BFX901.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFX901.pco'
	cobc BFX901.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFX901.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFX901.so" -MT "../../bin/BFX901.so" -MF .cobolit.tmp.133.54.150.224/BFX901-35b5615eb5fd1b37eea9629b620b929d0e56ebe7.d

clean_BFX901_pco: 
	rm -f "../../bin/BFX901.so" .cobolit.tmp.133.54.150.224/BFX901-35b5615eb5fd1b37eea9629b620b929d0e56ebe7.d

target_BFTPCA_pco: ../../bin/BFTPCA.so

-include .cobolit.tmp.133.54.150.224/BFTPCA-eabd023a3b38a193aa213e886e44e9cf042ad22d.d

../../bin/BFTPCA.so: BFTPCA.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFTPCA.pco'
	cobc BFTPCA.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFTPCA.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFTPCA.so" -MT "../../bin/BFTPCA.so" -MF .cobolit.tmp.133.54.150.224/BFTPCA-eabd023a3b38a193aa213e886e44e9cf042ad22d.d

clean_BFTPCA_pco: 
	rm -f "../../bin/BFTPCA.so" .cobolit.tmp.133.54.150.224/BFTPCA-eabd023a3b38a193aa213e886e44e9cf042ad22d.d

target_BFGDOC_cbl: ../../bin/BFGDOC.so

-include .cobolit.tmp.133.54.150.224/BFGDOC-bc3760d24324d5c3cdcab6c0ba2b29e8f5935da2.d

../../bin/BFGDOC.so: BFGDOC.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFGDOC.cbl'
	cobc BFGDOC.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFGDOC.so" -MT "../../bin/BFGDOC.so" -MF .cobolit.tmp.133.54.150.224/BFGDOC-bc3760d24324d5c3cdcab6c0ba2b29e8f5935da2.d

clean_BFGDOC_cbl: 
	rm -f "../../bin/BFGDOC.so" .cobolit.tmp.133.54.150.224/BFGDOC-bc3760d24324d5c3cdcab6c0ba2b29e8f5935da2.d

target_BFG950_pco: ../../bin/BFG950.so

-include .cobolit.tmp.133.54.150.224/BFG950-8ba5cc5b55f44a1b2c2e5d02d959c9d653a3c8a1.d

../../bin/BFG950.so: BFG950.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG950.pco'
	cobc BFG950.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG950.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG950.so" -MT "../../bin/BFG950.so" -MF .cobolit.tmp.133.54.150.224/BFG950-8ba5cc5b55f44a1b2c2e5d02d959c9d653a3c8a1.d

clean_BFG950_pco: 
	rm -f "../../bin/BFG950.so" .cobolit.tmp.133.54.150.224/BFG950-8ba5cc5b55f44a1b2c2e5d02d959c9d653a3c8a1.d

target_BFG910_pco: ../../bin/BFG910.so

-include .cobolit.tmp.133.54.150.224/BFG910-8faecc29bbd8ed9c865b579c2b56cea3c4bdb3d0.d

../../bin/BFG910.so: BFG910.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG910.pco'
	cobc BFG910.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG910.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG910.so" -MT "../../bin/BFG910.so" -MF .cobolit.tmp.133.54.150.224/BFG910-8faecc29bbd8ed9c865b579c2b56cea3c4bdb3d0.d

clean_BFG910_pco: 
	rm -f "../../bin/BFG910.so" .cobolit.tmp.133.54.150.224/BFG910-8faecc29bbd8ed9c865b579c2b56cea3c4bdb3d0.d

target_BFG900_pco: ../../bin/BFG900.so

-include .cobolit.tmp.133.54.150.224/BFG900-9864489906f94a764f55662d9b6bab5b52a538e1.d

../../bin/BFG900.so: BFG900.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG900.pco'
	cobc BFG900.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG900.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG900.so" -MT "../../bin/BFG900.so" -MF .cobolit.tmp.133.54.150.224/BFG900-9864489906f94a764f55662d9b6bab5b52a538e1.d

clean_BFG900_pco: 
	rm -f "../../bin/BFG900.so" .cobolit.tmp.133.54.150.224/BFG900-9864489906f94a764f55662d9b6bab5b52a538e1.d

target_BFG262_pco: ../../bin/BFG262.so

-include .cobolit.tmp.133.54.150.224/BFG262-7c475a6b2a6caff398c7c8296e7ada7199d21f71.d

../../bin/BFG262.so: BFG262.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG262.pco'
	cobc BFG262.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG262.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG262.so" -MT "../../bin/BFG262.so" -MF .cobolit.tmp.133.54.150.224/BFG262-7c475a6b2a6caff398c7c8296e7ada7199d21f71.d

clean_BFG262_pco: 
	rm -f "../../bin/BFG262.so" .cobolit.tmp.133.54.150.224/BFG262-7c475a6b2a6caff398c7c8296e7ada7199d21f71.d

target_BFG261_pco: ../../bin/BFG261.so

-include .cobolit.tmp.133.54.150.224/BFG261-9013001699c21fcb9b407971830a48ac7dd7dad0.d

../../bin/BFG261.so: BFG261.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG261.pco'
	cobc BFG261.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG261.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG261.so" -MT "../../bin/BFG261.so" -MF .cobolit.tmp.133.54.150.224/BFG261-9013001699c21fcb9b407971830a48ac7dd7dad0.d

clean_BFG261_pco: 
	rm -f "../../bin/BFG261.so" .cobolit.tmp.133.54.150.224/BFG261-9013001699c21fcb9b407971830a48ac7dd7dad0.d

target_BFG260_pco: ../../bin/BFG260.so

-include .cobolit.tmp.133.54.150.224/BFG260-967b288f928fff334d974840899401b5542054ad.d

../../bin/BFG260.so: BFG260.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG260.pco'
	cobc BFG260.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG260.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG260.so" -MT "../../bin/BFG260.so" -MF .cobolit.tmp.133.54.150.224/BFG260-967b288f928fff334d974840899401b5542054ad.d

clean_BFG260_pco: 
	rm -f "../../bin/BFG260.so" .cobolit.tmp.133.54.150.224/BFG260-967b288f928fff334d974840899401b5542054ad.d

target_BFG250_pco: ../../bin/BFG250.so

-include .cobolit.tmp.133.54.150.224/BFG250-f60546b7b4bc58a0674848ac26b83db5b73e7290.d

../../bin/BFG250.so: BFG250.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG250.pco'
	cobc BFG250.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG250.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG250.so" -MT "../../bin/BFG250.so" -MF .cobolit.tmp.133.54.150.224/BFG250-f60546b7b4bc58a0674848ac26b83db5b73e7290.d

clean_BFG250_pco: 
	rm -f "../../bin/BFG250.so" .cobolit.tmp.133.54.150.224/BFG250-f60546b7b4bc58a0674848ac26b83db5b73e7290.d

target_BFG245_pco: ../../bin/BFG245.so

-include .cobolit.tmp.133.54.150.224/BFG245-3d39b94d0d8b22f67534f4b5affc2adf5b26aa34.d

../../bin/BFG245.so: BFG245.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG245.pco'
	cobc BFG245.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG245.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG245.so" -MT "../../bin/BFG245.so" -MF .cobolit.tmp.133.54.150.224/BFG245-3d39b94d0d8b22f67534f4b5affc2adf5b26aa34.d

clean_BFG245_pco: 
	rm -f "../../bin/BFG245.so" .cobolit.tmp.133.54.150.224/BFG245-3d39b94d0d8b22f67534f4b5affc2adf5b26aa34.d

target_BFG240_pco: ../../bin/BFG240.so

-include .cobolit.tmp.133.54.150.224/BFG240-e489b3e9d19c23bc1a42343861a6e89cf24e763f.d

../../bin/BFG240.so: BFG240.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG240.pco'
	cobc BFG240.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG240.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG240.so" -MT "../../bin/BFG240.so" -MF .cobolit.tmp.133.54.150.224/BFG240-e489b3e9d19c23bc1a42343861a6e89cf24e763f.d

clean_BFG240_pco: 
	rm -f "../../bin/BFG240.so" .cobolit.tmp.133.54.150.224/BFG240-e489b3e9d19c23bc1a42343861a6e89cf24e763f.d

target_BFG220_pco: ../../bin/BFG220.so

-include .cobolit.tmp.133.54.150.224/BFG220-49fcdd2dd1b77b2f6bee12eff4cb9776fb5abb90.d

../../bin/BFG220.so: BFG220.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG220.pco'
	cobc BFG220.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG220.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG220.so" -MT "../../bin/BFG220.so" -MF .cobolit.tmp.133.54.150.224/BFG220-49fcdd2dd1b77b2f6bee12eff4cb9776fb5abb90.d

clean_BFG220_pco: 
	rm -f "../../bin/BFG220.so" .cobolit.tmp.133.54.150.224/BFG220-49fcdd2dd1b77b2f6bee12eff4cb9776fb5abb90.d

target_BFG215_pco: ../../bin/BFG215.so

-include .cobolit.tmp.133.54.150.224/BFG215-c068c7d47e993d1d587ea734e2763686f62a3d7e.d

../../bin/BFG215.so: BFG215.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG215.pco'
	cobc BFG215.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG215.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG215.so" -MT "../../bin/BFG215.so" -MF .cobolit.tmp.133.54.150.224/BFG215-c068c7d47e993d1d587ea734e2763686f62a3d7e.d

clean_BFG215_pco: 
	rm -f "../../bin/BFG215.so" .cobolit.tmp.133.54.150.224/BFG215-c068c7d47e993d1d587ea734e2763686f62a3d7e.d

target_BFG211_pco: ../../bin/BFG211.so

-include .cobolit.tmp.133.54.150.224/BFG211-70f972baa9f00fdb610d721be75cf52b3a68d2b1.d

../../bin/BFG211.so: BFG211.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG211.pco'
	cobc BFG211.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG211.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG211.so" -MT "../../bin/BFG211.so" -MF .cobolit.tmp.133.54.150.224/BFG211-70f972baa9f00fdb610d721be75cf52b3a68d2b1.d

clean_BFG211_pco: 
	rm -f "../../bin/BFG211.so" .cobolit.tmp.133.54.150.224/BFG211-70f972baa9f00fdb610d721be75cf52b3a68d2b1.d

target_BFG210_cbl: ../../bin/BFG210.so

-include .cobolit.tmp.133.54.150.224/BFG210-947ccd26e435e87726aca04b5132f9f0d517ca54.d

../../bin/BFG210.so: BFG210.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG210.cbl'
	cobc BFG210.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG210.so" -MT "../../bin/BFG210.so" -MF .cobolit.tmp.133.54.150.224/BFG210-947ccd26e435e87726aca04b5132f9f0d517ca54.d

clean_BFG210_cbl: 
	rm -f "../../bin/BFG210.so" .cobolit.tmp.133.54.150.224/BFG210-947ccd26e435e87726aca04b5132f9f0d517ca54.d

target_BFG200_pco: ../../bin/BFG200.so

-include .cobolit.tmp.133.54.150.224/BFG200-58117836f8ac18e5b56df9fe39a0afa731dc69ff.d

../../bin/BFG200.so: BFG200.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG200.pco'
	cobc BFG200.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG200.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG200.so" -MT "../../bin/BFG200.so" -MF .cobolit.tmp.133.54.150.224/BFG200-58117836f8ac18e5b56df9fe39a0afa731dc69ff.d

clean_BFG200_pco: 
	rm -f "../../bin/BFG200.so" .cobolit.tmp.133.54.150.224/BFG200-58117836f8ac18e5b56df9fe39a0afa731dc69ff.d

target_BFG180_pco: ../../bin/BFG180.so

-include .cobolit.tmp.133.54.150.224/BFG180-6d1d4185b1256eeead6736784b756e81d3a4e5dd.d

../../bin/BFG180.so: BFG180.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG180.pco'
	cobc BFG180.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG180.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG180.so" -MT "../../bin/BFG180.so" -MF .cobolit.tmp.133.54.150.224/BFG180-6d1d4185b1256eeead6736784b756e81d3a4e5dd.d

clean_BFG180_pco: 
	rm -f "../../bin/BFG180.so" .cobolit.tmp.133.54.150.224/BFG180-6d1d4185b1256eeead6736784b756e81d3a4e5dd.d

target_BFG150_pco: ../../bin/BFG150.so

-include .cobolit.tmp.133.54.150.224/BFG150-5cf172142d843f107e755e86cadc44668095fb92.d

../../bin/BFG150.so: BFG150.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG150.pco'
	cobc BFG150.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG150.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG150.so" -MT "../../bin/BFG150.so" -MF .cobolit.tmp.133.54.150.224/BFG150-5cf172142d843f107e755e86cadc44668095fb92.d

clean_BFG150_pco: 
	rm -f "../../bin/BFG150.so" .cobolit.tmp.133.54.150.224/BFG150-5cf172142d843f107e755e86cadc44668095fb92.d

target_BFG140_pco: ../../bin/BFG140.so

-include .cobolit.tmp.133.54.150.224/BFG140-a3b4d4272508e402b836e7d92db524598d02eb1d.d

../../bin/BFG140.so: BFG140.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG140.pco'
	cobc BFG140.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG140.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG140.so" -MT "../../bin/BFG140.so" -MF .cobolit.tmp.133.54.150.224/BFG140-a3b4d4272508e402b836e7d92db524598d02eb1d.d

clean_BFG140_pco: 
	rm -f "../../bin/BFG140.so" .cobolit.tmp.133.54.150.224/BFG140-a3b4d4272508e402b836e7d92db524598d02eb1d.d

target_BFG130_pco: ../../bin/BFG130.so

-include .cobolit.tmp.133.54.150.224/BFG130-1f29ff1be64fd5d51a2f386b2646bc1326ba2237.d

../../bin/BFG130.so: BFG130.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG130.pco'
	cobc BFG130.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG130.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG130.so" -MT "../../bin/BFG130.so" -MF .cobolit.tmp.133.54.150.224/BFG130-1f29ff1be64fd5d51a2f386b2646bc1326ba2237.d

clean_BFG130_pco: 
	rm -f "../../bin/BFG130.so" .cobolit.tmp.133.54.150.224/BFG130-1f29ff1be64fd5d51a2f386b2646bc1326ba2237.d

target_BFG120_pco: ../../bin/BFG120.so

-include .cobolit.tmp.133.54.150.224/BFG120-64dbba0add5f793cd916561c9d7d9512422c5dad.d

../../bin/BFG120.so: BFG120.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG120.pco'
	cobc BFG120.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG120.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG120.so" -MT "../../bin/BFG120.so" -MF .cobolit.tmp.133.54.150.224/BFG120-64dbba0add5f793cd916561c9d7d9512422c5dad.d

clean_BFG120_pco: 
	rm -f "../../bin/BFG120.so" .cobolit.tmp.133.54.150.224/BFG120-64dbba0add5f793cd916561c9d7d9512422c5dad.d

target_BFG115_pco: ../../bin/BFG115.so

-include .cobolit.tmp.133.54.150.224/BFG115-48ba1033efe1aaec6fa8c1b4384f9457b071bbdc.d

../../bin/BFG115.so: BFG115.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG115.pco'
	cobc BFG115.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG115.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG115.so" -MT "../../bin/BFG115.so" -MF .cobolit.tmp.133.54.150.224/BFG115-48ba1033efe1aaec6fa8c1b4384f9457b071bbdc.d

clean_BFG115_pco: 
	rm -f "../../bin/BFG115.so" .cobolit.tmp.133.54.150.224/BFG115-48ba1033efe1aaec6fa8c1b4384f9457b071bbdc.d

target_BFG110_pco: ../../bin/BFG110.so

-include .cobolit.tmp.133.54.150.224/BFG110-663256cae68832584dd3c192b631d2ec624c6139.d

../../bin/BFG110.so: BFG110.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG110.pco'
	cobc BFG110.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG110.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG110.so" -MT "../../bin/BFG110.so" -MF .cobolit.tmp.133.54.150.224/BFG110-663256cae68832584dd3c192b631d2ec624c6139.d

clean_BFG110_pco: 
	rm -f "../../bin/BFG110.so" .cobolit.tmp.133.54.150.224/BFG110-663256cae68832584dd3c192b631d2ec624c6139.d

target_BFG100_pco: ../../bin/BFG100.so

-include .cobolit.tmp.133.54.150.224/BFG100-06eb71343d645db556c0f32a773edbea0d7aa916.d

../../bin/BFG100.so: BFG100.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG100.pco'
	cobc BFG100.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG100.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG100.so" -MT "../../bin/BFG100.so" -MF .cobolit.tmp.133.54.150.224/BFG100-06eb71343d645db556c0f32a773edbea0d7aa916.d

clean_BFG100_pco: 
	rm -f "../../bin/BFG100.so" .cobolit.tmp.133.54.150.224/BFG100-06eb71343d645db556c0f32a773edbea0d7aa916.d

target_BFG002A_pco: ../../bin/BFG002A.so

-include .cobolit.tmp.133.54.150.224/BFG002A-603be2b4e40533c961ec2d96362fae7e7f84435d.d

../../bin/BFG002A.so: BFG002A.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG002A.pco'
	cobc BFG002A.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG002A.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG002A.so" -MT "../../bin/BFG002A.so" -MF .cobolit.tmp.133.54.150.224/BFG002A-603be2b4e40533c961ec2d96362fae7e7f84435d.d

clean_BFG002A_pco: 
	rm -f "../../bin/BFG002A.so" .cobolit.tmp.133.54.150.224/BFG002A-603be2b4e40533c961ec2d96362fae7e7f84435d.d

target_BFG002_pco: ../../bin/BFG002.so

-include .cobolit.tmp.133.54.150.224/BFG002-8bbb7ff9b930f1b0eb6112907af04488e21b6d72.d

../../bin/BFG002.so: BFG002.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG002.pco'
	cobc BFG002.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG002.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG002.so" -MT "../../bin/BFG002.so" -MF .cobolit.tmp.133.54.150.224/BFG002-8bbb7ff9b930f1b0eb6112907af04488e21b6d72.d

clean_BFG002_pco: 
	rm -f "../../bin/BFG002.so" .cobolit.tmp.133.54.150.224/BFG002-8bbb7ff9b930f1b0eb6112907af04488e21b6d72.d

target_BFG001N_pco: ../../bin/BFG001N.so

-include .cobolit.tmp.133.54.150.224/BFG001N-130076d02203d1abbab9369d1e669f39cf95dc10.d

../../bin/BFG001N.so: BFG001N.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG001N.pco'
	cobc BFG001N.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG001N.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG001N.so" -MT "../../bin/BFG001N.so" -MF .cobolit.tmp.133.54.150.224/BFG001N-130076d02203d1abbab9369d1e669f39cf95dc10.d

clean_BFG001N_pco: 
	rm -f "../../bin/BFG001N.so" .cobolit.tmp.133.54.150.224/BFG001N-130076d02203d1abbab9369d1e669f39cf95dc10.d

target_BFG000A_pco: ../../bin/BFG000A.so

-include .cobolit.tmp.133.54.150.224/BFG000A-80bf1ce686f5b0e2279fafd550462f3622c2de6e.d

../../bin/BFG000A.so: BFG000A.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG000A.pco'
	cobc BFG000A.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG000A.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG000A.so" -MT "../../bin/BFG000A.so" -MF .cobolit.tmp.133.54.150.224/BFG000A-80bf1ce686f5b0e2279fafd550462f3622c2de6e.d

clean_BFG000A_pco: 
	rm -f "../../bin/BFG000A.so" .cobolit.tmp.133.54.150.224/BFG000A-80bf1ce686f5b0e2279fafd550462f3622c2de6e.d

target_BFG000_pco: ../../bin/BFG000.so

-include .cobolit.tmp.133.54.150.224/BFG000-82bb5ab770e5afdf280ba18ca47dc1d171bc22b7.d

../../bin/BFG000.so: BFG000.pco
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BFG000.pco'
	cobc BFG000.pco -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -preprocess="../TOOLS/citprodb2.sh BFG000.pco" -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BFG000.so" -MT "../../bin/BFG000.so" -MF .cobolit.tmp.133.54.150.224/BFG000-82bb5ab770e5afdf280ba18ca47dc1d171bc22b7.d

clean_BFG000_pco: 
	rm -f "../../bin/BFG000.so" .cobolit.tmp.133.54.150.224/BFG000-82bb5ab770e5afdf280ba18ca47dc1d171bc22b7.d

target_BETDATC_cbl: ../../bin/BETDATC.so

-include .cobolit.tmp.133.54.150.224/BETDATC-bf90b45adaaa2cbef3ed3182a5e56f8dcafcbe29.d

../../bin/BETDATC.so: BETDATC.cbl
	@echo 'building /app/decoz01/workspace/DECOZ/BATCH/BETDATC.cbl'
	cobc BETDATC.cbl -I "../COPY" -I "../COPY/DESC" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -g -fixed -fsign-ebcdic -conf="../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../bin/BETDATC.so" -MT "../../bin/BETDATC.so" -MF .cobolit.tmp.133.54.150.224/BETDATC-bf90b45adaaa2cbef3ed3182a5e56f8dcafcbe29.d

clean_BETDATC_cbl: 
	rm -f "../../bin/BETDATC.so" .cobolit.tmp.133.54.150.224/BETDATC-bf90b45adaaa2cbef3ed3182a5e56f8dcafcbe29.d



clean: clean_ZABAUDIT_cbl clean_HELLO_cbl clean_BFX901_pco clean_BFTPCA_pco clean_BFGDOC_cbl clean_BFG950_pco clean_BFG910_pco clean_BFG900_pco clean_BFG262_pco clean_BFG261_pco clean_BFG260_pco clean_BFG250_pco clean_BFG245_pco clean_BFG240_pco clean_BFG220_pco clean_BFG215_pco clean_BFG211_pco clean_BFG210_cbl clean_BFG200_pco clean_BFG180_pco clean_BFG150_pco clean_BFG140_pco clean_BFG130_pco clean_BFG120_pco clean_BFG115_pco clean_BFG110_pco clean_BFG100_pco clean_BFG002A_pco clean_BFG002_pco clean_BFG001N_pco clean_BFG000A_pco clean_BFG000_pco clean_BETDATC_cbl 

target_c: 
target__settings: 
target__launches: 


clean_c: 
clean__settings: 
clean__launches: 

